package com.ysweet.system.mapper;

import com.ysweet.common.core.page.BaseMapperPlus;
import com.ysweet.system.domain.SysUserRole;

import java.util.List;

/**
 * 用户与角色关联表 数据层
 *
 * @author 小金木
 */
public interface SysUserRoleMapper extends BaseMapperPlus<SysUserRole> {

	/**
	 * 批量新增用户角色信息
	 *
	 * @param userRoleList 用户角色列表
	 * @return 结果
	 */
	public int batchUserRole(List<SysUserRole> userRoleList);

}
