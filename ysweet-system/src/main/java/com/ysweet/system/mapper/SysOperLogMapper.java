package com.ysweet.system.mapper;

import com.ysweet.common.core.page.BaseMapperPlus;
import com.ysweet.system.domain.SysOperLog;

/**
 * 操作日志 数据层
 *
 * @author 小金木
 */
public interface SysOperLogMapper extends BaseMapperPlus<SysOperLog> {

}
