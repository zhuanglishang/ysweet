package com.ysweet.system.mapper;

import com.ysweet.common.core.page.BaseMapperPlus;
import com.ysweet.system.domain.SysConfig;

/**
 * 参数配置 数据层
 *
 * @author 小金木
 */
public interface SysConfigMapper extends BaseMapperPlus<SysConfig> {

}
