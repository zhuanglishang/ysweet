package com.ysweet.system.mapper;

import com.ysweet.common.core.domain.entity.SysDictType;
import com.ysweet.common.core.page.BaseMapperPlus;

/**
 * 字典表 数据层
 *
 * @author 小金木
 */
public interface SysDictTypeMapper extends BaseMapperPlus<SysDictType> {

}
