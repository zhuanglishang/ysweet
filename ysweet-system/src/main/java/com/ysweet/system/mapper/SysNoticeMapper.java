package com.ysweet.system.mapper;

import com.ysweet.common.core.page.BaseMapperPlus;
import com.ysweet.system.domain.SysNotice;

/**
 * 通知公告表 数据层
 *
 * @author 小金木
 */
public interface SysNoticeMapper extends BaseMapperPlus<SysNotice> {

}
