package com.ysweet.system.mapper;

import com.ysweet.common.core.page.BaseMapperPlus;
import com.ysweet.system.domain.SysLogininfor;

/**
 * 系统访问日志情况信息 数据层
 *
 * @author 小金木
 */
public interface SysLogininforMapper extends BaseMapperPlus<SysLogininfor> {

}
