package com.ysweet.quartz.mapper;

import com.ysweet.common.core.page.BaseMapperPlus;
import com.ysweet.quartz.domain.SysJob;

/**
 * 调度任务信息 数据层
 *
 * @author 小金木
 */
public interface SysJobMapper extends BaseMapperPlus<SysJob> {

}
