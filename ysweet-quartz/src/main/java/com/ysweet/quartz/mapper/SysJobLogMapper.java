package com.ysweet.quartz.mapper;

import com.ysweet.common.core.page.BaseMapperPlus;
import com.ysweet.quartz.domain.SysJobLog;

/**
 * 调度任务日志信息 数据层
 *
 * @author 小金木
 */
public interface SysJobLogMapper extends BaseMapperPlus<SysJobLog> {

}
