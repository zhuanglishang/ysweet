package com.ysweet.web.controller.shop;

import java.util.Arrays;
import java.util.Map;

import com.ysweet.shop.service.IYsMaterialGroupService;
import com.ysweet.system.domain.YsMaterialGroup;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.*;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.ysweet.common.annotation.Log;
import com.ysweet.common.core.controller.BaseController;
import com.ysweet.common.core.domain.AjaxResult;
import com.ysweet.common.enums.BusinessType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 素材分组Controller
 *
 * @author 小金木
 * @date 2021-06-18
 */
@Api(value = "素材分组控制器", tags = {"素材分组管理"})
@RequiredArgsConstructor
@RestController
@RequestMapping("/shop/group")
public class YsMaterialGroupController extends BaseController {

	private final IYsMaterialGroupService iYsMaterialGroupService;

	/**
	 * 查询素材分组列表
	 */
	@ApiOperation("查询素材分组列表")
	@PreAuthorize("@ss.hasPermi('shop:group:list')")
	@GetMapping("/list")
	public AjaxResult list() {
		return AjaxResult.success(iYsMaterialGroupService.list());
	}


	/**
	 * 新增素材分组
	 */
	@ApiOperation("新增素材分组")
	@PreAuthorize("@ss.hasPermi('shop:group:add')")
	@Log(title = "素材分组", businessType = BusinessType.INSERT)
	@PostMapping(consumes="application/json")
	public AjaxResult add(@RequestBody Map<String,String> map) {
		return iYsMaterialGroupService.addGroup(map.get("name"));
	}

	/**
	 * 修改素材分组
	 */
	@ApiOperation("修改素材分组")
	@PreAuthorize("@ss.hasPermi('shop:group:edit')")
	@Log(title = "素材分组", businessType = BusinessType.UPDATE)
	@PutMapping()
	public AjaxResult<Void> edit(@Validated @RequestBody YsMaterialGroup ysMaterialGroup) {
		return toAjax(iYsMaterialGroupService.saveOrUpdate(ysMaterialGroup) ? 1 : 0);
	}

	/**
	 * 删除素材分组
	 */
	@ApiOperation("删除素材分组")
	@PreAuthorize("@ss.hasPermi('shop:group:remove')")
	@Log(title = "素材分组", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
	public AjaxResult<Void> remove(@NotEmpty(message = "主键不能为空")
								   @PathVariable String[] ids) {
		return toAjax(iYsMaterialGroupService.removeByIds(Arrays.asList(ids)) ? 1 : 0);
	}
}
