package com.ysweet.web.controller.shop;

import com.ysweet.common.core.controller.BaseController;
import com.ysweet.common.core.domain.AjaxResult;
import com.ysweet.shop.service.IYsSysConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 系统配置管理
 *
 * @author 小金木
 * @date 2021年6月21日10:27:07
 */
@Api(value = "系统配置管理", tags = {"系统配置管理"})
@RequiredArgsConstructor
@RestController
@RequestMapping("/shop/sysConfig")
public class YsSysConfigController extends BaseController {

	private final IYsSysConfigService ysSysConfigService;

	@ApiOperation("获得指定类型的配置")
	@PreAuthorize("@ss.hasPermi('shop:sysconfig:list')")
	@GetMapping("/{type}")
	public AjaxResult getByType(@PathVariable("type") String type) {
		return AjaxResult.success(ysSysConfigService.selectByType(type));
	}

	@ApiOperation("获得指定类型的配置")
	@PreAuthorize("@ss.hasPermi('shop:sysconfig:list')")
	@PutMapping()
	public AjaxResult update(@RequestBody Map<String,String> param) {
		return AjaxResult.success(ysSysConfigService.updateConfig(param));
	}
}
