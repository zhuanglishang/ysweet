package com.ysweet.web.controller.shop;

import com.ysweet.common.core.domain.AjaxResult;
import com.ysweet.common.core.page.TableDataInfo;
import com.ysweet.common.utils.SecurityUtils;
import com.ysweet.shop.domain.YsArticle;
import com.ysweet.shop.service.IYsArticleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * 文章管理
 *
 * @author 小金木
 * @date 2021年6月21日10:27:07
 */
@Api(value = "文章管理控制器", tags = {"文章信息管理"})
@RequiredArgsConstructor
@RestController
@RequestMapping("/shop/article")
public class YsArticleController {

	private final IYsArticleService ysArticleService;

	@ApiOperation("查询文章列表")
	@PreAuthorize("@ss.hasPermi('shop:article:list')")
	@PostMapping("/list")
	public TableDataInfo<YsArticle> getArticle(@RequestBody YsArticle ysArticle) {
		return ysArticleService.selectOfPage(ysArticle);
	}


	@ApiOperation("修改或新增文章信息")
	@PreAuthorize("@ss.hasPermi('shop:article:update')")
	@PutMapping()
	public AjaxResult updateArticle(@RequestBody YsArticle ysArticle) {
		ysArticle.setAuthor(SecurityUtils.getUsername());
		return AjaxResult.success(ysArticleService.saveOrUpdate(ysArticle));
	}

	@ApiOperation("删除文章")
	@PreAuthorize("@ss.hasPermi('shop:article:delete')")
	@DeleteMapping("/{id}")
	public AjaxResult delete(@PathVariable("id") Long id) {
		return AjaxResult.success(ysArticleService.removeById(id));
	}
}
