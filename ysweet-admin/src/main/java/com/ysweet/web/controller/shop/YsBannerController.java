package com.ysweet.web.controller.shop;

import com.ysweet.common.core.controller.BaseController;
import com.ysweet.common.core.domain.AjaxResult;
import com.ysweet.common.core.page.TableDataInfo;
import com.ysweet.shop.domain.YsBanner;
import com.ysweet.shop.service.IYsBannerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * 广告信息管理
 *
 * @author 小金木
 * @date 2021年6月21日10:27:07
 */
@Api(value = "广告控制器", tags = {"广告信息管理"})
@RequiredArgsConstructor
@RestController
@RequestMapping("/shop/banner")
public class YsBannerController extends BaseController {

	private final IYsBannerService ysBannerService;

	@ApiOperation("查询广告列表")
	@PreAuthorize("@ss.hasPermi('shop:banner:list')")
	@PostMapping("/list")
	public TableDataInfo<YsBanner> getBanner(YsBanner ysBanner) {
		return ysBannerService.selectByPage(ysBanner);
	}

	@ApiOperation("修改或新增广告信息")
	@PreAuthorize("@ss.hasPermi('shop:banner:update')")
	@PutMapping()
	public AjaxResult updateBanner(@RequestBody YsBanner ysBanner) {
		return AjaxResult.success(ysBannerService.saveOrUpdate(ysBanner));
	}

	@ApiOperation("删除广告")
	@PreAuthorize("@ss.hasPermi('shop:banner:delete')")
	@DeleteMapping("/{id}")
	public AjaxResult delete(@PathVariable("id") Long id) {
		return AjaxResult.success(ysBannerService.removeById(id));
	}
}
