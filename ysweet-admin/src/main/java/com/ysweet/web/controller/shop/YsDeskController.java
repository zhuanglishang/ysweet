package com.ysweet.web.controller.shop;

import com.ysweet.common.core.domain.AjaxResult;
import com.ysweet.common.core.page.TableDataInfo;
import com.ysweet.shop.domain.YsDesk;
import com.ysweet.shop.service.IYsDeskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * 餐桌管理
 *
 * @author 小金木
 * @date 2021年8月24日13:58:35
 */
@Api(value = "餐桌管理", tags = {"餐桌管理"})
@RequiredArgsConstructor
@RestController
@RequestMapping("/shop/desk")
public class YsDeskController {

	private final IYsDeskService ysDeskService;

	@ApiOperation("查询餐桌信息")
	@PreAuthorize("@ss.hasPermi('shop:desk:list')")
	@PostMapping("/list")
	public TableDataInfo<YsDesk> list(@RequestBody YsDesk ysDesk) {
		return ysDeskService.listOfPage(ysDesk);
	}

	@ApiOperation("修改订单列表")
	@PreAuthorize("@ss.hasPermi('shop:desk:update')")
	@PutMapping()
	public AjaxResult update(@RequestBody YsDesk ysDesk) {
		return AjaxResult.success(ysDeskService.saveOrUpdate(ysDesk));
	}

	@ApiOperation("删除订单列表")
	@PreAuthorize("@ss.hasPermi('shop:desk:delete')")
	@DeleteMapping("/{id}")
	public AjaxResult delete(@PathVariable("id") Long id) {
		return AjaxResult.success(ysDeskService.removeById(id));
	}
}
