package com.ysweet.web.controller.shop;

import cn.hutool.crypto.digest.MD5;
import com.ysweet.common.core.controller.BaseController;
import com.ysweet.common.core.domain.AjaxResult;
import com.ysweet.common.core.page.TableDataInfo;
import com.ysweet.common.utils.SecurityUtils;
import com.ysweet.shop.domain.YsMaterial;
import com.ysweet.shop.service.IYsMaterialService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * 素材库Controller
 *
 * @author 小金木
 * @date 2021-06-18
 */
@Api(value = "素材库控制器", tags = {"素材库管理"})
@RequiredArgsConstructor
@RestController
@RequestMapping("/shop/material")
public class YsMaterialController extends BaseController {

	private final IYsMaterialService ysMaterialService;

	/**
	 * 查询素材列表
	 *
	 * @return
	 */
	@ApiOperation("查询素材列表")
	@PreAuthorize("@ss.hasPermi('shop:material:list')")
	@GetMapping("/list")
	public TableDataInfo<YsMaterial> list(YsMaterial ysMaterial) {
		return ysMaterialService.selectPageDictDataList(ysMaterial);
	}

	/**
	 * 删除素材
	 *
	 * @return
	 */
	@ApiOperation("删除素材")
	@PreAuthorize("@ss.hasPermi('shop:material:delete')")
	@DeleteMapping("/{id}")
	public AjaxResult delete(@PathVariable String id) {
		return AjaxResult.success(ysMaterialService.deleteMaterial(id));
	}

	@ApiOperation("添加素材")
	@PreAuthorize("@ss.hasPermi('shop:material:add')")
	@PostMapping()
	public AjaxResult add(@RequestBody YsMaterial ysMaterial) {
		ysMaterial.setId(MD5.create().digestHex(System.currentTimeMillis()+""));
		ysMaterial.setCreateId(SecurityUtils.getUsername());
		return AjaxResult.success(ysMaterialService.save(ysMaterial));
	}
}
