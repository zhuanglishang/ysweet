package com.ysweet.web.controller.shop;

import com.ysweet.common.core.controller.BaseController;
import com.ysweet.common.core.domain.AjaxResult;
import com.ysweet.common.core.page.TableDataInfo;
import com.ysweet.shop.domain.YsCoupon;
import com.ysweet.shop.service.IYsCouponService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/***
 * 优惠券增删改查
 * @author 小金木
 * @date 2021年6月25日11:24:21
 */
@Api(value = "优惠券管理相关逻辑", tags = {"优惠券管理相关逻辑"})
@RestController
@RequestMapping("/shop/coupon")
@RequiredArgsConstructor
public class YsCouponController extends BaseController {

	private final IYsCouponService ysCouponService;

	@PostMapping("/getAllCoupon")
	@PreAuthorize("@ss.hasPermi('shop:coupon:update')")
	@ApiOperation(value = "获得所有优惠券", notes = "获得所有优惠券")
	public TableDataInfo<YsCoupon> getAllCoupon(@RequestBody YsCoupon ysCoupon) {
		return ysCouponService.selectByPage(ysCoupon);
	}

	@ApiOperation("修改或新增优惠券")
	@PreAuthorize("@ss.hasPermi('shop:coupon:update')")
	@PutMapping()
	public AjaxResult updateCoupon(@RequestBody YsCoupon ysCoupon) {
		return AjaxResult.success(ysCouponService.saveOrUpdate(ysCoupon));
	}

	@ApiOperation("删除优惠券")
	@PreAuthorize("@ss.hasPermi('shop:coupon:delete')")
	@DeleteMapping("/{id}")
	public AjaxResult delete(@PathVariable("id") Long id) {
		return AjaxResult.success(ysCouponService.removeById(id));
	}
}
