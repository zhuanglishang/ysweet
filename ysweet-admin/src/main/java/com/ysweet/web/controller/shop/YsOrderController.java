package com.ysweet.web.controller.shop;

import com.ysweet.common.core.domain.AjaxResult;
import com.ysweet.common.core.page.TableDataInfo;
import com.ysweet.common.utils.SecurityUtils;
import com.ysweet.shop.domain.YsOrder;
import com.ysweet.shop.service.IYsOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * 订单管理
 *
 * @author 小金木
 * @date 2021年8月24日13:58:35
 */
@Api(value = "文订单管理控制器", tags = {"订单管理"})
@RequiredArgsConstructor
@RestController
@RequestMapping("/shop/order")
public class YsOrderController {

	private final IYsOrderService ysOrderService;

	@ApiOperation("查询订单列表")
	@PreAuthorize("@ss.hasPermi('shop:order:list')")
	@PostMapping("/list")
	public TableDataInfo<YsOrder> getArticle(@RequestBody YsOrder ysOrder) {
		return ysOrderService.selectOfPage(ysOrder);
	}


	@ApiOperation("修改订单列表")
	@PreAuthorize("@ss.hasPermi('shop:order:update')")
	@PutMapping()
	public AjaxResult updateArticle(@RequestBody YsOrder ysOrder) {
		ysOrder.setUserId(SecurityUtils.getLoginUser().getUser().getUserId());
		return AjaxResult.success(ysOrderService.saveOrUpdate(ysOrder));
	}

	@ApiOperation("删除订单列表")
	@PreAuthorize("@ss.hasPermi('shop:order:delete')")
	@DeleteMapping("/{id}")
	public AjaxResult delete(@PathVariable("id") Long id) {
		return AjaxResult.success(ysOrderService.removeById(id));
	}
}
