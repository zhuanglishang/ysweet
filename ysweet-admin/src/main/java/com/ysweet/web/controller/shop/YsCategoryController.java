package com.ysweet.web.controller.shop;

import com.ysweet.common.core.controller.BaseController;
import com.ysweet.common.core.domain.AjaxResult;
import com.ysweet.common.core.page.TableDataInfo;
import com.ysweet.shop.domain.YsCategory;
import com.ysweet.shop.service.IYsCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/***
 * 商品分类的增删改查
 * @author 小金木
 * @date 2021年7月6日17:12:02
 */
@Api(value = "商品分类相关逻辑", tags = {"商品分类相关逻辑"})
@RestController
@RequestMapping("/shop/category")
@RequiredArgsConstructor
public class YsCategoryController extends BaseController {

	private final IYsCategoryService ysCategoryService;

	@PostMapping("/getAllCategory")
	@PreAuthorize("@ss.hasPermi('shop:category:list')")
	@ApiOperation(value = "获得所有商品分类", notes = "获得所有商品分类")
	public TableDataInfo<YsCategory> getAllCategory(@RequestBody(required=false) YsCategory ysCategory) {
		return ysCategoryService.selectByPage(ysCategory);
	}

	@ApiOperation("修改或新增商品分类")
	@PreAuthorize("@ss.hasPermi('shop:category:update')")
	@PutMapping()
	public AjaxResult updateCategory(@RequestBody YsCategory ysCategory) {
		return AjaxResult.success(ysCategoryService.saveOrUpdate(ysCategory));
	}

	@ApiOperation("删除商品分类")
	@PreAuthorize("@ss.hasPermi('shop:category:delete')")
	@DeleteMapping("/{id}")
	public AjaxResult delete(@PathVariable("id") Long id) {
		return AjaxResult.success(ysCategoryService.removeById(id));
	}
}
