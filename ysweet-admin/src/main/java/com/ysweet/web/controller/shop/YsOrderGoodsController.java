package com.ysweet.web.controller.shop;

import com.ysweet.common.core.controller.BaseController;
import com.ysweet.common.core.domain.AjaxResult;
import com.ysweet.common.core.page.TableDataInfo;
import com.ysweet.shop.domain.YsGoods;
import com.ysweet.shop.domain.YsOrderGoods;
import com.ysweet.shop.service.impl.YsOrderGoodsServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.aspectj.weaver.loadtime.Aj;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 定单物品数据管理
 *
 * @author 小金木
 * @date 2021年11月12日17:20:34
 */
@Api(value = "商品管理", tags = {"商品管理"})
@RequiredArgsConstructor
@RestController
@RequestMapping("/shop/orderGoods")
public class YsOrderGoodsController extends BaseController {
	private final YsOrderGoodsServiceImpl ysOrderGoodsServiceImpl;

	@ApiOperation("查询商品列表")
	@PreAuthorize("@ss.hasPermi('shop:orderGoods:list')")
	@PostMapping("/list")
	public TableDataInfo<YsOrderGoods> getGoods(@RequestBody YsOrderGoods ysOrderGoods) {
		return ysOrderGoodsServiceImpl.selectOfPage(ysOrderGoods);
	}

	@ApiOperation("修改或新增查商品")
	@PreAuthorize("@ss.hasPermi('shop:orderGoods:update')")
	@PutMapping()
	public AjaxResult<Boolean> updateGoods(@RequestBody List<YsOrderGoods> ysOrderGoods) {
		return AjaxResult.success(ysOrderGoodsServiceImpl.saveOrUpdateOrderGoods(ysOrderGoods));
	}

}
