package com.ysweet.web.controller.shop;

import com.ysweet.common.core.controller.BaseController;
import com.ysweet.common.core.domain.AjaxResult;
import com.ysweet.common.core.page.TableDataInfo;
import com.ysweet.shop.domain.YsGoods;
import com.ysweet.shop.service.IYsGoodsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * 商品管理信息管理
 *
 * @author 小金木
 * @date 2021年6月21日10:27:07
 */
@Api(value = "商品管理", tags = {"商品管理"})
@RequiredArgsConstructor
@RestController
@RequestMapping("/shop/goods")
public class YsGoodsController extends BaseController {

	private final IYsGoodsService ysBannerService;

	@ApiOperation("查询商品列表")
	@PreAuthorize("@ss.hasPermi('shop:goods:list')")
	@PostMapping("/list")
	public TableDataInfo<YsGoods> getGoods(@RequestBody YsGoods ysGoods) {
		return ysBannerService.selectByPage(ysGoods);
	}

	@ApiOperation("修改或新增查商品")
	@PreAuthorize("@ss.hasPermi('shop:goods:update')")
	@PutMapping()
	public AjaxResult<Boolean> updateGoods(@RequestBody YsGoods ysGoods) {
		ysGoods=ysGoods==null?new YsGoods():ysGoods;
		return AjaxResult.success(ysBannerService.saveOrUpdateGoods(ysGoods));
	}

	@ApiOperation("删除商品")
	@PreAuthorize("@ss.hasPermi('shop:goods:delete')")
	@DeleteMapping("/{id}")
	public AjaxResult<Boolean> delete(@PathVariable("id") Long id) {
		return AjaxResult.success(ysBannerService.removeById(id));
	}
}
