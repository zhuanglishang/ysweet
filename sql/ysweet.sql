/*
 Navicat Premium Data Transfer

 Source Server         : 华为云
 Source Server Type    : MySQL
 Source Server Version : 80027
 Source Host           : 114.115.152.162:3306
 Source Schema         : ysweet

 Target Server Type    : MySQL
 Target Server Version : 80027
 File Encoding         : 65001

 Date: 08/11/2021 15:46:10
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for QRTZ_BLOB_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_BLOB_TRIGGERS`;
CREATE TABLE `QRTZ_BLOB_TRIGGERS`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `blob_data` blob NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `QRTZ_BLOB_TRIGGERS_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `QRTZ_TRIGGERS` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of QRTZ_BLOB_TRIGGERS
-- ----------------------------

-- ----------------------------
-- Table structure for QRTZ_CALENDARS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_CALENDARS`;
CREATE TABLE `QRTZ_CALENDARS`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `calendar_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `calendar` blob NOT NULL,
  PRIMARY KEY (`sched_name`, `calendar_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of QRTZ_CALENDARS
-- ----------------------------

-- ----------------------------
-- Table structure for QRTZ_CRON_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_CRON_TRIGGERS`;
CREATE TABLE `QRTZ_CRON_TRIGGERS`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `cron_expression` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `time_zone_id` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `QRTZ_CRON_TRIGGERS_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `QRTZ_TRIGGERS` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of QRTZ_CRON_TRIGGERS
-- ----------------------------
INSERT INTO `QRTZ_CRON_TRIGGERS` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', '0/10 * * * * ?', 'Asia/Shanghai');
INSERT INTO `QRTZ_CRON_TRIGGERS` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', '0/15 * * * * ?', 'Asia/Shanghai');
INSERT INTO `QRTZ_CRON_TRIGGERS` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', '0/20 * * * * ?', 'Asia/Shanghai');

-- ----------------------------
-- Table structure for QRTZ_FIRED_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_FIRED_TRIGGERS`;
CREATE TABLE `QRTZ_FIRED_TRIGGERS`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `entry_id` varchar(95) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `instance_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `fired_time` bigint NOT NULL,
  `sched_time` bigint NOT NULL,
  `priority` int NOT NULL,
  `state` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `requests_recovery` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`sched_name`, `entry_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of QRTZ_FIRED_TRIGGERS
-- ----------------------------

-- ----------------------------
-- Table structure for QRTZ_JOB_DETAILS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_JOB_DETAILS`;
CREATE TABLE `QRTZ_JOB_DETAILS`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `description` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `job_class_name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `is_durable` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `is_update_data` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `requests_recovery` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `job_data` blob NULL,
  PRIMARY KEY (`sched_name`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of QRTZ_JOB_DETAILS
-- ----------------------------
INSERT INTO `QRTZ_JOB_DETAILS` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 'com.ysweet.quartz.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001F636F6D2E7973776565742E71756172747A2E646F6D61696E2E5379734A6F62000000000000000102000E4C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C0006706172616D7371007E00034C000672656D61726B71007E00094C000673746174757371007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000A78707400013174000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B5974190300007870770800000179A7FFC9C87874000E302F3130202A202A202A202A203F74001172795461736B2E72794E6F506172616D7374000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000001740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E697A0E58F82EFBC89740001337371007E00053F40000000000000770800000010000000007874000074000131740000707800);
INSERT INTO `QRTZ_JOB_DETAILS` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', NULL, 'com.ysweet.quartz.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001F636F6D2E7973776565742E71756172747A2E646F6D61696E2E5379734A6F62000000000000000102000E4C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C0006706172616D7371007E00034C000672656D61726B71007E00094C000673746174757371007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000A78707400013174000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B5974190300007870770800000179A7FFC9C87874000E302F3135202A202A202A202A203F74001572795461736B2E7279506172616D7328277279272974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000002740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E69C89E58F82EFBC89740001337371007E00053F40000000000000770800000010000000007874000074000131740000707800);
INSERT INTO `QRTZ_JOB_DETAILS` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', NULL, 'com.ysweet.quartz.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001F636F6D2E7973776565742E71756172747A2E646F6D61696E2E5379734A6F62000000000000000102000E4C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C0006706172616D7371007E00034C000672656D61726B71007E00094C000673746174757371007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000A78707400013174000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B5974190300007870770800000179A7FFC9C87874000E302F3230202A202A202A202A203F74003872795461736B2E72794D756C7469706C65506172616D7328277279272C20747275652C20323030304C2C203331362E3530442C203130302974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000003740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E5A49AE58F82EFBC89740001337371007E00053F40000000000000770800000010000000007874000074000131740000707800);

-- ----------------------------
-- Table structure for QRTZ_LOCKS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_LOCKS`;
CREATE TABLE `QRTZ_LOCKS`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `lock_name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`sched_name`, `lock_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of QRTZ_LOCKS
-- ----------------------------
INSERT INTO `QRTZ_LOCKS` VALUES ('RuoyiScheduler', 'STATE_ACCESS');
INSERT INTO `QRTZ_LOCKS` VALUES ('RuoyiScheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for QRTZ_PAUSED_TRIGGER_GRPS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_PAUSED_TRIGGER_GRPS`;
CREATE TABLE `QRTZ_PAUSED_TRIGGER_GRPS`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`sched_name`, `trigger_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of QRTZ_PAUSED_TRIGGER_GRPS
-- ----------------------------

-- ----------------------------
-- Table structure for QRTZ_SCHEDULER_STATE
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_SCHEDULER_STATE`;
CREATE TABLE `QRTZ_SCHEDULER_STATE`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `instance_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `last_checkin_time` bigint NOT NULL,
  `checkin_interval` bigint NOT NULL,
  PRIMARY KEY (`sched_name`, `instance_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of QRTZ_SCHEDULER_STATE
-- ----------------------------
INSERT INTO `QRTZ_SCHEDULER_STATE` VALUES ('RuoyiScheduler', 'XinleiPang1636355340458', 1636357564698, 15000);

-- ----------------------------
-- Table structure for QRTZ_SIMPLE_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_SIMPLE_TRIGGERS`;
CREATE TABLE `QRTZ_SIMPLE_TRIGGERS`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `repeat_count` bigint NOT NULL,
  `repeat_interval` bigint NOT NULL,
  `times_triggered` bigint NOT NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `QRTZ_SIMPLE_TRIGGERS_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `QRTZ_TRIGGERS` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of QRTZ_SIMPLE_TRIGGERS
-- ----------------------------

-- ----------------------------
-- Table structure for QRTZ_SIMPROP_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_SIMPROP_TRIGGERS`;
CREATE TABLE `QRTZ_SIMPROP_TRIGGERS`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `str_prop_1` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `str_prop_2` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `str_prop_3` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `int_prop_1` int NULL DEFAULT NULL,
  `int_prop_2` int NULL DEFAULT NULL,
  `long_prop_1` bigint NULL DEFAULT NULL,
  `long_prop_2` bigint NULL DEFAULT NULL,
  `dec_prop_1` decimal(13, 4) NULL DEFAULT NULL,
  `dec_prop_2` decimal(13, 4) NULL DEFAULT NULL,
  `bool_prop_1` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `bool_prop_2` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `QRTZ_SIMPROP_TRIGGERS_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `QRTZ_TRIGGERS` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of QRTZ_SIMPROP_TRIGGERS
-- ----------------------------

-- ----------------------------
-- Table structure for QRTZ_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_TRIGGERS`;
CREATE TABLE `QRTZ_TRIGGERS`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `description` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `next_fire_time` bigint NULL DEFAULT NULL,
  `prev_fire_time` bigint NULL DEFAULT NULL,
  `priority` int NULL DEFAULT NULL,
  `trigger_state` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `trigger_type` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `start_time` bigint NOT NULL,
  `end_time` bigint NULL DEFAULT NULL,
  `calendar_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `misfire_instr` smallint NULL DEFAULT NULL,
  `job_data` blob NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  INDEX `sched_name`(`sched_name`, `job_name`, `job_group`) USING BTREE,
  CONSTRAINT `QRTZ_TRIGGERS_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `QRTZ_JOB_DETAILS` (`sched_name`, `job_name`, `job_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of QRTZ_TRIGGERS
-- ----------------------------
INSERT INTO `QRTZ_TRIGGERS` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 1636355350000, -1, 5, 'PAUSED', 'CRON', 1636355341000, 0, NULL, 2, '');
INSERT INTO `QRTZ_TRIGGERS` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', 'TASK_CLASS_NAME2', 'DEFAULT', NULL, 1636355355000, -1, 5, 'PAUSED', 'CRON', 1636355341000, 0, NULL, 2, '');
INSERT INTO `QRTZ_TRIGGERS` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', 'TASK_CLASS_NAME3', 'DEFAULT', NULL, 1636355360000, -1, 5, 'PAUSED', 'CRON', 1636355342000, 0, NULL, 2, '');

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `table_id` bigint NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 56 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '代码生成业务表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES (54, 'ys_banner', 'banner管理表', NULL, NULL, 'YsBanner', 'crud', 'com.ruoyi.system', 'system', 'banner', 'banner管理', 'ruoyi', '0', '/', '{}', 'admin', '2021-06-21 10:47:22', 'admin', '2021-06-21 11:06:04', NULL);
INSERT INTO `gen_table` VALUES (55, 'ys_goods', '商品表', NULL, NULL, 'YsGoods', 'crud', 'com.ruoyi.system', 'system', 'goods', '商品', 'ruoyi', '0', '/', NULL, 'admin', '2021-07-08 14:07:45', NULL, '2021-07-08 14:07:45', NULL);

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `column_id` bigint NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '字典类型',
  `sort` int NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 606 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '代码生成业务表字段' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES (574, '54', 'id', NULL, 'int', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2021-06-21 11:04:45', 'admin', '2021-06-21 11:06:04');
INSERT INTO `gen_table_column` VALUES (575, '54', 'name', 'Banner名称，通常作为标识', 'varchar(50)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2021-06-21 11:04:45', 'admin', '2021-06-21 11:06:05');
INSERT INTO `gen_table_column` VALUES (576, '54', 'description', 'Banner描述', 'varchar(255)', 'String', 'description', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2021-06-21 11:04:45', 'admin', '2021-06-21 11:06:05');
INSERT INTO `gen_table_column` VALUES (577, '54', 'image_url', '广告的图片,不能为空', 'varchar(255)', 'String', 'imageUrl', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2021-06-21 11:04:45', 'admin', '2021-06-21 11:06:05');
INSERT INTO `gen_table_column` VALUES (578, '54', 'redrict_type', '跳转的类型', 'int', 'Integer', 'redrictType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 5, 'admin', '2021-06-21 11:04:46', 'admin', '2021-06-21 11:06:05');
INSERT INTO `gen_table_column` VALUES (579, '54', 'key_str', '关键字,不同跳转有不同作用,可能是商品的id,也可能是文章的链接等等', 'varchar(255)', 'String', 'keyStr', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2021-06-21 11:04:46', 'admin', '2021-06-21 11:06:05');
INSERT INTO `gen_table_column` VALUES (580, '54', 'group', '广告分类', 'int', 'Integer', 'group', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2021-06-21 11:04:46', 'admin', '2021-06-21 11:06:05');
INSERT INTO `gen_table_column` VALUES (581, '54', 'is_show', '是否显示广告,默认是1显示', 'bit(1)', 'Integer', 'isShow', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2021-06-21 11:04:46', 'admin', '2021-06-21 11:06:05');
INSERT INTO `gen_table_column` VALUES (582, '54', 'create_time', NULL, 'datetime', 'Date', 'createTime', '0', '0', '1', '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 9, 'admin', '2021-06-21 11:04:46', 'admin', '2021-06-21 11:06:05');
INSERT INTO `gen_table_column` VALUES (583, '54', 'update_time', NULL, 'int', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 10, 'admin', '2021-06-21 11:04:46', 'admin', '2021-06-21 11:06:05');
INSERT INTO `gen_table_column` VALUES (584, '54', 'is_del', '逻辑删除', 'int', 'Integer', 'isDel', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 11, 'admin', '2021-06-21 11:04:46', 'admin', '2021-06-21 11:06:05');
INSERT INTO `gen_table_column` VALUES (585, '55', 'goods_id', '商品id(SKU)', 'int unsigned', 'String', 'goodsId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', NULL, '', 1, 'admin', '2021-07-08 14:07:45', NULL, '2021-07-08 14:07:45');
INSERT INTO `gen_table_column` VALUES (586, '55', 'goods_name', '商品名称', 'varchar(100)', 'String', 'goodsName', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2021-07-08 14:07:45', NULL, '2021-07-08 14:07:45');
INSERT INTO `gen_table_column` VALUES (587, '55', 'category_id', '商品分类id', 'int unsigned', 'String', 'categoryId', '0', '0', '1', '1', '1', '1', '1', 'EQ', NULL, '', 3, 'admin', '2021-07-08 14:07:45', NULL, '2021-07-08 14:07:45');
INSERT INTO `gen_table_column` VALUES (588, '55', 'market_price', '市场价', 'decimal(10,2)', 'BigDecimal', 'marketPrice', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2021-07-08 14:07:45', NULL, '2021-07-08 14:07:45');
INSERT INTO `gen_table_column` VALUES (589, '55', 'price', '价格', 'decimal(10,2)', 'BigDecimal', 'price', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2021-07-08 14:07:45', NULL, '2021-07-08 14:07:45');
INSERT INTO `gen_table_column` VALUES (590, '55', 'sales', '销售数量', 'int unsigned', 'String', 'sales', '0', '0', '1', '1', '1', '1', '1', 'EQ', NULL, '', 6, 'admin', '2021-07-08 14:07:45', NULL, '2021-07-08 14:07:45');
INSERT INTO `gen_table_column` VALUES (591, '55', 'collects', '收藏数量', 'int unsigned', 'String', 'collects', '0', '0', '1', '1', '1', '1', '1', 'EQ', NULL, '', 7, 'admin', '2021-07-08 14:07:46', NULL, '2021-07-08 14:07:46');
INSERT INTO `gen_table_column` VALUES (592, '55', 'showImge', '商品主图(商品列表展示图片)', 'text', 'String', 'showImge', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'textarea', '', 8, 'admin', '2021-07-08 14:07:46', NULL, '2021-07-08 14:07:46');
INSERT INTO `gen_table_column` VALUES (593, '55', 'keywords', '商品关键词', 'varchar(255)', 'String', 'keywords', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2021-07-08 14:07:46', NULL, '2021-07-08 14:07:46');
INSERT INTO `gen_table_column` VALUES (594, '55', 'description', '商品简介，促销语', 'varchar(255)', 'String', 'description', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 10, 'admin', '2021-07-08 14:07:46', NULL, '2021-07-08 14:07:46');
INSERT INTO `gen_table_column` VALUES (595, '55', 'content', '商品详情', 'text', 'String', 'content', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'editor', '', 11, 'admin', '2021-07-08 14:07:46', NULL, '2021-07-08 14:07:46');
INSERT INTO `gen_table_column` VALUES (596, '55', 'detailimgs', '展示图片(详情页面滚动图片)', 'text', 'String', 'detailimgs', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', '', 12, 'admin', '2021-07-08 14:07:46', NULL, '2021-07-08 14:07:46');
INSERT INTO `gen_table_column` VALUES (597, '55', 'is_hot', '是否热销商品', 'int', 'Long', 'isHot', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 13, 'admin', '2021-07-08 14:07:46', NULL, '2021-07-08 14:07:46');
INSERT INTO `gen_table_column` VALUES (598, '55', 'is_recommend', '是否推荐', 'int', 'Long', 'isRecommend', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 14, 'admin', '2021-07-08 14:07:46', NULL, '2021-07-08 14:07:46');
INSERT INTO `gen_table_column` VALUES (599, '55', 'stock', '库存', 'int', 'Long', 'stock', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 15, 'admin', '2021-07-08 14:07:46', NULL, '2021-07-08 14:07:46');
INSERT INTO `gen_table_column` VALUES (600, '55', 'sort', '排序', 'int', 'Long', 'sort', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 16, 'admin', '2021-07-08 14:07:46', NULL, '2021-07-08 14:07:46');
INSERT INTO `gen_table_column` VALUES (601, '55', 'is_new', '是否新品', 'int', 'Long', 'isNew', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 17, 'admin', '2021-07-08 14:07:46', NULL, '2021-07-08 14:07:46');
INSERT INTO `gen_table_column` VALUES (602, '55', 'sold_out', '是否下架 1下架，0正常', 'tinyint', 'Long', 'soldOut', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 18, 'admin', '2021-07-08 14:07:47', NULL, '2021-07-08 14:07:47');
INSERT INTO `gen_table_column` VALUES (603, '55', 'create_time', '商品添加时间', 'datetime', 'Date', 'createTime', '0', '0', '1', '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 19, 'admin', '2021-07-08 14:07:47', NULL, '2021-07-08 14:07:47');
INSERT INTO `gen_table_column` VALUES (604, '55', 'update_time', '商品编辑时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 20, 'admin', '2021-07-08 14:07:47', NULL, '2021-07-08 14:07:47');
INSERT INTO `gen_table_column` VALUES (605, '55', 'is_del', '', 'int', 'Long', 'isDel', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 21, 'admin', '2021-07-08 14:07:47', NULL, '2021-07-08 14:07:47');

-- ----------------------------
-- Table structure for shop_categroy
-- ----------------------------
DROP TABLE IF EXISTS `shop_categroy`;
CREATE TABLE `shop_categroy`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '分类名称',
  `is_del` int NOT NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '商品分类' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of shop_categroy
-- ----------------------------
INSERT INTO `shop_categroy` VALUES (1, '666', 1);
INSERT INTO `shop_categroy` VALUES (2, '123123', 0);

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `config_id` int NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 102 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '参数配置表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2021-05-26 17:27:09', '', NULL, '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2021-05-26 17:27:09', '', NULL, '初始化密码 123456');
INSERT INTO `sys_config` VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2021-05-26 17:27:09', '', NULL, '深色主题theme-dark，浅色主题theme-light');
INSERT INTO `sys_config` VALUES (100, '微信小程序appid', 'wxapp_appId', 'wx6abdf8110eae455f', 'N', 'admin', '2021-06-08 16:25:53', '', NULL, NULL);
INSERT INTO `sys_config` VALUES (101, '小程序秘钥', 'wxapp_secret', '527e73e701b0fa9da7a194fece8bd507', 'N', 'admin', '2021-06-08 16:28:49', 'admin', '2021-06-08 16:28:56', NULL);

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint NULL DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '部门名称',
  `order_num` int NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 200 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '部门表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (100, 0, '0', '麦当劳北海店', 0, '小金木', '15994051442', 'dovi_mc@qq.com', '0', '0', 'admin', '2021-05-26 17:27:09', 'admin', '2021-05-28 15:21:20');
INSERT INTO `sys_dept` VALUES (101, 100, '0,100', '管理人员', 1, '店长', '15994051442', 'dovi_mc@qq.com', '0', '0', 'admin', '2021-05-26 17:27:09', 'admin', '2021-05-28 15:21:20');
INSERT INTO `sys_dept` VALUES (102, 100, '0,100', '长沙分公司', 2, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2021-05-26 17:27:09', '', NULL);
INSERT INTO `sys_dept` VALUES (103, 101, '0,100,101', '店长', 1, '小金木', '15994051442', 'dovi_mc@qq.com', '0', '0', 'admin', '2021-05-26 17:27:09', 'admin', '2021-05-28 15:21:19');
INSERT INTO `sys_dept` VALUES (104, 101, '0,100,101', '市场部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2021-05-26 17:27:09', '', NULL);
INSERT INTO `sys_dept` VALUES (105, 101, '0,100,101', '测试部门', 3, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2021-05-26 17:27:09', '', NULL);
INSERT INTO `sys_dept` VALUES (106, 101, '0,100,101', '财务部门', 4, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2021-05-26 17:27:09', '', NULL);
INSERT INTO `sys_dept` VALUES (107, 101, '0,100,101', '运维部门', 5, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2021-05-26 17:27:09', '', NULL);
INSERT INTO `sys_dept` VALUES (108, 102, '0,100,102', '市场部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2021-05-26 17:27:09', '', NULL);
INSERT INTO `sys_dept` VALUES (109, 102, '0,100,102', '财务部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2021-05-26 17:27:09', '', NULL);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` bigint NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 113 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '字典数据表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2021-05-26 17:27:09', '', NULL, '性别男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2021-05-26 17:27:09', '', NULL, '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2021-05-26 17:27:09', '', NULL, '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2021-05-26 17:27:09', '', NULL, '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2021-05-26 17:27:09', '', NULL, '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2021-05-26 17:27:09', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2021-05-26 17:27:09', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2021-05-26 17:27:09', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2021-05-26 17:27:09', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2021-05-26 17:27:09', '', NULL, '默认分组');
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2021-05-26 17:27:09', '', NULL, '系统分组');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2021-05-26 17:27:09', '', NULL, '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2021-05-26 17:27:09', '', NULL, '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2021-05-26 17:27:09', '', NULL, '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2021-05-26 17:27:09', '', NULL, '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2021-05-26 17:27:09', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2021-05-26 17:27:09', '', NULL, '关闭状态');
INSERT INTO `sys_dict_data` VALUES (18, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2021-05-26 17:27:09', '', NULL, '新增操作');
INSERT INTO `sys_dict_data` VALUES (19, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2021-05-26 17:27:09', '', NULL, '修改操作');
INSERT INTO `sys_dict_data` VALUES (20, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2021-05-26 17:27:09', '', NULL, '删除操作');
INSERT INTO `sys_dict_data` VALUES (21, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2021-05-26 17:27:09', '', NULL, '授权操作');
INSERT INTO `sys_dict_data` VALUES (22, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2021-05-26 17:27:09', '', NULL, '导出操作');
INSERT INTO `sys_dict_data` VALUES (23, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2021-05-26 17:27:09', '', NULL, '导入操作');
INSERT INTO `sys_dict_data` VALUES (24, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2021-05-26 17:27:09', '', NULL, '强退操作');
INSERT INTO `sys_dict_data` VALUES (25, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2021-05-26 17:27:09', '', NULL, '生成操作');
INSERT INTO `sys_dict_data` VALUES (26, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2021-05-26 17:27:09', '', NULL, '清空操作');
INSERT INTO `sys_dict_data` VALUES (27, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2021-05-26 17:27:09', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (28, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2021-05-26 17:27:09', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (100, 1, '首页广告', '1', 'banner_type', NULL, NULL, 'N', '0', 'admin', '2021-06-21 17:28:04', NULL, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (101, 2, '点餐页广告', '2', 'banner_type', NULL, NULL, 'N', '0', 'admin', '2021-06-21 17:28:35', NULL, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (103, 1, '商品', '1', 'redirect_type', NULL, NULL, 'N', '0', 'admin', '2021-06-21 17:48:23', 'admin', '2021-06-21 17:48:44', NULL);
INSERT INTO `sys_dict_data` VALUES (104, 2, '文章', '2', 'redirect_type', NULL, NULL, 'N', '0', 'admin', '2021-06-21 17:48:33', NULL, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (105, 0, '不跳转', '0', 'redirect_type', NULL, NULL, 'N', '0', 'admin', '2021-06-22 11:01:02', NULL, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (108, 1, '店铺优惠券', '1', 'coupon_type', NULL, NULL, 'N', '0', 'admin', '2021-07-05 14:20:12', NULL, NULL, '店铺优惠券');
INSERT INTO `sys_dict_data` VALUES (109, 2, '商品优惠券', '2', 'coupon_type', NULL, NULL, 'N', '0', 'admin', '2021-07-05 14:20:27', NULL, NULL, '商品优惠券');
INSERT INTO `sys_dict_data` VALUES (110, 0, '活动公告', '0', 'article_type', NULL, NULL, 'N', '0', 'admin', '2021-08-18 14:52:10', NULL, '2021-08-18 14:52:10', '活动公告');
INSERT INTO `sys_dict_data` VALUES (111, 1, '独立文章', '1', 'article_type', NULL, NULL, 'N', '0', 'admin', '2021-08-18 14:52:22', NULL, '2021-08-18 14:52:22', '独立文章');
INSERT INTO `sys_dict_data` VALUES (112, 2, '公告', '2', 'article_type', NULL, NULL, 'N', '0', 'admin', '2021-08-18 14:52:44', NULL, '2021-08-18 14:52:44', '公告');

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` bigint NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 108 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '字典类型表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2021-05-26 17:27:09', '', NULL, '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2021-05-26 17:27:09', '', NULL, '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2021-05-26 17:27:09', '', NULL, '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2021-05-26 17:27:09', '', NULL, '任务状态列表');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2021-05-26 17:27:09', '', NULL, '任务分组列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2021-05-26 17:27:09', '', NULL, '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2021-05-26 17:27:09', '', NULL, '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2021-05-26 17:27:09', '', NULL, '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2021-05-26 17:27:09', '', NULL, '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2021-05-26 17:27:09', '', NULL, '登录状态列表');
INSERT INTO `sys_dict_type` VALUES (101, '广告类型', 'banner_type', '0', 'admin', '2021-06-21 17:27:39', NULL, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (102, '跳转类型', 'redirect_type', '0', 'admin', '2021-06-21 17:48:03', NULL, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (105, '优惠券类型', 'coupon_type', '0', 'admin', '2021-07-05 14:19:42', 'admin', '2021-08-18 14:49:30', NULL);
INSERT INTO `sys_dict_type` VALUES (107, '文章类型', 'article_type', '0', 'admin', '2021-08-18 14:50:26', 'admin', '2021-08-18 14:51:44', '');

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `job_id` bigint NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '定时任务调度表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_job` VALUES (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_job` VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2021-05-26 17:27:09', '', NULL, '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
  `job_log_id` bigint NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '日志信息',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '异常信息',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '定时任务调度日志表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor`  (
  `info_id` bigint NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `user_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '用户账号',
  `ipaddr` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '操作系统',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '提示消息',
  `login_time` datetime NULL DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 228 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '系统访问记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
INSERT INTO `sys_logininfor` VALUES (100, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-05-26 17:46:18');
INSERT INTO `sys_logininfor` VALUES (101, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-05-26 19:47:52');
INSERT INTO `sys_logininfor` VALUES (102, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2021-05-27 15:27:08');
INSERT INTO `sys_logininfor` VALUES (103, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-05-27 15:27:27');
INSERT INTO `sys_logininfor` VALUES (104, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-05-27 16:37:00');
INSERT INTO `sys_logininfor` VALUES (105, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2021-05-27 17:46:35');
INSERT INTO `sys_logininfor` VALUES (106, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-05-27 17:46:41');
INSERT INTO `sys_logininfor` VALUES (107, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-05-28 08:11:22');
INSERT INTO `sys_logininfor` VALUES (108, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-05-28 15:09:30');
INSERT INTO `sys_logininfor` VALUES (109, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2021-05-28 16:11:07');
INSERT INTO `sys_logininfor` VALUES (110, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-05-28 16:11:12');
INSERT INTO `sys_logininfor` VALUES (111, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-05-28 16:14:16');
INSERT INTO `sys_logininfor` VALUES (112, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2021-05-28 18:10:47');
INSERT INTO `sys_logininfor` VALUES (113, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-05-28 18:10:52');
INSERT INTO `sys_logininfor` VALUES (114, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-05-29 10:28:48');
INSERT INTO `sys_logininfor` VALUES (115, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2021-05-29 10:29:04');
INSERT INTO `sys_logininfor` VALUES (116, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-05-29 10:29:34');
INSERT INTO `sys_logininfor` VALUES (117, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2021-05-29 10:30:38');
INSERT INTO `sys_logininfor` VALUES (118, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-06-01 14:51:21');
INSERT INTO `sys_logininfor` VALUES (119, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-06-08 16:21:36');
INSERT INTO `sys_logininfor` VALUES (120, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-06-09 16:30:08');
INSERT INTO `sys_logininfor` VALUES (121, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-06-10 16:17:05');
INSERT INTO `sys_logininfor` VALUES (122, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-06-11 10:49:09');
INSERT INTO `sys_logininfor` VALUES (123, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-06-11 12:11:52');
INSERT INTO `sys_logininfor` VALUES (124, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-06-11 15:17:32');
INSERT INTO `sys_logininfor` VALUES (125, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2021-06-11 15:57:08');
INSERT INTO `sys_logininfor` VALUES (126, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-06-17 14:55:06');
INSERT INTO `sys_logininfor` VALUES (127, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '1', '验证码已失效', '2021-06-17 17:34:54');
INSERT INTO `sys_logininfor` VALUES (128, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-06-17 17:35:00');
INSERT INTO `sys_logininfor` VALUES (129, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-06-18 08:57:55');
INSERT INTO `sys_logininfor` VALUES (130, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-06-18 10:22:43');
INSERT INTO `sys_logininfor` VALUES (131, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-06-18 11:55:25');
INSERT INTO `sys_logininfor` VALUES (132, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-06-18 13:28:55');
INSERT INTO `sys_logininfor` VALUES (133, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-06-18 14:33:20');
INSERT INTO `sys_logininfor` VALUES (134, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-06-21 08:41:08');
INSERT INTO `sys_logininfor` VALUES (135, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-06-21 10:00:58');
INSERT INTO `sys_logininfor` VALUES (136, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-06-21 10:01:26');
INSERT INTO `sys_logininfor` VALUES (137, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-06-21 11:03:42');
INSERT INTO `sys_logininfor` VALUES (138, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-06-21 11:57:27');
INSERT INTO `sys_logininfor` VALUES (139, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '1', '验证码错误', '2021-06-21 14:21:35');
INSERT INTO `sys_logininfor` VALUES (140, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '1', '验证码错误', '2021-06-21 14:21:38');
INSERT INTO `sys_logininfor` VALUES (141, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-06-21 14:21:43');
INSERT INTO `sys_logininfor` VALUES (142, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-06-21 17:26:12');
INSERT INTO `sys_logininfor` VALUES (143, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-06-21 18:41:15');
INSERT INTO `sys_logininfor` VALUES (144, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-06-22 08:46:18');
INSERT INTO `sys_logininfor` VALUES (145, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-06-22 09:14:47');
INSERT INTO `sys_logininfor` VALUES (146, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-06-22 09:46:51');
INSERT INTO `sys_logininfor` VALUES (147, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-06-22 10:42:08');
INSERT INTO `sys_logininfor` VALUES (148, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-06-22 10:42:26');
INSERT INTO `sys_logininfor` VALUES (149, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-06-22 11:33:04');
INSERT INTO `sys_logininfor` VALUES (150, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-06-22 14:40:58');
INSERT INTO `sys_logininfor` VALUES (151, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-06-23 16:10:01');
INSERT INTO `sys_logininfor` VALUES (152, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-06-24 09:44:24');
INSERT INTO `sys_logininfor` VALUES (153, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-06-24 10:30:36');
INSERT INTO `sys_logininfor` VALUES (154, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '1', '验证码错误', '2021-06-24 11:24:23');
INSERT INTO `sys_logininfor` VALUES (155, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-06-24 11:24:28');
INSERT INTO `sys_logininfor` VALUES (156, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-06-24 13:54:35');
INSERT INTO `sys_logininfor` VALUES (157, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-06-26 10:42:59');
INSERT INTO `sys_logininfor` VALUES (158, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-07-05 09:29:06');
INSERT INTO `sys_logininfor` VALUES (159, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-07-05 11:25:15');
INSERT INTO `sys_logininfor` VALUES (160, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-07-05 13:43:31');
INSERT INTO `sys_logininfor` VALUES (161, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-07-05 14:39:48');
INSERT INTO `sys_logininfor` VALUES (162, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '1', '验证码错误', '2021-07-05 16:08:36');
INSERT INTO `sys_logininfor` VALUES (163, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-07-05 16:08:40');
INSERT INTO `sys_logininfor` VALUES (164, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-07-06 08:52:34');
INSERT INTO `sys_logininfor` VALUES (165, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '1', '验证码错误', '2021-07-06 09:33:55');
INSERT INTO `sys_logininfor` VALUES (166, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-07-06 09:33:59');
INSERT INTO `sys_logininfor` VALUES (167, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-07-06 09:41:12');
INSERT INTO `sys_logininfor` VALUES (168, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '1', '验证码错误', '2021-07-06 15:40:07');
INSERT INTO `sys_logininfor` VALUES (169, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-07-06 15:40:11');
INSERT INTO `sys_logininfor` VALUES (170, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-07-06 17:29:02');
INSERT INTO `sys_logininfor` VALUES (171, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-07-07 09:57:43');
INSERT INTO `sys_logininfor` VALUES (172, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-07-07 12:30:15');
INSERT INTO `sys_logininfor` VALUES (173, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-07-07 14:48:11');
INSERT INTO `sys_logininfor` VALUES (174, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '1', '验证码错误', '2021-07-08 08:57:21');
INSERT INTO `sys_logininfor` VALUES (175, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-07-08 08:57:26');
INSERT INTO `sys_logininfor` VALUES (176, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-07-08 11:02:38');
INSERT INTO `sys_logininfor` VALUES (177, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-07-08 14:05:50');
INSERT INTO `sys_logininfor` VALUES (178, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-07-08 16:17:37');
INSERT INTO `sys_logininfor` VALUES (179, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-07-08 17:19:20');
INSERT INTO `sys_logininfor` VALUES (180, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '1', '验证码已失效', '2021-07-09 09:05:34');
INSERT INTO `sys_logininfor` VALUES (181, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-07-09 09:07:13');
INSERT INTO `sys_logininfor` VALUES (182, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2021-07-09 09:17:45');
INSERT INTO `sys_logininfor` VALUES (183, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-07-09 09:17:51');
INSERT INTO `sys_logininfor` VALUES (184, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-07-09 11:09:18');
INSERT INTO `sys_logininfor` VALUES (185, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-07-09 14:22:06');
INSERT INTO `sys_logininfor` VALUES (186, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '1', '验证码已失效', '2021-07-10 08:51:16');
INSERT INTO `sys_logininfor` VALUES (187, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-07-10 08:51:20');
INSERT INTO `sys_logininfor` VALUES (188, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '1', '验证码错误', '2021-07-10 10:37:07');
INSERT INTO `sys_logininfor` VALUES (189, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '1', '验证码错误', '2021-07-10 10:37:11');
INSERT INTO `sys_logininfor` VALUES (190, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '1', '验证码错误', '2021-07-10 10:37:12');
INSERT INTO `sys_logininfor` VALUES (191, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '1', '验证码错误', '2021-07-10 10:37:22');
INSERT INTO `sys_logininfor` VALUES (192, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-07-10 10:38:24');
INSERT INTO `sys_logininfor` VALUES (193, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-07-10 11:31:37');
INSERT INTO `sys_logininfor` VALUES (194, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '1', '用户不存在/密码错误', '2021-07-10 15:49:12');
INSERT INTO `sys_logininfor` VALUES (195, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '1', '用户不存在/密码错误', '2021-07-10 15:49:20');
INSERT INTO `sys_logininfor` VALUES (196, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-07-10 15:49:27');
INSERT INTO `sys_logininfor` VALUES (197, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '退出成功', '2021-07-10 15:49:49');
INSERT INTO `sys_logininfor` VALUES (198, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-07-10 15:51:09');
INSERT INTO `sys_logininfor` VALUES (199, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-07-12 09:22:22');
INSERT INTO `sys_logininfor` VALUES (200, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '1', '验证码错误', '2021-07-12 11:03:29');
INSERT INTO `sys_logininfor` VALUES (201, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-07-12 11:03:34');
INSERT INTO `sys_logininfor` VALUES (202, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-07-12 12:01:20');
INSERT INTO `sys_logininfor` VALUES (203, 'admin', '192.168.1.93', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '1', '验证码已失效', '2021-07-12 14:30:21');
INSERT INTO `sys_logininfor` VALUES (204, 'admin', '192.168.1.93', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '1', '验证码错误', '2021-07-12 14:30:23');
INSERT INTO `sys_logininfor` VALUES (205, 'admin', '192.168.1.93', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '1', '验证码错误', '2021-07-12 14:31:13');
INSERT INTO `sys_logininfor` VALUES (206, 'admin', '192.168.1.93', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-07-12 14:31:18');
INSERT INTO `sys_logininfor` VALUES (207, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '1', '验证码已失效', '2021-07-12 15:22:40');
INSERT INTO `sys_logininfor` VALUES (208, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-07-12 15:22:45');
INSERT INTO `sys_logininfor` VALUES (209, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-07-13 08:51:50');
INSERT INTO `sys_logininfor` VALUES (210, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-07-13 15:15:04');
INSERT INTO `sys_logininfor` VALUES (211, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '1', '验证码错误', '2021-07-13 18:03:12');
INSERT INTO `sys_logininfor` VALUES (212, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-07-13 18:03:18');
INSERT INTO `sys_logininfor` VALUES (213, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-08-13 09:48:05');
INSERT INTO `sys_logininfor` VALUES (214, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '1', '验证码错误', '2021-08-13 13:33:42');
INSERT INTO `sys_logininfor` VALUES (215, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-08-13 13:33:49');
INSERT INTO `sys_logininfor` VALUES (216, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-08-13 15:25:50');
INSERT INTO `sys_logininfor` VALUES (217, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-08-16 14:32:52');
INSERT INTO `sys_logininfor` VALUES (218, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-08-16 15:56:58');
INSERT INTO `sys_logininfor` VALUES (219, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-08-17 13:52:02');
INSERT INTO `sys_logininfor` VALUES (220, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-08-17 16:15:58');
INSERT INTO `sys_logininfor` VALUES (221, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-08-18 10:01:56');
INSERT INTO `sys_logininfor` VALUES (222, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-08-18 14:06:08');
INSERT INTO `sys_logininfor` VALUES (223, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-08-19 09:22:58');
INSERT INTO `sys_logininfor` VALUES (224, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-08-20 17:22:50');
INSERT INTO `sys_logininfor` VALUES (225, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-10-14 16:33:13');
INSERT INTO `sys_logininfor` VALUES (226, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-10-14 17:05:46');
INSERT INTO `sys_logininfor` VALUES (227, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-10-14 17:40:02');
INSERT INTO `sys_logininfor` VALUES (228, 'admin', '127.0.0.1', '内网IP', 'MSEdge', 'Windows 10 or Windows Server 2016', '0', '登录成功', '2021-11-08 15:22:33');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '菜单名称',
  `parent_id` bigint NULL DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int NULL DEFAULT 0 COMMENT '显示顺序',
  `path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '组件路径',
  `is_frame` int NULL DEFAULT 1 COMMENT '是否为外链（0是 1否）',
  `is_cache` int NULL DEFAULT 0 COMMENT '是否缓存（0缓存 1不缓存）',
  `menu_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2031 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '菜单权限表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 1, 'system', NULL, 1, 0, 'M', '0', '0', '', 'system', 'admin', '2021-05-26 17:27:09', '', NULL, '系统管理目录');
INSERT INTO `sys_menu` VALUES (2, '系统监控', 0, 2, 'monitor', NULL, 1, 0, 'M', '0', '0', '', 'monitor', 'admin', '2021-05-26 17:27:09', '', NULL, '系统监控目录');
INSERT INTO `sys_menu` VALUES (3, '系统工具', 0, 3, 'tool', NULL, 1, 0, 'M', '0', '0', '', 'tool', 'admin', '2021-05-26 17:27:09', '', NULL, '系统工具目录');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 1, 'user', 'system/user/index', 1, 0, 'C', '0', '0', 'system:user:list', 'user', 'admin', '2021-05-26 17:27:09', '', NULL, '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 2, 'role', 'system/role/index', 1, 0, 'C', '0', '0', 'system:role:list', 'peoples', 'admin', '2021-05-26 17:27:09', '', NULL, '角色管理菜单');
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 1, 3, 'menu', 'system/menu/index', 1, 0, 'C', '0', '0', 'system:menu:list', 'tree-table', 'admin', '2021-05-26 17:27:09', '', NULL, '菜单管理菜单');
INSERT INTO `sys_menu` VALUES (103, '部门管理', 1, 4, 'dept', 'system/dept/index', 1, 0, 'C', '0', '0', 'system:dept:list', 'tree', 'admin', '2021-05-26 17:27:09', '', NULL, '部门管理菜单');
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 1, 5, 'post', 'system/post/index', 1, 0, 'C', '0', '0', 'system:post:list', 'post', 'admin', '2021-05-26 17:27:09', '', NULL, '岗位管理菜单');
INSERT INTO `sys_menu` VALUES (105, '字典管理', 1, 6, 'dict', 'system/dict/index', 1, 0, 'C', '0', '0', 'system:dict:list', 'dict', 'admin', '2021-05-26 17:27:09', '', NULL, '字典管理菜单');
INSERT INTO `sys_menu` VALUES (106, '参数设置', 1, 7, 'config', 'system/config/index', 1, 0, 'C', '0', '0', 'system:config:list', 'edit', 'admin', '2021-05-26 17:27:09', '', NULL, '参数设置菜单');
INSERT INTO `sys_menu` VALUES (107, '通知公告', 1, 8, 'notice', 'system/notice/index', 1, 0, 'C', '0', '0', 'system:notice:list', 'message', 'admin', '2021-05-26 17:27:09', '', NULL, '通知公告菜单');
INSERT INTO `sys_menu` VALUES (108, '日志管理', 1, 9, 'log', '', 1, 0, 'M', '0', '0', '', 'log', 'admin', '2021-05-26 17:27:09', '', NULL, '日志管理菜单');
INSERT INTO `sys_menu` VALUES (109, '在线用户', 2, 1, 'online', 'monitor/online/index', 1, 0, 'C', '0', '0', 'monitor:online:list', 'online', 'admin', '2021-05-26 17:27:09', '', NULL, '在线用户菜单');
INSERT INTO `sys_menu` VALUES (110, '定时任务', 2, 2, 'job', 'monitor/job/index', 1, 0, 'C', '0', '0', 'monitor:job:list', 'job', 'admin', '2021-05-26 17:27:09', '', NULL, '定时任务菜单');
INSERT INTO `sys_menu` VALUES (111, '数据监控', 2, 3, 'druid', 'monitor/druid/index', 1, 0, 'C', '0', '0', 'monitor:druid:list', 'druid', 'admin', '2021-05-26 17:27:09', '', NULL, '数据监控菜单');
INSERT INTO `sys_menu` VALUES (112, '服务监控', 2, 4, 'server', 'monitor/server/index', 1, 0, 'C', '0', '0', 'monitor:server:list', 'server', 'admin', '2021-05-26 17:27:09', '', NULL, '服务监控菜单');
INSERT INTO `sys_menu` VALUES (113, '缓存监控', 2, 5, 'cache', 'monitor/cache/index', 1, 0, 'C', '0', '0', 'monitor:cache:list', 'redis', 'admin', '2021-05-26 17:27:09', '', NULL, '缓存监控菜单');
INSERT INTO `sys_menu` VALUES (114, '表单构建', 3, 1, 'build', 'tool/build/index', 1, 0, 'C', '0', '0', 'tool:build:list', 'build', 'admin', '2021-05-26 17:27:09', '', NULL, '表单构建菜单');
INSERT INTO `sys_menu` VALUES (115, '代码生成', 3, 2, 'gen', 'tool/gen/index', 1, 0, 'C', '0', '0', 'tool:gen:list', 'code', 'admin', '2021-05-26 17:27:09', '', NULL, '代码生成菜单');
INSERT INTO `sys_menu` VALUES (116, '系统接口', 3, 3, 'swagger', 'tool/swagger/index', 1, 0, 'C', '0', '0', 'tool:swagger:list', 'swagger', 'admin', '2021-05-26 17:27:09', '', NULL, '系统接口菜单');
INSERT INTO `sys_menu` VALUES (500, '操作日志', 108, 1, 'operlog', 'monitor/operlog/index', 1, 0, 'C', '0', '0', 'monitor:operlog:list', 'form', 'admin', '2021-05-26 17:27:09', '', NULL, '操作日志菜单');
INSERT INTO `sys_menu` VALUES (501, '登录日志', 108, 2, 'logininfor', 'monitor/logininfor/index', 1, 0, 'C', '0', '0', 'monitor:logininfor:list', 'logininfor', 'admin', '2021-05-26 17:27:09', '', NULL, '登录日志菜单');
INSERT INTO `sys_menu` VALUES (1001, '用户查询', 100, 1, '', '', 1, 0, 'F', '0', '0', 'system:user:query', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1002, '用户新增', 100, 2, '', '', 1, 0, 'F', '0', '0', 'system:user:add', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1003, '用户修改', 100, 3, '', '', 1, 0, 'F', '0', '0', 'system:user:edit', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1004, '用户删除', 100, 4, '', '', 1, 0, 'F', '0', '0', 'system:user:remove', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1005, '用户导出', 100, 5, '', '', 1, 0, 'F', '0', '0', 'system:user:export', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1006, '用户导入', 100, 6, '', '', 1, 0, 'F', '0', '0', 'system:user:import', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1007, '重置密码', 100, 7, '', '', 1, 0, 'F', '0', '0', 'system:user:resetPwd', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1008, '角色查询', 101, 1, '', '', 1, 0, 'F', '0', '0', 'system:role:query', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1009, '角色新增', 101, 2, '', '', 1, 0, 'F', '0', '0', 'system:role:add', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1010, '角色修改', 101, 3, '', '', 1, 0, 'F', '0', '0', 'system:role:edit', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1011, '角色删除', 101, 4, '', '', 1, 0, 'F', '0', '0', 'system:role:remove', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1012, '角色导出', 101, 5, '', '', 1, 0, 'F', '0', '0', 'system:role:export', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1013, '菜单查询', 102, 1, '', '', 1, 0, 'F', '0', '0', 'system:menu:query', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1014, '菜单新增', 102, 2, '', '', 1, 0, 'F', '0', '0', 'system:menu:add', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1015, '菜单修改', 102, 3, '', '', 1, 0, 'F', '0', '0', 'system:menu:edit', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1016, '菜单删除', 102, 4, '', '', 1, 0, 'F', '0', '0', 'system:menu:remove', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1017, '部门查询', 103, 1, '', '', 1, 0, 'F', '0', '0', 'system:dept:query', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1018, '部门新增', 103, 2, '', '', 1, 0, 'F', '0', '0', 'system:dept:add', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1019, '部门修改', 103, 3, '', '', 1, 0, 'F', '0', '0', 'system:dept:edit', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1020, '部门删除', 103, 4, '', '', 1, 0, 'F', '0', '0', 'system:dept:remove', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1021, '岗位查询', 104, 1, '', '', 1, 0, 'F', '0', '0', 'system:post:query', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1022, '岗位新增', 104, 2, '', '', 1, 0, 'F', '0', '0', 'system:post:add', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1023, '岗位修改', 104, 3, '', '', 1, 0, 'F', '0', '0', 'system:post:edit', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1024, '岗位删除', 104, 4, '', '', 1, 0, 'F', '0', '0', 'system:post:remove', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1025, '岗位导出', 104, 5, '', '', 1, 0, 'F', '0', '0', 'system:post:export', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1026, '字典查询', 105, 1, '#', '', 1, 0, 'F', '0', '0', 'system:dict:query', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1027, '字典新增', 105, 2, '#', '', 1, 0, 'F', '0', '0', 'system:dict:add', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1028, '字典修改', 105, 3, '#', '', 1, 0, 'F', '0', '0', 'system:dict:edit', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1029, '字典删除', 105, 4, '#', '', 1, 0, 'F', '0', '0', 'system:dict:remove', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1030, '字典导出', 105, 5, '#', '', 1, 0, 'F', '0', '0', 'system:dict:export', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1031, '参数查询', 106, 1, '#', '', 1, 0, 'F', '0', '0', 'system:config:query', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1032, '参数新增', 106, 2, '#', '', 1, 0, 'F', '0', '0', 'system:config:add', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1033, '参数修改', 106, 3, '#', '', 1, 0, 'F', '0', '0', 'system:config:edit', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1034, '参数删除', 106, 4, '#', '', 1, 0, 'F', '0', '0', 'system:config:remove', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1035, '参数导出', 106, 5, '#', '', 1, 0, 'F', '0', '0', 'system:config:export', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1036, '公告查询', 107, 1, '#', '', 1, 0, 'F', '0', '0', 'system:notice:query', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1037, '公告新增', 107, 2, '#', '', 1, 0, 'F', '0', '0', 'system:notice:add', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1038, '公告修改', 107, 3, '#', '', 1, 0, 'F', '0', '0', 'system:notice:edit', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1039, '公告删除', 107, 4, '#', '', 1, 0, 'F', '0', '0', 'system:notice:remove', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1040, '操作查询', 500, 1, '#', '', 1, 0, 'F', '0', '0', 'monitor:operlog:query', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1041, '操作删除', 500, 2, '#', '', 1, 0, 'F', '0', '0', 'monitor:operlog:remove', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1042, '日志导出', 500, 4, '#', '', 1, 0, 'F', '0', '0', 'monitor:operlog:export', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1043, '登录查询', 501, 1, '#', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:query', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1044, '登录删除', 501, 2, '#', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:remove', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1045, '日志导出', 501, 3, '#', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:export', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1046, '在线查询', 109, 1, '#', '', 1, 0, 'F', '0', '0', 'monitor:online:query', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1047, '批量强退', 109, 2, '#', '', 1, 0, 'F', '0', '0', 'monitor:online:batchLogout', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1048, '单条强退', 109, 3, '#', '', 1, 0, 'F', '0', '0', 'monitor:online:forceLogout', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1049, '任务查询', 110, 1, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:query', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1050, '任务新增', 110, 2, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:add', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1051, '任务修改', 110, 3, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:edit', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1052, '任务删除', 110, 4, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:remove', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1053, '状态修改', 110, 5, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:changeStatus', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1054, '任务导出', 110, 7, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:export', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1055, '生成查询', 115, 1, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:query', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1056, '生成修改', 115, 2, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:edit', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1057, '生成删除', 115, 3, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:remove', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1058, '导入代码', 115, 2, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:import', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1059, '预览代码', 115, 4, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:preview', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1060, '生成代码', 115, 5, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:code', '#', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2012, '商铺管理', 0, 0, 'shop', NULL, 1, 0, 'M', '0', '0', NULL, 'shopping', 'admin', '2021-06-11 10:57:53', 'admin', '2021-06-11 10:59:07', '');
INSERT INTO `sys_menu` VALUES (2019, '广告管理', 2012, 2, 'banner', 'shop/banner/index', 1, 0, 'C', '0', '0', NULL, 'edit', 'admin', '2021-06-17 17:42:44', 'admin', '2021-06-17 17:43:25', '');
INSERT INTO `sys_menu` VALUES (2020, '店铺设置', 2012, 2, 'config', 'shop/config/index', 1, 0, 'C', '0', '0', NULL, 'swagger', 'admin', '2021-06-23 17:01:52', 'admin', '2021-07-09 09:08:24', '');
INSERT INTO `sys_menu` VALUES (2021, '优惠券', 2012, 3, 'coupon', 'shop/coupon/index', 1, 0, 'C', '0', '0', NULL, 'dict', 'admin', '2021-06-26 11:16:53', 'admin', '2021-07-09 09:08:33', '');
INSERT INTO `sys_menu` VALUES (2022, '商品分类', 2012, 4, 'category', 'shop/category/index', 1, 0, 'C', '0', '0', NULL, 'dict', 'admin', '2021-07-06 17:30:10', 'admin', '2021-07-09 09:08:41', '');
INSERT INTO `sys_menu` VALUES (2023, '商品管理', 2012, 5, 'goods', 'shop/goods/index', 1, 0, 'C', '0', '0', NULL, 'shopping', 'admin', '2021-07-08 11:05:14', 'admin', '2021-07-09 09:15:23', '');
INSERT INTO `sys_menu` VALUES (2024, '商品添加', 2012, 6, 'goodsAdd', 'shop/goods/GoodsAdd', 1, 1, 'C', '1', '0', NULL, 'build', 'admin', '2021-07-09 09:10:41', 'admin', '2021-07-12 17:51:28', '');
INSERT INTO `sys_menu` VALUES (2025, '商品修改', 2012, 7, 'goodsUpdate', 'shop/goods/GoodsAdd', 1, 1, 'C', '1', '0', NULL, 'checkbox', 'admin', '2021-08-13 16:41:19', 'admin', '2021-08-13 16:42:46', '');
INSERT INTO `sys_menu` VALUES (2026, '文章管理', 2012, 8, 'article', 'shop/article/index', 1, 0, 'C', '0', '0', 'shop:article:list', 'education', 'admin', '2021-08-18 14:07:46', 'admin', '2021-08-18 15:52:14', '');
INSERT INTO `sys_menu` VALUES (2027, '新增文章', 2012, 9, 'articleAdd', 'shop/article/articleAdd', 1, 1, 'C', '1', '0', 'shop:article:update', '#', 'admin', '2021-08-18 15:13:42', 'admin', '2021-08-18 15:51:21', '');
INSERT INTO `sys_menu` VALUES (2028, '文章修改', 2012, 10, 'articleUpdate', 'shop/article/articleAdd', 1, 1, 'C', '1', '0', NULL, '#', 'admin', '2021-08-19 10:01:21', NULL, '2021-08-19 10:01:21', '');
INSERT INTO `sys_menu` VALUES (2029, '餐桌管理', 2012, 11, 'desk', 'shop/desk/index', 1, 0, 'C', '0', '0', NULL, 'example', 'admin', '2021-10-14 17:08:48', NULL, '2021-10-14 17:08:48', '');
INSERT INTO `sys_menu` VALUES (2030, '订单管理', 2012, 12, 'order', 'shop/order/index', 1, 0, 'C', '0', '0', NULL, 'dict', 'admin', '2021-10-14 17:42:03', 'admin', '2021-10-14 17:43:09', '');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `notice_id` int NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '公告标题',
  `notice_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` longblob NULL COMMENT '公告内容',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '通知公告表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES (1, '温馨提醒：2018-07-01 若依新版本发布啦', '2', 0x3C703E37373737373737373737373737373737373737373737373737373737373737373737373C2F703E, '0', 'admin', '2021-05-26 17:27:09', 'admin', '2021-05-28 15:23:36', '管理员');
INSERT INTO `sys_notice` VALUES (2, '维护通知：2018-07-01 若依系统凌晨维护', '1', 0xE7BBB4E68AA4E58685E5AEB9, '0', 'admin', '2021-05-26 17:27:09', '', NULL, '管理员');

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `oper_id` bigint NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '模块标题',
  `business_type` int NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '请求方式',
  `operator_type` int NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '返回参数',
  `status` int NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime NULL DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 163 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '操作日志记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES (1, '操作日志', 9, 'com.ysweet.web.controller.monitor.SysOperlogController.clean()', 'DELETE', 1, 'admin', NULL, '/monitor/operlog/clean', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-05-28 15:25:22');
INSERT INTO `sys_oper_log` VALUES (2, '商品分类', 1, 'com.ysweet.shop.controller.CategroyController.add()', 'POST', 1, 'admin', NULL, '/shop/categroy', '127.0.0.1', '内网IP', '{\"name\":\"123123\",\"id\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-05-28 18:11:17');
INSERT INTO `sys_oper_log` VALUES (3, '代码生成', 6, 'com.ysweet.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'yx_user', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-06-01 14:51:55');
INSERT INTO `sys_oper_log` VALUES (4, '代码生成', 6, 'com.ysweet.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'yx_user', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-06-01 14:51:56');
INSERT INTO `sys_oper_log` VALUES (5, '代码生成', 6, 'com.ysweet.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'yx_user', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-06-01 14:51:57');
INSERT INTO `sys_oper_log` VALUES (6, '代码生成', 3, 'com.ysweet.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/tool/gen/14', '127.0.0.1', '内网IP', '{tableIds=14}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-06-01 14:52:02');
INSERT INTO `sys_oper_log` VALUES (7, '代码生成', 3, 'com.ysweet.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/tool/gen/13', '127.0.0.1', '内网IP', '{tableIds=13}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-06-01 14:52:05');
INSERT INTO `sys_oper_log` VALUES (8, '代码生成', 8, 'com.ysweet.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', 0, NULL, '2021-06-01 14:52:12');
INSERT INTO `sys_oper_log` VALUES (9, '参数管理', 1, 'com.ysweet.web.controller.system.SysConfigController.add()', 'POST', 1, 'admin', NULL, '/system/config', '127.0.0.1', '内网IP', '{\"configName\":\"微信小程序appid\",\"configKey\":\"wxapp_appId\",\"createBy\":\"admin\",\"configType\":\"N\",\"configValue\":\"wx6abdf8110eae455f\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-06-08 16:25:53');
INSERT INTO `sys_oper_log` VALUES (10, '参数管理', 1, 'com.ysweet.web.controller.system.SysConfigController.add()', 'POST', 1, 'admin', NULL, '/system/config', '127.0.0.1', '内网IP', '{\"configName\":\"小程序秘钥\",\"configKey\":\"wxapp_secret\",\"createBy\":\"admin\",\"configType\":\"Y\",\"configValue\":\"527e73e701b0fa9da7a194fece8bd507\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-06-08 16:28:49');
INSERT INTO `sys_oper_log` VALUES (11, '参数管理', 2, 'com.ysweet.web.controller.system.SysConfigController.edit()', 'PUT', 1, 'admin', NULL, '/system/config', '127.0.0.1', '内网IP', '{\"configName\":\"小程序秘钥\",\"configKey\":\"wxapp_secret\",\"createBy\":\"admin\",\"createTime\":1623140929000,\"updateBy\":\"admin\",\"configId\":101,\"configType\":\"N\",\"configValue\":\"527e73e701b0fa9da7a194fece8bd507\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-06-08 16:28:56');
INSERT INTO `sys_oper_log` VALUES (12, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', '', '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', 0, '', '2021-06-09 16:37:24');
INSERT INTO `sys_oper_log` VALUES (13, '参数管理', 9, 'com.ruoyi.web.controller.system.SysConfigController.refreshCache()', 'DELETE', 1, 'admin', '', '/system/config/refreshCache', '127.0.0.1', '内网IP', '{}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-10 16:45:56');
INSERT INTO `sys_oper_log` VALUES (14, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', '', '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"PangXinlei\",\"columns\":[{\"capJavaField\":\"Uid\",\"usableColumn\":false,\"columnId\":110,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"uid\",\"edit\":false,\"query\":false,\"columnComment\":\"用户id\",\"updateTime\":1623380162867,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint unsigned\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1622530313000,\"tableId\":12,\"pk\":true,\"columnName\":\"uid\"},{\"capJavaField\":\"Username\",\"usableColumn\":false,\"columnId\":111,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"username\",\"htmlType\":\"input\",\"edit\":false,\"query\":true,\"columnComment\":\"用户账户(跟accout一样)\",\"isQuery\":\"1\",\"updateTime\":1623380162976,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1622530313000,\"tableId\":12,\"pk\":false,\"columnName\":\"username\"},{\"capJavaField\":\"Password\",\"usableColumn\":false,\"columnId\":112,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"password\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"用户密码（跟pwd）\",\"updateTime\":1623380163084,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1622530313000,\"isEdit\":\"1\",\"tableId\":12,\"pk\":false,\"columnName\":\"password\"},{\"capJavaField\":\"Mark\",\"usableColumn\":false,\"columnId\":113,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"mark\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"用户备注\",\"updateTime\":1623380163196,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"S', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-11 10:56:05');
INSERT INTO `sys_oper_log` VALUES (15, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"shopping\",\"orderNum\":\"0\",\"menuName\":\"商铺管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"/\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1623380273100,\"isFrame\":\"1\",\"menuId\":2012,\"menuType\":\"M\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-11 10:57:53');
INSERT INTO `sys_oper_log` VALUES (16, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', '', '/system/role', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":1,\"admin\":true,\"remark\":\"超级管理员\",\"dataScope\":\"1\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"1\",\"deptCheckStrictly\":true,\"createBy\":\"admin\",\"createTime\":1622021229000,\"updateBy\":\"\",\"menuCheckStrictly\":true,\"roleKey\":\"admin\",\"roleName\":\"超级管理员\",\"menuIds\":[2012,1,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016,103,1017,1018,1019,1020,104,1021,1022,1023,1024,1025,105,1026,1027,1028,1029,1030,106,1031,1032,1033,1034,1035,107,1036,1037,1038,1039,108,500,1040,1041,1042,501,1043,1044,1045,2,109,1046,1047,1048,110,1049,1050,1051,1052,1053,1054,111,112,113,3,114,2006,2007,2008,2009,2010,2011,115,1055,1058,1056,1057,1059,1060,116],\"status\":\"0\"}', 'null', 1, '不允许操作超级管理员角色', '2021-06-11 10:58:23');
INSERT INTO `sys_oper_log` VALUES (17, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"shopping\",\"orderNum\":\"0\",\"menuName\":\"商铺管理\",\"remark\":\"\",\"updateTime\":1623380347393,\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"shop\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1623380273000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2012,\"menuType\":\"M\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-11 10:59:08');
INSERT INTO `sys_oper_log` VALUES (18, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', '', '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"PangXinlei\",\"columns\":[{\"capJavaField\":\"Uid\",\"usableColumn\":false,\"columnId\":110,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"uid\",\"edit\":false,\"query\":false,\"columnComment\":\"用户id\",\"updateTime\":1623380163000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint unsigned\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1622530313000,\"tableId\":12,\"pk\":true,\"columnName\":\"uid\"},{\"capJavaField\":\"Username\",\"usableColumn\":false,\"columnId\":111,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"username\",\"htmlType\":\"input\",\"edit\":false,\"query\":true,\"columnComment\":\"用户账户(跟accout一样)\",\"isQuery\":\"1\",\"updateTime\":1623380163000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1622530313000,\"tableId\":12,\"pk\":false,\"columnName\":\"username\"},{\"capJavaField\":\"Password\",\"usableColumn\":false,\"columnId\":112,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"password\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"用户密码（跟pwd）\",\"updateTime\":1623380163000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1622530313000,\"isEdit\":\"1\",\"tableId\":12,\"pk\":false,\"columnName\":\"password\"},{\"capJavaField\":\"Mark\",\"usableColumn\":false,\"columnId\":113,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"mark\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"用户备注\",\"updateTime\":1623380163000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"S', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-11 11:01:48');
INSERT INTO `sys_oper_log` VALUES (19, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', '', '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', 0, '', '2021-06-11 11:03:12');
INSERT INTO `sys_oper_log` VALUES (20, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"people\",\"orderNum\":\"1\",\"menuName\":\"会员管理\",\"remark\":\"用户菜单\",\"updateTime\":1623382128405,\"params\":{},\"parentId\":2012,\"isCache\":\"0\",\"path\":\"user\",\"component\":\"shop/user/index\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1623381910000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2013,\"menuType\":\"C\",\"perms\":\"shop:user:list\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-11 11:28:49');
INSERT INTO `sys_oper_log` VALUES (21, '菜单管理', 3, 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', '', '/system/menu/2007', '127.0.0.1', '内网IP', '{menuId=2007}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-17 14:57:04');
INSERT INTO `sys_oper_log` VALUES (22, '菜单管理', 3, 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', '', '/system/menu/2008', '127.0.0.1', '内网IP', '{menuId=2008}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-17 14:57:09');
INSERT INTO `sys_oper_log` VALUES (23, '菜单管理', 3, 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', '', '/system/menu/2009', '127.0.0.1', '内网IP', '{menuId=2009}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-17 14:57:12');
INSERT INTO `sys_oper_log` VALUES (24, '菜单管理', 3, 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', '', '/system/menu/2010', '127.0.0.1', '内网IP', '{menuId=2010}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-17 14:57:15');
INSERT INTO `sys_oper_log` VALUES (25, '菜单管理', 3, 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', '', '/system/menu/2011', '127.0.0.1', '内网IP', '{menuId=2011}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-17 14:57:18');
INSERT INTO `sys_oper_log` VALUES (26, '菜单管理', 3, 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', '', '/system/menu/2006', '127.0.0.1', '内网IP', '{menuId=2006}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-17 14:57:23');
INSERT INTO `sys_oper_log` VALUES (27, '代码生成', 3, 'com.ruoyi.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', '', '/tool/gen/11', '127.0.0.1', '内网IP', '{tableIds=11}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-17 15:03:42');
INSERT INTO `sys_oper_log` VALUES (28, '代码生成', 3, 'com.ruoyi.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', '', '/tool/gen/12', '127.0.0.1', '内网IP', '{tableIds=12}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-17 15:03:45');
INSERT INTO `sys_oper_log` VALUES (29, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', '', '/tool/gen/importTable', '127.0.0.1', '内网IP', 'ys_zhgl,ys_video', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-17 15:05:32');
INSERT INTO `sys_oper_log` VALUES (30, '代码生成', 3, 'com.ruoyi.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', '', '/tool/gen/15,16', '127.0.0.1', '内网IP', '{tableIds=15,16}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-17 15:05:40');
INSERT INTO `sys_oper_log` VALUES (31, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', '', '/tool/gen/importTable', '127.0.0.1', '内网IP', 'ys_video,ys_zhgl', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-17 15:07:29');
INSERT INTO `sys_oper_log` VALUES (32, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', '', '/tool/gen/importTable', '127.0.0.1', '内网IP', 'ys_image_category,ys_money_log,ys_nav,ys_order,ys_order_goods,ys_order_log,ys_play_award,ys_points_record,ys_rate,ys_reduction', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-17 15:07:50');
INSERT INTO `sys_oper_log` VALUES (33, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', '', '/tool/gen/importTable', '127.0.0.1', '内网IP', 'ys_user_coupon,ys_user_level', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-17 15:08:13');
INSERT INTO `sys_oper_log` VALUES (34, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', '', '/tool/gen/importTable', '127.0.0.1', '内网IP', 'ys_banner,ys_banner_item,ys_category,ys_coupon,ys_favorites,ys_fx_goods,ys_fx_record,ys_goods,ys_goods_sku,ys_group', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-17 15:08:29');
INSERT INTO `sys_oper_log` VALUES (35, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', '', '/tool/gen/importTable', '127.0.0.1', '内网IP', 'ys_group_rule,ys_image,ys_reduction_goods,ys_region,ys_search,ys_sys_backup,ys_sys_config,ys_tui,ys_user,ys_user_address', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-17 15:08:44');
INSERT INTO `sys_oper_log` VALUES (36, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', '', '/tool/gen/importTable', '127.0.0.1', '内网IP', 'ys_article', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-17 15:08:59');
INSERT INTO `sys_oper_log` VALUES (37, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', '', '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', 0, '', '2021-06-17 15:12:48');
INSERT INTO `sys_oper_log` VALUES (38, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', '', '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', 0, '', '2021-06-17 15:24:08');
INSERT INTO `sys_oper_log` VALUES (39, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', '', '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', 0, '', '2021-06-17 15:25:14');
INSERT INTO `sys_oper_log` VALUES (40, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', '', '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', 0, '', '2021-06-17 15:26:04');
INSERT INTO `sys_oper_log` VALUES (41, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"edit\",\"orderNum\":\"2\",\"menuName\":\"广告管理\",\"params\":{},\"parentId\":2012,\"isCache\":\"0\",\"path\":\"/banner\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1623922964146,\"isFrame\":\"1\",\"menuId\":2019,\"menuType\":\"C\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-17 17:42:45');
INSERT INTO `sys_oper_log` VALUES (42, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"edit\",\"orderNum\":\"2\",\"menuName\":\"广告管理\",\"remark\":\"\",\"updateTime\":1623923005294,\"params\":{},\"parentId\":2012,\"isCache\":\"0\",\"path\":\"/shop/banner\",\"component\":\"/shop/banner/index\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1623922964000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2019,\"menuType\":\"C\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-17 17:43:25');
INSERT INTO `sys_oper_log` VALUES (43, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"edit\",\"orderNum\":\"2\",\"menuName\":\"广告管理\",\"remark\":\"\",\"updateTime\":1623923005000,\"params\":{},\"parentId\":2012,\"isCache\":\"0\",\"path\":\"banner\",\"component\":\"/shop/banner/index\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1623922964000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2019,\"menuType\":\"C\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-17 17:43:58');
INSERT INTO `sys_oper_log` VALUES (44, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', '', '/system/role', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":1,\"admin\":true,\"remark\":\"超级管理员\",\"dataScope\":\"1\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"1\",\"deptCheckStrictly\":true,\"createBy\":\"admin\",\"createTime\":1622021229000,\"updateBy\":\"\",\"menuCheckStrictly\":true,\"roleKey\":\"admin\",\"roleName\":\"超级管理员\",\"menuIds\":[2012,2013,2014,2015,2016,2017,2018,2019],\"status\":\"0\"}', 'null', 1, '不允许操作超级管理员角色', '2021-06-17 17:44:21');
INSERT INTO `sys_oper_log` VALUES (45, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"edit\",\"orderNum\":\"2\",\"menuName\":\"广告管理\",\"remark\":\"\",\"updateTime\":1623923005000,\"params\":{},\"parentId\":2012,\"isCache\":\"0\",\"path\":\"banner\",\"component\":\"shop/banner/index\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1623922964000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2019,\"menuType\":\"C\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-17 17:47:14');
INSERT INTO `sys_oper_log` VALUES (46, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', '', '/tool/gen/importTable', '127.0.0.1', '内网IP', '', 'null', 1, '\r\n### Error querying database.  Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 3\r\n### The error may exist in file [D:\\YSweet\\ysweet-server\\ruoyi-generator\\target\\classes\\mapper\\generator\\GenTableMapper.xml]\r\n### The error may involve defaultParameterMap\r\n### The error occurred while setting parameters\r\n### SQL: select table_name, table_comment, create_time, update_time from information_schema.tables   where table_name NOT LIKE \'qrtz_%\' and table_name NOT LIKE \'gen_%\' and table_schema = (select database())   and table_name in\r\n### Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 3\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 3', '2021-06-18 11:21:05');
INSERT INTO `sys_oper_log` VALUES (47, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', '', '/tool/gen/importTable', '127.0.0.1', '内网IP', 'ys_material_group', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-18 11:21:09');
INSERT INTO `sys_oper_log` VALUES (48, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', '', '/tool/gen/importTable', '127.0.0.1', '内网IP', 'ys_material', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-18 11:21:17');
INSERT INTO `sys_oper_log` VALUES (49, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', '', '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', 0, '', '2021-06-18 11:21:36');
INSERT INTO `sys_oper_log` VALUES (50, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', '', '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":1623998784286,\"dictName\":\"123123\",\"dictId\":100,\"params\":{},\"dictType\":\"1312312\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-18 14:46:25');
INSERT INTO `sys_oper_log` VALUES (51, '素材分组', 2, 'com.ruoyi.web.controller.shop.YsMaterialGroupController.edit()', 'PUT', 1, 'admin', '', '/shop/group', '127.0.0.1', '内网IP', '{\"name\":\"新闻1\",\"updateTime\":1624004371580,\"id\":\"2b854bcc104371c4a90f13283765e8c5\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-18 16:19:32');
INSERT INTO `sys_oper_log` VALUES (52, '素材分组', 2, 'com.ruoyi.web.controller.shop.YsMaterialGroupController.edit()', 'PUT', 1, 'admin', '', '/shop/group', '127.0.0.1', '内网IP', '{\"name\":\"新闻\",\"updateTime\":1624004388787,\"id\":\"2b854bcc104371c4a90f13283765e8c5\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-18 16:19:49');
INSERT INTO `sys_oper_log` VALUES (53, '素材分组', 1, 'com.ruoyi.web.controller.shop.YsMaterialGroupController.add()', 'POST', 1, 'admin', '', '/shop/group', '127.0.0.1', '内网IP', '', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\r\n### The error may exist in com/ruoyi/system/mapper/YsMaterialGroupMapper.java (best guess)\r\n### The error may involve com.ruoyi.system.mapper.YsMaterialGroupMapper.insert-Inline\r\n### The error occurred while setting parameters\r\n### SQL: INSERT INTO ys_material_group  ( create_time, create_id,  update_time )  VALUES  ( ?, ?,  ? )\r\n### Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n; Field \'id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'id\' doesn\'t have a default value', '2021-06-18 16:37:02');
INSERT INTO `sys_oper_log` VALUES (54, '素材分组', 1, 'com.ruoyi.web.controller.shop.YsMaterialGroupController.add()', 'POST', 1, 'admin', '', '/shop/group', '127.0.0.1', '内网IP', '', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\r\n### The error may exist in com/ruoyi/system/mapper/YsMaterialGroupMapper.java (best guess)\r\n### The error may involve com.ruoyi.system.mapper.YsMaterialGroupMapper.insert-Inline\r\n### The error occurred while setting parameters\r\n### SQL: INSERT INTO ys_material_group  ( create_time, create_id,  update_time )  VALUES  ( ?, ?,  ? )\r\n### Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n; Field \'id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'id\' doesn\'t have a default value', '2021-06-18 16:37:57');
INSERT INTO `sys_oper_log` VALUES (55, '素材分组', 1, 'com.ruoyi.web.controller.shop.YsMaterialGroupController.add()', 'POST', 1, 'admin', '', '/shop/group', '127.0.0.1', '内网IP', '', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\r\n### The error may exist in com/ruoyi/system/mapper/YsMaterialGroupMapper.java (best guess)\r\n### The error may involve com.ruoyi.system.mapper.YsMaterialGroupMapper.insert-Inline\r\n### The error occurred while setting parameters\r\n### SQL: INSERT INTO ys_material_group  ( create_time, create_id,  update_time )  VALUES  ( ?, ?,  ? )\r\n### Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n; Field \'id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'id\' doesn\'t have a default value', '2021-06-18 16:38:53');
INSERT INTO `sys_oper_log` VALUES (56, '素材分组', 1, 'com.ruoyi.web.controller.shop.YsMaterialGroupController.add()', 'POST', 1, 'admin', '', '/shop/group', '127.0.0.1', '内网IP', '', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\r\n### The error may exist in com/ruoyi/system/mapper/YsMaterialGroupMapper.java (best guess)\r\n### The error may involve com.ruoyi.system.mapper.YsMaterialGroupMapper.insert-Inline\r\n### The error occurred while setting parameters\r\n### SQL: INSERT INTO ys_material_group  ( create_time, create_id,  update_time )  VALUES  ( ?, ?,  ? )\r\n### Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n; Field \'id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'id\' doesn\'t have a default value', '2021-06-18 16:39:30');
INSERT INTO `sys_oper_log` VALUES (57, '素材分组', 1, 'com.ruoyi.web.controller.shop.YsMaterialGroupController.add()', 'POST', 1, 'admin', '', '/shop/group', '127.0.0.1', '内网IP', '', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\r\n### The error may exist in com/ruoyi/system/mapper/YsMaterialGroupMapper.java (best guess)\r\n### The error may involve com.ruoyi.system.mapper.YsMaterialGroupMapper.insert-Inline\r\n### The error occurred while setting parameters\r\n### SQL: INSERT INTO ys_material_group  ( create_time, create_id,  update_time )  VALUES  ( ?, ?,  ? )\r\n### Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n; Field \'id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'id\' doesn\'t have a default value', '2021-06-18 16:43:04');
INSERT INTO `sys_oper_log` VALUES (58, '素材分组', 1, 'com.ruoyi.web.controller.shop.YsMaterialGroupController.add()', 'POST', 1, 'admin', '', '/shop/group', '127.0.0.1', '内网IP', '{\"groupName\":\"12321321\"}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\r\n### The error may exist in com/ruoyi/system/mapper/YsMaterialGroupMapper.java (best guess)\r\n### The error may involve com.ruoyi.system.mapper.YsMaterialGroupMapper.insert-Inline\r\n### The error occurred while setting parameters\r\n### SQL: INSERT INTO ys_material_group  ( create_time, create_id, name, update_time )  VALUES  ( ?, ?, ?, ? )\r\n### Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n; Field \'id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'id\' doesn\'t have a default value', '2021-06-18 16:45:56');
INSERT INTO `sys_oper_log` VALUES (59, '素材分组', 1, 'com.ruoyi.web.controller.shop.YsMaterialGroupController.add()', 'POST', 1, 'admin', '', '/shop/group', '127.0.0.1', '内网IP', '{\"groupName\":\"3213123\"}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\r\n### The error may exist in com/ruoyi/system/mapper/YsMaterialGroupMapper.java (best guess)\r\n### The error may involve com.ruoyi.system.mapper.YsMaterialGroupMapper.insert-Inline\r\n### The error occurred while setting parameters\r\n### SQL: INSERT INTO ys_material_group  ( create_time, create_id, name, update_time )  VALUES  ( ?, ?, ?, ? )\r\n### Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n; Field \'id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'id\' doesn\'t have a default value', '2021-06-18 16:47:03');
INSERT INTO `sys_oper_log` VALUES (60, '素材分组', 1, 'com.ruoyi.web.controller.shop.YsMaterialGroupController.add()', 'POST', 1, 'admin', '', '/shop/group', '127.0.0.1', '内网IP', '{\"groupName\":\"1232132\"}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\r\n### The error may exist in com/ruoyi/system/mapper/YsMaterialGroupMapper.java (best guess)\r\n### The error may involve com.ruoyi.system.mapper.YsMaterialGroupMapper.insert-Inline\r\n### The error occurred while setting parameters\r\n### SQL: INSERT INTO ys_material_group  ( create_time, create_id, name, update_time )  VALUES  ( ?, ?, ?, ? )\r\n### Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n; Field \'id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'id\' doesn\'t have a default value', '2021-06-18 16:50:37');
INSERT INTO `sys_oper_log` VALUES (61, '素材分组', 1, 'com.ruoyi.web.controller.shop.YsMaterialGroupController.add()', 'POST', 1, 'admin', '', '/shop/group', '127.0.0.1', '内网IP', '', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\r\n### The error may exist in com/ruoyi/system/mapper/YsMaterialGroupMapper.java (best guess)\r\n### The error may involve com.ruoyi.system.mapper.YsMaterialGroupMapper.insert-Inline\r\n### The error occurred while setting parameters\r\n### SQL: INSERT INTO ys_material_group  ( create_time, create_id,  update_time )  VALUES  ( ?, ?,  ? )\r\n### Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n; Field \'id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'id\' doesn\'t have a default value', '2021-06-18 16:53:32');
INSERT INTO `sys_oper_log` VALUES (62, '素材分组', 1, 'com.ruoyi.web.controller.shop.YsMaterialGroupController.add()', 'POST', 1, 'admin', '', '/shop/group', '127.0.0.1', '内网IP', '{\"name\":\"12321312\"}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\r\n### The error may exist in com/ruoyi/system/mapper/YsMaterialGroupMapper.java (best guess)\r\n### The error may involve com.ruoyi.system.mapper.YsMaterialGroupMapper.insert-Inline\r\n### The error occurred while setting parameters\r\n### SQL: INSERT INTO ys_material_group  ( create_time, create_id, name, update_time )  VALUES  ( ?, ?, ?, ? )\r\n### Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n; Field \'id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'id\' doesn\'t have a default value', '2021-06-18 17:08:01');
INSERT INTO `sys_oper_log` VALUES (63, '素材分组', 1, 'com.ruoyi.web.controller.shop.YsMaterialGroupController.add()', 'POST', 1, 'admin', '', '/shop/group', '127.0.0.1', '内网IP', '{\"name\":\"1321312\"}', '{\"code\":200,\"data\":true,\"msg\":\"操作成功\"}', 0, '', '2021-06-18 17:10:27');
INSERT INTO `sys_oper_log` VALUES (64, '素材分组', 1, 'com.ruoyi.web.controller.shop.YsMaterialGroupController.add()', 'POST', 1, 'admin', '', '/shop/group', '127.0.0.1', '内网IP', '{\"name\":\"321312\"}', '{\"code\":200,\"data\":true,\"msg\":\"操作成功\"}', 0, '', '2021-06-18 17:11:49');
INSERT INTO `sys_oper_log` VALUES (65, '素材分组', 3, 'com.ruoyi.web.controller.shop.YsMaterialGroupController.remove()', 'DELETE', 1, 'admin', '', '/shop/group/55ccf8f7e639c15779fc7733743c75ef', '127.0.0.1', '内网IP', '{ids=55ccf8f7e639c15779fc7733743c75ef}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-18 17:21:46');
INSERT INTO `sys_oper_log` VALUES (66, '素材分组', 3, 'com.ruoyi.web.controller.shop.YsMaterialGroupController.remove()', 'DELETE', 1, 'admin', '', '/shop/group/8595d6443eeec147699633649de37c6a', '127.0.0.1', '内网IP', '{ids=8595d6443eeec147699633649de37c6a}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-18 17:22:17');
INSERT INTO `sys_oper_log` VALUES (67, '素材分组', 1, 'com.ruoyi.web.controller.shop.YsMaterialGroupController.add()', 'POST', 1, 'admin', '', '/shop/group', '127.0.0.1', '内网IP', '{\"name\":\"1231231\"}', '{\"code\":200,\"data\":true,\"msg\":\"操作成功\"}', 0, '', '2021-06-18 17:23:58');
INSERT INTO `sys_oper_log` VALUES (68, '素材分组', 3, 'com.ruoyi.web.controller.shop.YsMaterialGroupController.remove()', 'DELETE', 1, 'admin', '', '/shop/group/8d4646eb2d7067126eb08adb0672f7bb', '127.0.0.1', '内网IP', '{ids=8d4646eb2d7067126eb08adb0672f7bb}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-18 17:24:04');
INSERT INTO `sys_oper_log` VALUES (69, '素材分组', 1, 'com.ruoyi.web.controller.shop.YsMaterialGroupController.add()', 'POST', 1, 'admin', '', '/shop/group', '127.0.0.1', '内网IP', '{\"name\":\"123123\"}', '{\"code\":200,\"data\":true,\"msg\":\"操作成功\"}', 0, '', '2021-06-18 17:24:53');
INSERT INTO `sys_oper_log` VALUES (70, '素材分组', 3, 'com.ruoyi.web.controller.shop.YsMaterialGroupController.remove()', 'DELETE', 1, 'admin', '', '/shop/group/4297f44b13955235245b2497399d7a93', '127.0.0.1', '内网IP', '{ids=4297f44b13955235245b2497399d7a93}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-18 17:24:57');
INSERT INTO `sys_oper_log` VALUES (71, '素材分组', 1, 'com.ruoyi.web.controller.shop.YsMaterialGroupController.add()', 'POST', 1, 'admin', '', '/shop/group', '127.0.0.1', '内网IP', '{\"name\":\"新闻\"}', '{\"code\":200,\"data\":true,\"msg\":\"操作成功\"}', 0, '', '2021-06-21 10:14:52');
INSERT INTO `sys_oper_log` VALUES (72, '素材分组', 3, 'com.ruoyi.web.controller.shop.YsMaterialGroupController.remove()', 'DELETE', 1, 'admin', '', '/shop/group/1d9c15c5d2162ce48e816b504182e5be', '127.0.0.1', '内网IP', '{ids=1d9c15c5d2162ce48e816b504182e5be}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-21 10:15:00');
INSERT INTO `sys_oper_log` VALUES (73, '代码生成', 3, 'com.ruoyi.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', '', '/tool/gen/17,18,19,20,21,22,23,24,25,26', '127.0.0.1', '内网IP', '{tableIds=17,18,19,20,21,22,23,24,25,26}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-21 11:04:16');
INSERT INTO `sys_oper_log` VALUES (74, '代码生成', 3, 'com.ruoyi.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', '', '/tool/gen/27,28,29,30,31,32,33,34,35,36', '127.0.0.1', '内网IP', '{tableIds=27,28,29,30,31,32,33,34,35,36}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-21 11:04:19');
INSERT INTO `sys_oper_log` VALUES (75, '代码生成', 3, 'com.ruoyi.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', '', '/tool/gen/37,38,39,40,41,42,43,44,45,46', '127.0.0.1', '内网IP', '{tableIds=37,38,39,40,41,42,43,44,45,46}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-21 11:04:27');
INSERT INTO `sys_oper_log` VALUES (76, '代码生成', 3, 'com.ruoyi.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', '', '/tool/gen/47,48,49,50,51,52,53', '127.0.0.1', '内网IP', '{tableIds=47,48,49,50,51,52,53}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-21 11:04:31');
INSERT INTO `sys_oper_log` VALUES (77, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', '', '/tool/gen/importTable', '127.0.0.1', '内网IP', 'ys_banner', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-21 11:04:46');
INSERT INTO `sys_oper_log` VALUES (78, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', '', '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":574,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"admin\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"updateTime\":1624244764445,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1624244685000,\"tableId\":54,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Name\",\"usableColumn\":false,\"columnId\":575,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"admin\",\"isInsert\":\"1\",\"javaField\":\"name\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"Banner名称，通常作为标识\",\"isQuery\":\"1\",\"updateTime\":1624244764529,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1624244685000,\"isEdit\":\"1\",\"tableId\":54,\"pk\":false,\"columnName\":\"name\"},{\"capJavaField\":\"Description\",\"usableColumn\":false,\"columnId\":576,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"admin\",\"isInsert\":\"1\",\"javaField\":\"description\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"Banner描述\",\"isQuery\":\"1\",\"updateTime\":1624244764615,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1624244685000,\"isEdit\":\"1\",\"tableId\":54,\"pk\":false,\"columnName\":\"description\"},{\"capJavaField\":\"ImageUrl\",\"usableColumn\":false,\"columnId\":577,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"admin\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"imageUrl\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"广告的图片,不能为空\",\"isQuery\":\"1\",\"updateTime\":16', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-21 11:06:06');
INSERT INTO `sys_oper_log` VALUES (79, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', '', '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":1624267658766,\"dictName\":\"广告类型\",\"dictId\":101,\"params\":{},\"dictType\":\"banner_type\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-21 17:27:39');
INSERT INTO `sys_oper_log` VALUES (80, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', '', '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"1\",\"dictSort\":1,\"params\":{},\"dictType\":\"banner_type\",\"dictLabel\":\"首页广告\",\"createBy\":\"admin\",\"default\":false,\"createTime\":1624267683981,\"dictCode\":100,\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-21 17:28:04');
INSERT INTO `sys_oper_log` VALUES (81, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', '', '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"2\",\"dictSort\":2,\"params\":{},\"dictType\":\"banner_type\",\"dictLabel\":\"点餐页广告\",\"createBy\":\"admin\",\"default\":false,\"createTime\":1624267714794,\"dictCode\":101,\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-21 17:28:35');
INSERT INTO `sys_oper_log` VALUES (82, '字典类型', 3, 'com.ruoyi.web.controller.system.SysDictTypeController.remove()', 'DELETE', 1, 'admin', '', '/system/dict/type/100', '127.0.0.1', '内网IP', '{dictIds=100}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-21 17:28:51');
INSERT INTO `sys_oper_log` VALUES (83, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', '', '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"0\",\"dictSort\":0,\"params\":{},\"dictType\":\"banner_type\",\"dictLabel\":\"全部\",\"createBy\":\"admin\",\"default\":false,\"createTime\":1624268013446,\"dictCode\":102,\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-21 17:33:34');
INSERT INTO `sys_oper_log` VALUES (84, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', '', '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":1624268883452,\"dictName\":\"跳转类型\",\"dictId\":102,\"params\":{},\"dictType\":\"redirect_type\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-21 17:48:04');
INSERT INTO `sys_oper_log` VALUES (85, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', '', '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"1\",\"dictSort\":0,\"params\":{},\"dictType\":\"redirect_type\",\"dictLabel\":\"商品\",\"createBy\":\"admin\",\"default\":false,\"createTime\":1624268902793,\"dictCode\":103,\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-21 17:48:23');
INSERT INTO `sys_oper_log` VALUES (86, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', '', '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"2\",\"dictSort\":2,\"params\":{},\"dictType\":\"redirect_type\",\"dictLabel\":\"文章\",\"createBy\":\"admin\",\"default\":false,\"createTime\":1624268912701,\"dictCode\":104,\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-21 17:48:33');
INSERT INTO `sys_oper_log` VALUES (87, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', '', '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"0\",\"dictSort\":0,\"updateTime\":1624268924149,\"params\":{},\"dictType\":\"redirect_type\",\"dictLabel\":\"商品\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1624268903000,\"dictCode\":103,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-21 17:48:44');
INSERT INTO `sys_oper_log` VALUES (88, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', '', '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"1\",\"dictSort\":1,\"updateTime\":1624268924000,\"params\":{},\"dictType\":\"redirect_type\",\"dictLabel\":\"商品\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1624268903000,\"dictCode\":103,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-21 17:48:52');
INSERT INTO `sys_oper_log` VALUES (89, '字典类型', 3, 'com.ruoyi.web.controller.system.SysDictDataController.remove()', 'DELETE', 1, 'admin', '', '/system/dict/data/102', '127.0.0.1', '内网IP', '{dictCodes=102}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-22 10:41:17');
INSERT INTO `sys_oper_log` VALUES (90, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', '', '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"0\",\"dictSort\":0,\"params\":{},\"dictType\":\"redirect_type\",\"dictLabel\":\"不跳转\",\"createBy\":\"admin\",\"default\":false,\"createTime\":1624330862099,\"dictCode\":105,\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-22 11:01:03');
INSERT INTO `sys_oper_log` VALUES (91, '菜单管理', 3, 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', '', '/system/menu/2013', '127.0.0.1', '内网IP', '{menuId=2013}', '{\"code\":500,\"msg\":\"存在子菜单,不允许删除\"}', 0, '', '2021-06-22 16:31:46');
INSERT INTO `sys_oper_log` VALUES (92, '菜单管理', 3, 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', '', '/system/menu/2014', '127.0.0.1', '内网IP', '{menuId=2014}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-22 16:31:51');
INSERT INTO `sys_oper_log` VALUES (93, '菜单管理', 3, 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', '', '/system/menu/2015', '127.0.0.1', '内网IP', '{menuId=2015}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-22 16:31:54');
INSERT INTO `sys_oper_log` VALUES (94, '菜单管理', 3, 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', '', '/system/menu/2017', '127.0.0.1', '内网IP', '{menuId=2017}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-22 16:31:58');
INSERT INTO `sys_oper_log` VALUES (95, '菜单管理', 3, 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', '', '/system/menu/2018', '127.0.0.1', '内网IP', '{menuId=2018}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-22 16:32:01');
INSERT INTO `sys_oper_log` VALUES (96, '菜单管理', 3, 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', '', '/system/menu/2016', '127.0.0.1', '内网IP', '{menuId=2016}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-22 16:32:04');
INSERT INTO `sys_oper_log` VALUES (97, '菜单管理', 3, 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', '', '/system/menu/2013', '127.0.0.1', '内网IP', '{menuId=2013}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-22 16:32:09');
INSERT INTO `sys_oper_log` VALUES (98, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', '', '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":1624436084733,\"dictName\":\"12312\",\"remark\":\"321312\",\"dictId\":103,\"params\":{},\"dictType\":\"12312\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-23 16:14:46');
INSERT INTO `sys_oper_log` VALUES (99, '字典类型', 3, 'com.ruoyi.web.controller.system.SysDictTypeController.remove()', 'DELETE', 1, 'admin', '', '/system/dict/type/103', '127.0.0.1', '内网IP', '{dictIds=103}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-23 16:16:02');
INSERT INTO `sys_oper_log` VALUES (100, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', '', '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":1624436830492,\"dictName\":\"打打\",\"remark\":\"打仨\",\"dictId\":104,\"params\":{},\"dictType\":\"仨大\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-23 16:27:18');
INSERT INTO `sys_oper_log` VALUES (101, '字典类型', 2, 'com.ruoyi.web.controller.system.SysDictTypeController.edit()', 'PUT', 1, 'admin', '', '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":1624436830000,\"updateBy\":\"admin\",\"dictName\":\"打打\",\"remark\":\"打仨\",\"updateTime\":1624436990655,\"dictId\":104,\"params\":{},\"dictType\":\"bbbbb\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-23 16:29:51');
INSERT INTO `sys_oper_log` VALUES (102, '字典类型', 3, 'com.ruoyi.web.controller.system.SysDictTypeController.remove()', 'DELETE', 1, 'admin', '', '/system/dict/type/104', '127.0.0.1', '内网IP', '{dictIds=104}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-23 16:30:09');
INSERT INTO `sys_oper_log` VALUES (103, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', '', '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"5\",\"dictSort\":0,\"params\":{},\"dictType\":\"redirect_type\",\"dictLabel\":\"跳挑剔\",\"createBy\":\"admin\",\"default\":false,\"createTime\":1624437053812,\"dictCode\":106,\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-23 16:30:54');
INSERT INTO `sys_oper_log` VALUES (104, '字典类型', 3, 'com.ruoyi.web.controller.system.SysDictDataController.remove()', 'DELETE', 1, 'admin', '', '/system/dict/data/106', '127.0.0.1', '内网IP', '{dictCodes=106}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-23 16:31:53');
INSERT INTO `sys_oper_log` VALUES (105, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', '', '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"212\",\"dictSort\":0,\"params\":{},\"dictType\":\"redirect_type\",\"dictLabel\":\"发的\",\"createBy\":\"admin\",\"default\":false,\"createTime\":1624437779744,\"dictCode\":107,\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-23 16:43:00');
INSERT INTO `sys_oper_log` VALUES (106, '字典类型', 3, 'com.ruoyi.web.controller.system.SysDictDataController.remove()', 'DELETE', 1, 'admin', '', '/system/dict/data/107', '127.0.0.1', '内网IP', '{dictCodes=107}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-23 16:43:06');
INSERT INTO `sys_oper_log` VALUES (107, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"swagger\",\"orderNum\":\"2\",\"menuName\":\"店铺设置\",\"params\":{},\"parentId\":2012,\"isCache\":\"0\",\"path\":\"/shop/config\",\"component\":\"shop/config\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1624438912090,\"isFrame\":\"1\",\"menuId\":2020,\"menuType\":\"C\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-23 17:01:52');
INSERT INTO `sys_oper_log` VALUES (108, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"swagger\",\"orderNum\":\"2\",\"menuName\":\"店铺设置\",\"remark\":\"\",\"updateTime\":1624438923940,\"params\":{},\"parentId\":2012,\"isCache\":\"0\",\"path\":\"/shop/config\",\"component\":\"shop/config/index\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1624438912000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2020,\"menuType\":\"C\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-23 17:02:04');
INSERT INTO `sys_oper_log` VALUES (109, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"dict\",\"orderNum\":\"3\",\"menuName\":\"优惠券\",\"params\":{},\"parentId\":2012,\"isCache\":\"0\",\"path\":\"/shop/coupon\",\"component\":\"/shop/coupon\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1624677413314,\"isFrame\":\"1\",\"menuId\":2021,\"menuType\":\"C\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-26 11:16:54');
INSERT INTO `sys_oper_log` VALUES (110, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"dict\",\"orderNum\":\"3\",\"menuName\":\"优惠券\",\"remark\":\"\",\"updateTime\":1624677426632,\"params\":{},\"parentId\":2012,\"isCache\":\"0\",\"path\":\"/shop/coupon\",\"component\":\"shop/coupon/index\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1624677413000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2021,\"menuType\":\"C\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-06-26 11:17:07');
INSERT INTO `sys_oper_log` VALUES (111, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', '', '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":1625465981615,\"dictName\":\"优惠券类型\",\"dictId\":105,\"params\":{},\"dictType\":\"coupon_type\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-07-05 14:19:42');
INSERT INTO `sys_oper_log` VALUES (112, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', '', '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"1\",\"dictSort\":1,\"remark\":\"店铺优惠券\",\"params\":{},\"dictType\":\"coupon_type\",\"dictLabel\":\"店铺优惠券\",\"createBy\":\"admin\",\"default\":false,\"createTime\":1625466012115,\"dictCode\":108,\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-07-05 14:20:12');
INSERT INTO `sys_oper_log` VALUES (113, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', '', '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"2\",\"dictSort\":2,\"remark\":\"商品优惠券\",\"params\":{},\"dictType\":\"coupon_type\",\"dictLabel\":\"商品优惠券\",\"createBy\":\"admin\",\"default\":false,\"createTime\":1625466026719,\"dictCode\":109,\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-07-05 14:20:27');
INSERT INTO `sys_oper_log` VALUES (114, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"dict\",\"orderNum\":\"4\",\"menuName\":\"商品分类管理\",\"params\":{},\"parentId\":2012,\"isCache\":\"0\",\"path\":\"/page/category\",\"component\":\"/page/category/index\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1625563810412,\"isFrame\":\"1\",\"menuId\":2022,\"menuType\":\"C\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-07-06 17:30:11');
INSERT INTO `sys_oper_log` VALUES (115, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"dict\",\"orderNum\":\"4\",\"menuName\":\"商品分类管理\",\"remark\":\"\",\"updateTime\":1625563831169,\"params\":{},\"parentId\":2012,\"isCache\":\"0\",\"path\":\"shop/category/index\",\"component\":\"/page/category/index\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1625563810000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2022,\"menuType\":\"C\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-07-06 17:30:31');
INSERT INTO `sys_oper_log` VALUES (116, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"dict\",\"orderNum\":\"4\",\"menuName\":\"商品分类管理\",\"remark\":\"\",\"updateTime\":1625563831000,\"params\":{},\"parentId\":2012,\"isCache\":\"0\",\"path\":\"shop/category\",\"component\":\"/shop/category/index\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1625563810000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2022,\"menuType\":\"C\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-07-06 17:31:17');
INSERT INTO `sys_oper_log` VALUES (117, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"dict\",\"orderNum\":\"4\",\"menuName\":\"商品分类管理\",\"remark\":\"\",\"updateTime\":1625563831000,\"params\":{},\"parentId\":2012,\"isCache\":\"0\",\"path\":\"shop/category\",\"component\":\"shop/category/index\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1625563810000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2022,\"menuType\":\"C\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-07-06 17:31:25');
INSERT INTO `sys_oper_log` VALUES (118, '素材分组', 1, 'com.ruoyi.web.controller.shop.YsMaterialGroupController.add()', 'POST', 1, 'admin', '', '/shop/group', '127.0.0.1', '内网IP', '{\"name\":\"666\"}', '{\"code\":200,\"data\":true,\"msg\":\"操作成功\"}', 0, '', '2021-07-07 10:05:06');
INSERT INTO `sys_oper_log` VALUES (119, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, 'admin', '', '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"code\":200,\"data\":{\"imgUrl\":\"/profile/avatar/2021/07/07/be3c1c06-0981-47a4-b225-9fddb8899596.jpeg\"},\"msg\":\"操作成功\"}', 0, '', '2021-07-07 11:48:30');
INSERT INTO `sys_oper_log` VALUES (120, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, 'admin', '', '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"code\":200,\"data\":{\"imgUrl\":\"/profile/avatar/2021/07/07/a223a686-0dca-4e0c-aac6-67d630b806ea.jpeg\"},\"msg\":\"操作成功\"}', 0, '', '2021-07-07 12:30:47');
INSERT INTO `sys_oper_log` VALUES (121, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, 'admin', '', '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"code\":200,\"data\":{\"imgUrl\":\"https://xjm-picture.oss-cn-beijing.aliyuncs.com/shop/avatar/20210707123219151491.blob\"},\"msg\":\"操作成功\"}', 0, '', '2021-07-07 12:32:20');
INSERT INTO `sys_oper_log` VALUES (122, '用户头像', 2, 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', 1, 'admin', '', '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"code\":200,\"data\":{\"imgUrl\":\"https://xjm-picture.oss-cn-beijing.aliyuncs.com/shop/avatar/20210707152800278954.blob\"},\"msg\":\"操作成功\"}', 0, '', '2021-07-07 15:28:01');
INSERT INTO `sys_oper_log` VALUES (123, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"dict\",\"orderNum\":\"4\",\"menuName\":\"商品分类\",\"remark\":\"\",\"updateTime\":1625649139897,\"params\":{},\"parentId\":2012,\"isCache\":\"0\",\"path\":\"shop/category\",\"component\":\"shop/category/index\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1625563810000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2022,\"menuType\":\"C\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-07-07 17:12:20');
INSERT INTO `sys_oper_log` VALUES (124, '素材分组', 3, 'com.ruoyi.web.controller.shop.YsMaterialGroupController.remove()', 'DELETE', 1, 'admin', '', '/shop/group/2b854bcc104371c4a90f13283765e8c5', '127.0.0.1', '内网IP', '{ids=2b854bcc104371c4a90f13283765e8c5}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-07-07 17:20:00');
INSERT INTO `sys_oper_log` VALUES (125, '素材分组', 3, 'com.ruoyi.web.controller.shop.YsMaterialGroupController.remove()', 'DELETE', 1, 'admin', '', '/shop/group/5b61232049e724f587989d376b260252', '127.0.0.1', '内网IP', '{ids=5b61232049e724f587989d376b260252}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-07-07 17:20:03');
INSERT INTO `sys_oper_log` VALUES (126, '素材分组', 3, 'com.ruoyi.web.controller.shop.YsMaterialGroupController.remove()', 'DELETE', 1, 'admin', '', '/shop/group/98a76c9e94d8691deab95bd2908cbab2', '127.0.0.1', '内网IP', '{ids=98a76c9e94d8691deab95bd2908cbab2}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-07-07 17:20:09');
INSERT INTO `sys_oper_log` VALUES (127, '素材分组', 3, 'com.ruoyi.web.controller.shop.YsMaterialGroupController.remove()', 'DELETE', 1, 'admin', '', '/shop/group/d9b1b9d69f5b72cda63e26dc9f087623', '127.0.0.1', '内网IP', '{ids=d9b1b9d69f5b72cda63e26dc9f087623}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-07-07 17:20:12');
INSERT INTO `sys_oper_log` VALUES (128, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"shopping\",\"orderNum\":\"5\",\"menuName\":\"商品管理\",\"updateTime\":1625713514358,\"params\":{},\"parentId\":2012,\"isCache\":\"0\",\"path\":\"good\",\"component\":\"shop/good/index\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1625713514358,\"isFrame\":\"1\",\"menuId\":2023,\"menuType\":\"C\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-07-08 11:05:15');
INSERT INTO `sys_oper_log` VALUES (129, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', '', '/tool/gen/importTable', '127.0.0.1', '内网IP', 'ys_goods', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-07-08 14:07:47');
INSERT INTO `sys_oper_log` VALUES (130, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"swagger\",\"orderNum\":\"2\",\"menuName\":\"店铺设置\",\"remark\":\"\",\"updateTime\":1625792903943,\"params\":{},\"parentId\":2012,\"isCache\":\"0\",\"path\":\"config\",\"component\":\"shop/config/index\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1624438912000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2020,\"menuType\":\"C\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-07-09 09:08:24');
INSERT INTO `sys_oper_log` VALUES (131, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"dict\",\"orderNum\":\"3\",\"menuName\":\"优惠券\",\"remark\":\"\",\"updateTime\":1625792913266,\"params\":{},\"parentId\":2012,\"isCache\":\"0\",\"path\":\"coupon\",\"component\":\"shop/coupon/index\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1624677413000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2021,\"menuType\":\"C\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-07-09 09:08:33');
INSERT INTO `sys_oper_log` VALUES (132, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"dict\",\"orderNum\":\"4\",\"menuName\":\"商品分类\",\"remark\":\"\",\"updateTime\":1625792920559,\"params\":{},\"parentId\":2012,\"isCache\":\"0\",\"path\":\"category\",\"component\":\"shop/category/index\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1625563810000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2022,\"menuType\":\"C\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-07-09 09:08:41');
INSERT INTO `sys_oper_log` VALUES (133, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"shopping\",\"orderNum\":\"5\",\"menuName\":\"商品管理\",\"remark\":\"\",\"updateTime\":1625792929931,\"params\":{},\"parentId\":2012,\"isCache\":\"0\",\"path\":\"goods\",\"component\":\"shop/good/index\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1625713514000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2023,\"menuType\":\"C\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-07-09 09:08:50');
INSERT INTO `sys_oper_log` VALUES (134, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"1\",\"icon\":\"build\",\"orderNum\":\"6\",\"menuName\":\"商品添加\",\"updateTime\":1625793040824,\"params\":{},\"parentId\":2012,\"isCache\":\"0\",\"path\":\"goodsAdd\",\"component\":\"shop/goods/goodsAdd\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1625793040824,\"isFrame\":\"1\",\"menuId\":2024,\"menuType\":\"C\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-07-09 09:10:41');
INSERT INTO `sys_oper_log` VALUES (135, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"shopping\",\"orderNum\":\"5\",\"menuName\":\"商品管理\",\"remark\":\"\",\"updateTime\":1625793322774,\"params\":{},\"parentId\":2012,\"isCache\":\"0\",\"path\":\"goods\",\"component\":\"shop/goods/index\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1625713514000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2023,\"menuType\":\"C\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-07-09 09:15:23');
INSERT INTO `sys_oper_log` VALUES (136, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"1\",\"icon\":\"build\",\"orderNum\":\"6\",\"menuName\":\"商品添加\",\"remark\":\"\",\"updateTime\":1625793526971,\"params\":{},\"parentId\":2012,\"isCache\":\"1\",\"path\":\"goodsAdd\",\"component\":\"shop/goods/goodsAdd\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1625793041000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2024,\"menuType\":\"C\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-07-09 09:18:47');
INSERT INTO `sys_oper_log` VALUES (137, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"1\",\"icon\":\"build\",\"orderNum\":\"6\",\"menuName\":\"商品添加\",\"remark\":\"\",\"updateTime\":1626083487500,\"params\":{},\"parentId\":2012,\"isCache\":\"1\",\"path\":\"goodsAdd\",\"component\":\"shop/goods/GoodsAdd\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1625793041000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2024,\"menuType\":\"C\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-07-12 17:51:28');
INSERT INTO `sys_oper_log` VALUES (138, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"1\",\"icon\":\"checkbox\",\"orderNum\":\"3\",\"menuName\":\"商品修改\",\"updateTime\":1628844078958,\"params\":{},\"parentId\":2012,\"isCache\":\"1\",\"path\":\"123\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1628844078958,\"isFrame\":\"1\",\"menuId\":2025,\"menuType\":\"C\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-08-13 16:41:19');
INSERT INTO `sys_oper_log` VALUES (139, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"1\",\"icon\":\"checkbox\",\"orderNum\":\"7\",\"menuName\":\"商品修改\",\"remark\":\"\",\"updateTime\":1628844090156,\"params\":{},\"parentId\":2012,\"isCache\":\"1\",\"path\":\"123\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1628844079000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2025,\"menuType\":\"C\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-08-13 16:41:30');
INSERT INTO `sys_oper_log` VALUES (140, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"1\",\"icon\":\"checkbox\",\"orderNum\":\"7\",\"menuName\":\"商品修改\",\"remark\":\"\",\"updateTime\":1628844102559,\"params\":{},\"parentId\":2012,\"isCache\":\"1\",\"path\":\"goodsAdd\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1628844079000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2025,\"menuType\":\"C\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-08-13 16:41:43');
INSERT INTO `sys_oper_log` VALUES (141, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"1\",\"icon\":\"checkbox\",\"orderNum\":\"7\",\"menuName\":\"商品修改\",\"remark\":\"\",\"updateTime\":1628844129357,\"params\":{},\"parentId\":2012,\"isCache\":\"1\",\"path\":\"goodsAdd\",\"component\":\"shop/goods/GoodsAdd\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1628844079000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2025,\"menuType\":\"C\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-08-13 16:42:10');
INSERT INTO `sys_oper_log` VALUES (142, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"1\",\"icon\":\"checkbox\",\"orderNum\":\"7\",\"menuName\":\"商品修改\",\"remark\":\"\",\"updateTime\":1628844166313,\"params\":{},\"parentId\":2012,\"isCache\":\"1\",\"path\":\"goodsUpdate\",\"component\":\"shop/goods/GoodsAdd\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1628844079000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2025,\"menuType\":\"C\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-08-13 16:42:46');
INSERT INTO `sys_oper_log` VALUES (143, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', '', '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":1629252840801,\"dictName\":\"商品分类\",\"remark\":\"商品分类\",\"updateTime\":1629252840801,\"dictId\":106,\"params\":{},\"dictType\":\"goods_type\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-08-18 10:14:01');
INSERT INTO `sys_oper_log` VALUES (144, '字典类型', 3, 'com.ruoyi.web.controller.system.SysDictTypeController.remove()', 'DELETE', 1, 'admin', '', '/system/dict/type/106', '127.0.0.1', '内网IP', '{dictIds=106}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-08-18 10:14:33');
INSERT INTO `sys_oper_log` VALUES (145, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"education\",\"orderNum\":\"9\",\"menuName\":\"文章管理\",\"updateTime\":1629266865816,\"params\":{},\"parentId\":2012,\"isCache\":\"0\",\"path\":\"article\",\"component\":\"shop/article\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1629266865816,\"isFrame\":\"1\",\"menuId\":2026,\"menuType\":\"C\",\"perms\":\"shop:article:list\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-08-18 14:07:46');
INSERT INTO `sys_oper_log` VALUES (146, '字典类型', 2, 'com.ruoyi.web.controller.system.SysDictTypeController.edit()', 'PUT', 1, 'admin', '', '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":1625465982000,\"updateBy\":\"admin\",\"dictName\":\"文章类型\",\"updateTime\":1629268889423,\"dictId\":105,\"params\":{},\"dictType\":\"article_type\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-08-18 14:41:30');
INSERT INTO `sys_oper_log` VALUES (147, '字典类型', 2, 'com.ruoyi.web.controller.system.SysDictTypeController.edit()', 'PUT', 1, 'admin', '', '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":1625465982000,\"updateBy\":\"admin\",\"dictName\":\"优惠券类型\",\"updateTime\":1629269370358,\"dictId\":105,\"params\":{},\"dictType\":\"coupon_type\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-08-18 14:49:31');
INSERT INTO `sys_oper_log` VALUES (148, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', '', '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":1629269426496,\"dictName\":\"article_type\",\"remark\":\"article_type\",\"updateTime\":1629269426496,\"dictId\":107,\"params\":{},\"dictType\":\"文章类型\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-08-18 14:50:27');
INSERT INTO `sys_oper_log` VALUES (149, '字典类型', 2, 'com.ruoyi.web.controller.system.SysDictTypeController.edit()', 'PUT', 1, 'admin', '', '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":1629269426000,\"updateBy\":\"admin\",\"dictName\":\"文章类型\",\"remark\":\"\",\"updateTime\":1629269504167,\"dictId\":107,\"params\":{},\"dictType\":\"article_type\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-08-18 14:51:45');
INSERT INTO `sys_oper_log` VALUES (150, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', '', '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"0\",\"dictSort\":0,\"remark\":\"活动公告\",\"updateTime\":1629269530009,\"params\":{},\"dictType\":\"article_type\",\"dictLabel\":\"活动公告\",\"createBy\":\"admin\",\"default\":false,\"createTime\":1629269530009,\"dictCode\":110,\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-08-18 14:52:10');
INSERT INTO `sys_oper_log` VALUES (151, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', '', '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"1\",\"dictSort\":1,\"remark\":\"独立文章\",\"updateTime\":1629269541660,\"params\":{},\"dictType\":\"article_type\",\"dictLabel\":\"独立文章\",\"createBy\":\"admin\",\"default\":false,\"createTime\":1629269541660,\"dictCode\":111,\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-08-18 14:52:22');
INSERT INTO `sys_oper_log` VALUES (152, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', '', '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"2\",\"dictSort\":2,\"remark\":\"公告\",\"updateTime\":1629269563965,\"params\":{},\"dictType\":\"article_type\",\"dictLabel\":\"公告\",\"createBy\":\"admin\",\"default\":false,\"createTime\":1629269563965,\"dictCode\":112,\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-08-18 14:52:44');
INSERT INTO `sys_oper_log` VALUES (153, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"orderNum\":\"9\",\"menuName\":\"新增文章\",\"updateTime\":1629270821573,\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"articleAdd\",\"component\":\"shop/articleAdd\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1629270821573,\"isFrame\":\"1\",\"menuId\":2027,\"menuType\":\"C\",\"perms\":\"shop:article:update\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-08-18 15:13:42');
INSERT INTO `sys_oper_log` VALUES (154, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"#\",\"orderNum\":\"9\",\"menuName\":\"新增文章\",\"remark\":\"\",\"updateTime\":1629270854801,\"params\":{},\"parentId\":2012,\"isCache\":\"0\",\"path\":\"articleAdd\",\"component\":\"shop/articleAdd\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1629270822000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2027,\"menuType\":\"C\",\"perms\":\"shop:article:update\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-08-18 15:14:15');
INSERT INTO `sys_oper_log` VALUES (155, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"education\",\"orderNum\":\"8\",\"menuName\":\"文章管理\",\"remark\":\"\",\"updateTime\":1629270863786,\"params\":{},\"parentId\":2012,\"isCache\":\"0\",\"path\":\"article\",\"component\":\"shop/article\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1629266866000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2026,\"menuType\":\"C\",\"perms\":\"shop:article:list\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-08-18 15:14:24');
INSERT INTO `sys_oper_log` VALUES (156, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"1\",\"icon\":\"#\",\"orderNum\":\"9\",\"menuName\":\"新增文章\",\"remark\":\"\",\"updateTime\":1629271660977,\"params\":{},\"parentId\":2012,\"isCache\":\"1\",\"path\":\"articleAdd\",\"component\":\"shop/articleAdd\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1629270822000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2027,\"menuType\":\"C\",\"perms\":\"shop:article:update\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-08-18 15:27:41');
INSERT INTO `sys_oper_log` VALUES (157, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"1\",\"icon\":\"#\",\"orderNum\":\"9\",\"menuName\":\"新增文章\",\"remark\":\"\",\"updateTime\":1629273081415,\"params\":{},\"parentId\":2012,\"isCache\":\"1\",\"path\":\"articleAdd\",\"component\":\"shop/article/articleAdd\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1629270822000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2027,\"menuType\":\"C\",\"perms\":\"shop:article:update\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-08-18 15:51:22');
INSERT INTO `sys_oper_log` VALUES (158, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"education\",\"orderNum\":\"8\",\"menuName\":\"文章管理\",\"remark\":\"\",\"updateTime\":1629273133924,\"params\":{},\"parentId\":2012,\"isCache\":\"0\",\"path\":\"article\",\"component\":\"shop/article/index\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1629266866000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2026,\"menuType\":\"C\",\"perms\":\"shop:article:list\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-08-18 15:52:14');
INSERT INTO `sys_oper_log` VALUES (159, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"1\",\"orderNum\":\"10\",\"menuName\":\"文章修改\",\"updateTime\":1629338481314,\"params\":{},\"parentId\":2012,\"isCache\":\"1\",\"path\":\"articleUpdate\",\"component\":\"shop/article/articleAdd\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1629338481314,\"isFrame\":\"1\",\"menuId\":2028,\"menuType\":\"C\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-08-19 10:01:22');
INSERT INTO `sys_oper_log` VALUES (160, '菜单管理', 1, 'com.ysweet.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"example\",\"orderNum\":\"11\",\"menuName\":\"餐桌管理\",\"updateTime\":1634202527710,\"params\":{},\"parentId\":2012,\"isCache\":\"0\",\"path\":\"desk\",\"component\":\"shop/desk/index\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1634202527710,\"isFrame\":\"1\",\"menuId\":2029,\"menuType\":\"C\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-10-14 17:08:48');
INSERT INTO `sys_oper_log` VALUES (161, '菜单管理', 1, 'com.ysweet.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"dict\",\"orderNum\":\"12\",\"menuName\":\"订单管理\",\"updateTime\":1634204523287,\"params\":{},\"parentId\":2012,\"isCache\":\"0\",\"path\":\"shop/order/index\",\"component\":\"order\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1634204523287,\"isFrame\":\"1\",\"menuId\":2030,\"menuType\":\"C\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-10-14 17:42:03');
INSERT INTO `sys_oper_log` VALUES (162, '菜单管理', 2, 'com.ysweet.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '', '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"dict\",\"orderNum\":\"12\",\"menuName\":\"订单管理\",\"remark\":\"\",\"updateTime\":1634204589072,\"params\":{},\"parentId\":2012,\"isCache\":\"0\",\"path\":\"order\",\"component\":\"shop/order/index\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1634204523000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2030,\"menuType\":\"C\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\"}', 0, '', '2021-10-14 17:43:09');

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `post_id` bigint NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '岗位名称',
  `post_sort` int NOT NULL COMMENT '显示顺序',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '岗位信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, 'ceo', '董事长', 1, '0', 'admin', '2021-05-26 17:27:09', '', NULL, '');
INSERT INTO `sys_post` VALUES (4, 'user', '普通员工', 4, '0', 'admin', '2021-05-26 17:27:09', '', NULL, '');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色权限字符串',
  `role_sort` int NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `menu_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '菜单树选择项是否关联显示',
  `dept_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '部门树选择项是否关联显示',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'admin', 1, '1', 1, 1, '0', '0', 'admin', '2021-05-26 17:27:09', '', NULL, '超级管理员');
INSERT INTO `sys_role` VALUES (2, '普通角色', 'common', 2, '2', 1, 1, '0', '0', 'admin', '2021-05-26 17:27:09', 'admin', '2021-05-28 15:13:20', '普通角色');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `role_id` bigint NOT NULL COMMENT '角色ID',
  `dept_id` bigint NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色和部门关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES (2, 100);
INSERT INTO `sys_role_dept` VALUES (2, 101);
INSERT INTO `sys_role_dept` VALUES (2, 105);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint NOT NULL COMMENT '角色ID',
  `menu_id` bigint NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (2, 1);
INSERT INTO `sys_role_menu` VALUES (2, 2);
INSERT INTO `sys_role_menu` VALUES (2, 3);
INSERT INTO `sys_role_menu` VALUES (2, 100);
INSERT INTO `sys_role_menu` VALUES (2, 101);
INSERT INTO `sys_role_menu` VALUES (2, 102);
INSERT INTO `sys_role_menu` VALUES (2, 103);
INSERT INTO `sys_role_menu` VALUES (2, 104);
INSERT INTO `sys_role_menu` VALUES (2, 105);
INSERT INTO `sys_role_menu` VALUES (2, 106);
INSERT INTO `sys_role_menu` VALUES (2, 107);
INSERT INTO `sys_role_menu` VALUES (2, 108);
INSERT INTO `sys_role_menu` VALUES (2, 109);
INSERT INTO `sys_role_menu` VALUES (2, 110);
INSERT INTO `sys_role_menu` VALUES (2, 111);
INSERT INTO `sys_role_menu` VALUES (2, 112);
INSERT INTO `sys_role_menu` VALUES (2, 113);
INSERT INTO `sys_role_menu` VALUES (2, 114);
INSERT INTO `sys_role_menu` VALUES (2, 115);
INSERT INTO `sys_role_menu` VALUES (2, 116);
INSERT INTO `sys_role_menu` VALUES (2, 500);
INSERT INTO `sys_role_menu` VALUES (2, 501);
INSERT INTO `sys_role_menu` VALUES (2, 1001);
INSERT INTO `sys_role_menu` VALUES (2, 1002);
INSERT INTO `sys_role_menu` VALUES (2, 1003);
INSERT INTO `sys_role_menu` VALUES (2, 1004);
INSERT INTO `sys_role_menu` VALUES (2, 1005);
INSERT INTO `sys_role_menu` VALUES (2, 1006);
INSERT INTO `sys_role_menu` VALUES (2, 1007);
INSERT INTO `sys_role_menu` VALUES (2, 1008);
INSERT INTO `sys_role_menu` VALUES (2, 1009);
INSERT INTO `sys_role_menu` VALUES (2, 1010);
INSERT INTO `sys_role_menu` VALUES (2, 1011);
INSERT INTO `sys_role_menu` VALUES (2, 1012);
INSERT INTO `sys_role_menu` VALUES (2, 1013);
INSERT INTO `sys_role_menu` VALUES (2, 1014);
INSERT INTO `sys_role_menu` VALUES (2, 1015);
INSERT INTO `sys_role_menu` VALUES (2, 1016);
INSERT INTO `sys_role_menu` VALUES (2, 1017);
INSERT INTO `sys_role_menu` VALUES (2, 1018);
INSERT INTO `sys_role_menu` VALUES (2, 1019);
INSERT INTO `sys_role_menu` VALUES (2, 1020);
INSERT INTO `sys_role_menu` VALUES (2, 1021);
INSERT INTO `sys_role_menu` VALUES (2, 1022);
INSERT INTO `sys_role_menu` VALUES (2, 1023);
INSERT INTO `sys_role_menu` VALUES (2, 1024);
INSERT INTO `sys_role_menu` VALUES (2, 1025);
INSERT INTO `sys_role_menu` VALUES (2, 1026);
INSERT INTO `sys_role_menu` VALUES (2, 1027);
INSERT INTO `sys_role_menu` VALUES (2, 1028);
INSERT INTO `sys_role_menu` VALUES (2, 1029);
INSERT INTO `sys_role_menu` VALUES (2, 1030);
INSERT INTO `sys_role_menu` VALUES (2, 1031);
INSERT INTO `sys_role_menu` VALUES (2, 1032);
INSERT INTO `sys_role_menu` VALUES (2, 1033);
INSERT INTO `sys_role_menu` VALUES (2, 1034);
INSERT INTO `sys_role_menu` VALUES (2, 1035);
INSERT INTO `sys_role_menu` VALUES (2, 1036);
INSERT INTO `sys_role_menu` VALUES (2, 1037);
INSERT INTO `sys_role_menu` VALUES (2, 1038);
INSERT INTO `sys_role_menu` VALUES (2, 1039);
INSERT INTO `sys_role_menu` VALUES (2, 1040);
INSERT INTO `sys_role_menu` VALUES (2, 1041);
INSERT INTO `sys_role_menu` VALUES (2, 1042);
INSERT INTO `sys_role_menu` VALUES (2, 1043);
INSERT INTO `sys_role_menu` VALUES (2, 1044);
INSERT INTO `sys_role_menu` VALUES (2, 1045);
INSERT INTO `sys_role_menu` VALUES (2, 1046);
INSERT INTO `sys_role_menu` VALUES (2, 1047);
INSERT INTO `sys_role_menu` VALUES (2, 1048);
INSERT INTO `sys_role_menu` VALUES (2, 1049);
INSERT INTO `sys_role_menu` VALUES (2, 1050);
INSERT INTO `sys_role_menu` VALUES (2, 1051);
INSERT INTO `sys_role_menu` VALUES (2, 1052);
INSERT INTO `sys_role_menu` VALUES (2, 1053);
INSERT INTO `sys_role_menu` VALUES (2, 1054);
INSERT INTO `sys_role_menu` VALUES (2, 1055);
INSERT INTO `sys_role_menu` VALUES (2, 1056);
INSERT INTO `sys_role_menu` VALUES (2, 1057);
INSERT INTO `sys_role_menu` VALUES (2, 1058);
INSERT INTO `sys_role_menu` VALUES (2, 1059);
INSERT INTO `sys_role_menu` VALUES (2, 1060);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint NULL DEFAULT NULL COMMENT '部门ID',
  `user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户账号',
  `nick_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '00' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '头像地址',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '密码',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime NULL DEFAULT NULL COMMENT '最后登录时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 103, 'admin', '小金木', '00', 'dovi_mc@163.com', '15994051442', '1', 'https://xjm-picture.oss-cn-beijing.aliyuncs.com/shop/avatar/20210707152800278954.blob', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2021-11-08 15:22:33', 'admin', '2021-05-26 17:27:09', 'admin', '2021-11-08 15:22:33', '管理员');

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `post_id` bigint NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户与岗位关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (1, 1);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `role_id` bigint NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户和角色关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);

-- ----------------------------
-- Table structure for ys_article
-- ----------------------------
DROP TABLE IF EXISTS `ys_article`;
CREATE TABLE `ys_article`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '文章编号',
  `type` int NOT NULL DEFAULT 0 COMMENT '0活动公告，1独立文章，2公告',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文章标题',
  `summary` varchar(140) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '文章摘要',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '文章正文',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '文章标题图片',
  `is_hidden` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否显示 0 显示 1不显示',
  `author` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发布者用户名 ',
  `create_time` datetime NOT NULL COMMENT '文章发布时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `is_del` int NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 AVG_ROW_LENGTH = 6553 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'CMS文章表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of ys_article
-- ----------------------------
INSERT INTO `ys_article` VALUES (1, 1, '比特币暴涨,显卡涨幅明显', '比特币', '8月8日，亿邦国际方面称，华铁恒安购买的矿机所对应的其中一个比特币挖矿矿池、矿工号、比特币收益及比特币钱包地址全部归属于号码为139开头的手机用户，而该手机用户恰巧是华铁应急实控人胡丹锋的妻子潘倩。8月9日，《每日经济新闻》记者拨打了该手机号，但电话无法接通。', 'https://xjm-picture.oss-cn-beijing.aliyuncs.com/shop/-1/20210707162149781205.png', 0, 'admin', '2010-01-01 00:00:00', NULL, 0);
INSERT INTO `ys_article` VALUES (4, 0, '法国新冠累计确诊超650万', '', '<p>中新社北京8月18日电 综合消息：世界卫生组织17日公布的最新数据显示，截至欧洲中部时间17日18时28分(北京时间18日零时28分)，全球确诊病例较前一日增加410464例，达到207784507例；死亡病例增加7137例，达到4370424例。<s>﻿</s></p>', 'https://xjm-picture.oss-cn-beijing.aliyuncs.com/shop/-1/20210816143718562220.jpeg', 0, 'admin', '2021-08-18 16:59:24', '2021-08-18 17:33:25', 0);
INSERT INTO `ys_article` VALUES (5, 0, '222222222222', '', '<p><img src=\"https://xjm-picture.oss-cn-beijing.aliyuncs.com/shop/null/20210819092525236490.jpeg\"></p>', 'https://xjm-picture.oss-cn-beijing.aliyuncs.com/shop/-1/20210816143718562220.jpeg', 0, 'admin', '2021-08-19 09:25:51', NULL, 0);

-- ----------------------------
-- Table structure for ys_banner
-- ----------------------------
DROP TABLE IF EXISTS `ys_banner`;
CREATE TABLE `ys_banner`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'Banner名称，通常作为标识',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'Banner描述',
  `image_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '广告的图片,不能为空',
  `redirect_type` int NULL DEFAULT NULL COMMENT '跳转的类型',
  `key_str` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '关键字,不同跳转有不同作用,可能是商品的id,也可能是文章的链接等等',
  `group_id` int NOT NULL COMMENT '广告分类',
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '是否显示广告,默认是1显示',
  `create_time` datetime NOT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  `is_del` int NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = 'banner管理表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ys_banner
-- ----------------------------
INSERT INTO `ys_banner` VALUES (1, '首页', '新品上市', 'https://xjm-picture.oss-cn-beijing.aliyuncs.com/shop/-1/20210707144914879420.jpg', 1, '', 1, b'1', '2021-06-21 14:34:33', '2021-07-10 15:58:36', 0);
INSERT INTO `ys_banner` VALUES (6, 'dfds', 'fdsfsd', 'https://xjm-picture.oss-cn-beijing.aliyuncs.com/shop/-1/20210707162150079109.png', 0, NULL, 1, b'1', '2021-07-10 15:59:33', NULL, 1);

-- ----------------------------
-- Table structure for ys_category
-- ----------------------------
DROP TABLE IF EXISTS `ys_category`;
CREATE TABLE `ys_category`  (
  `category_id` int NOT NULL AUTO_INCREMENT,
  `category_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `short_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '商品分类简称 ',
  `is_visible` int NOT NULL DEFAULT 1 COMMENT '是否显示  1 显示 0 不显示',
  `sort` int NULL DEFAULT NULL,
  `category_pic` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '商品分类图片',
  `create_time` datetime NOT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  `is_del` int NULL DEFAULT 0,
  PRIMARY KEY (`category_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 AVG_ROW_LENGTH = 244 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品分类表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ys_category
-- ----------------------------
INSERT INTO `ys_category` VALUES (1, '主食', '主食', 1, 1, 'https://xjm-picture.oss-cn-beijing.aliyuncs.com/shop/-1/20210707162150079923.png', '2021-07-07 15:14:09', '2021-07-12 09:26:01', 0);
INSERT INTO `ys_category` VALUES (2, '饮品', '饮品', 1, 2, 'https://xjm-picture.oss-cn-beijing.aliyuncs.com/shop/-1/20210707162149781205.png', '2021-07-07 15:50:21', '2021-08-17 16:16:11', 0);

-- ----------------------------
-- Table structure for ys_coupon
-- ----------------------------
DROP TABLE IF EXISTS `ys_coupon`;
CREATE TABLE `ys_coupon`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `coupon_type` int NOT NULL DEFAULT 3 COMMENT '类型1:店铺优惠券',
  `coupon_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '优惠券名称',
  `coupon_status` int NOT NULL DEFAULT 1 COMMENT '状态1:使用1次，2使用无数次',
  `is_show` int NOT NULL DEFAULT 0 COMMENT '是否能领取,0不可领取,1能领取',
  `stock` int NULL DEFAULT 0 COMMENT '库存null为无限张',
  `coupon_full` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '满多少，0为无门槛',
  `reduce` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '减多少',
  `start_time` datetime NULL DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime NULL DEFAULT NULL COMMENT '结束时间',
  `coupon_day` int NULL DEFAULT NULL COMMENT '使用时间',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `is_del` int NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '优惠券表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ys_coupon
-- ----------------------------
INSERT INTO `ys_coupon` VALUES (1, 1, '无门槛5元优惠', 1, 0, 666, 0.00, 5.00, '2021-07-07 00:00:00', '2021-08-25 00:00:00', 0, '2021-06-26 11:47:15', '2021-06-26 11:47:18', 0);

-- ----------------------------
-- Table structure for ys_desk
-- ----------------------------
DROP TABLE IF EXISTS `ys_desk`;
CREATE TABLE `ys_desk`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `desk_number` int NOT NULL COMMENT '桌号',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `h5_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'H5二维码',
  `xcx_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '小程序二维码',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `is_del` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of ys_desk
-- ----------------------------
INSERT INTO `ys_desk` VALUES (1, 1, '1号餐桌', NULL, NULL, '2021-08-20 11:45:17', '2021-08-20 11:45:20', 0);

-- ----------------------------
-- Table structure for ys_favorites
-- ----------------------------
DROP TABLE IF EXISTS `ys_favorites`;
CREATE TABLE `ys_favorites`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '记录ID',
  `uid` int NOT NULL COMMENT '用户ID',
  `fav_id` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '商品ID',
  `img_id` int NULL DEFAULT NULL,
  `price` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '商品收藏时价格',
  `create_time` int NULL DEFAULT 0 COMMENT '收藏时间',
  `ucid` int NOT NULL DEFAULT 3 COMMENT 'ucid',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AVG_ROW_LENGTH = 8192 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '收藏表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ys_favorites
-- ----------------------------

-- ----------------------------
-- Table structure for ys_fx_goods
-- ----------------------------
DROP TABLE IF EXISTS `ys_fx_goods`;
CREATE TABLE `ys_fx_goods`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `goods_id` int NOT NULL DEFAULT 0,
  `price` float NOT NULL DEFAULT 0 COMMENT '价格',
  `created_at` int NOT NULL DEFAULT 0,
  `updated_at` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '分销商品表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ys_fx_goods
-- ----------------------------

-- ----------------------------
-- Table structure for ys_fx_record
-- ----------------------------
DROP TABLE IF EXISTS `ys_fx_record`;
CREATE TABLE `ys_fx_record`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL DEFAULT 0,
  `order_id` int NOT NULL DEFAULT 0,
  `goods_id` int NOT NULL DEFAULT 0,
  `money` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '分销提成',
  `created_at` int NOT NULL DEFAULT 0,
  `updated_at` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '分销记录表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ys_fx_record
-- ----------------------------

-- ----------------------------
-- Table structure for ys_goods
-- ----------------------------
DROP TABLE IF EXISTS `ys_goods`;
CREATE TABLE `ys_goods`  (
  `good_id` int UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '商品id(SKU)',
  `good_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '商品名称',
  `category_id` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '商品分类id',
  `spec_type` int NULL DEFAULT 0 COMMENT '规格类型',
  `show_image` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品主图(商品列表展示图片)',
  `keywords` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '商品关键词',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '商品简介，促销语',
  `detail_images` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '展示图片(详情页面滚动图片)',
  `is_hot` int NOT NULL DEFAULT 0 COMMENT '是否热销商品',
  `is_recommend` int NOT NULL DEFAULT 0 COMMENT '是否推荐',
  `sort` int NOT NULL DEFAULT 0 COMMENT '排序',
  `is_new` int NOT NULL DEFAULT 0 COMMENT '是否新品',
  `sold_out` tinyint NOT NULL DEFAULT 0 COMMENT '是否下架 1下架，0正常',
  `create_time` datetime NOT NULL COMMENT '商品添加时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '商品编辑时间',
  `is_del` int NULL DEFAULT NULL,
  PRIMARY KEY (`good_id`) USING BTREE,
  INDEX `UK_ns_goods_category_id`(`category_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 AVG_ROW_LENGTH = 16554 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of ys_goods
-- ----------------------------
INSERT INTO `ys_goods` VALUES (2, '水果奶茶', 1, 1, 'https://xjm-picture.oss-cn-beijing.aliyuncs.com/shop/-1/20210707144914879420.jpg', '水果奶茶', '香甜可口，流连忘返', '[\"https://xjm-picture.oss-cn-beijing.aliyuncs.com/shop/-1/20210707162149781205.png\"]', 1, 0, 1, 1, 0, '2021-08-13 11:38:28', '2021-08-18 10:54:27', 0);
INSERT INTO `ys_goods` VALUES (3, '卫龙辣条', 2, 0, 'https://xjm-picture.oss-cn-beijing.aliyuncs.com/shop/-1/20210816143718562220.jpeg', '辣条', '卫龙辣条 畅销30年', '[\"https://xjm-picture.oss-cn-beijing.aliyuncs.com/shop/-1/20210816143718562220.jpeg\"]', 0, 1, 2, 0, 0, '2021-08-16 14:37:48', '2021-08-18 10:51:36', 0);

-- ----------------------------
-- Table structure for ys_goods_sku
-- ----------------------------
DROP TABLE IF EXISTS `ys_goods_sku`;
CREATE TABLE `ys_goods_sku`  (
  `sku_id` int NOT NULL AUTO_INCREMENT COMMENT '表序号',
  `goods_id` int NOT NULL DEFAULT 0 COMMENT '商品编号',
  `json` json NOT NULL,
  PRIMARY KEY (`sku_id`) USING BTREE,
  UNIQUE INDEX `goods_id`(`goods_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 AVG_ROW_LENGTH = 481 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品skui规格价格库存信息表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of ys_goods_sku
-- ----------------------------
INSERT INTO `ys_goods_sku` VALUES (1, 2, '{\"data\": [{\"attr\": \"多冰,椰果\", \"price\": 22, \"stock\": 111, \"value1\": \"多冰\", \"value2\": \"椰果\", \"costPrice\": 11}, {\"attr\": \"多冰,红豆\", \"price\": 33, \"stock\": 111, \"value1\": \"多冰\", \"value2\": \"红豆\", \"costPrice\": 22}, {\"attr\": \"多冰,酸奶\", \"price\": 44, \"stock\": 111, \"value1\": \"多冰\", \"value2\": \"酸奶\", \"costPrice\": 33}, {\"attr\": \"多冰,花生碎\", \"price\": 12, \"stock\": 111, \"value1\": \"多冰\", \"value2\": \"花生碎\", \"costPrice\": 23}, {\"attr\": \"少冰,椰果\", \"price\": 32, \"stock\": 111, \"value1\": \"少冰\", \"value2\": \"椰果\", \"costPrice\": 12}, {\"attr\": \"少冰,红豆\", \"price\": 32, \"stock\": 111, \"value1\": \"少冰\", \"value2\": \"红豆\", \"costPrice\": 21}, {\"attr\": \"少冰,酸奶\", \"price\": 22, \"stock\": 111, \"value1\": \"少冰\", \"value2\": \"酸奶\", \"costPrice\": 15}, {\"attr\": \"少冰,花生碎\", \"price\": 21, \"stock\": 111, \"value1\": \"少冰\", \"value2\": \"花生碎\", \"costPrice\": 17}, {\"attr\": \"无冰,椰果\", \"price\": 21, \"stock\": 1111, \"value1\": \"无冰\", \"value2\": \"椰果\", \"costPrice\": 12}, {\"attr\": \"无冰,红豆\", \"price\": 21, \"stock\": 111, \"value1\": \"无冰\", \"value2\": \"红豆\", \"costPrice\": 18}, {\"attr\": \"无冰,酸奶\", \"price\": 23, \"stock\": 111, \"value1\": \"无冰\", \"value2\": \"酸奶\", \"costPrice\": 19}, {\"attr\": \"无冰,花生碎\", \"price\": 23, \"stock\": 111, \"value1\": \"无冰\", \"value2\": \"花生碎\", \"costPrice\": 8}], \"head\": [{\"slot\": \"value1\", \"title\": \"加冰\"}, {\"slot\": \"value2\", \"title\": \"加料\"}, {\"slot\": \"price\", \"title\": \"原价\"}, {\"slot\": \"costPrice\", \"title\": \"折扣价\"}, {\"slot\": \"stock\", \"title\": \"库存\"}, {\"slot\": \"action\", \"title\": \"操作\"}], \"attrs\": [{\"value\": \"加冰\", \"detail\": [\"多冰\", \"少冰\", \"无冰\"]}, {\"value\": \"加料\", \"detail\": [\"椰果\", \"红豆\", \"酸奶\", \"花生碎\"]}]}');
INSERT INTO `ys_goods_sku` VALUES (3, 3, '{\"data\": [{\"price\": 3, \"stock\": 111111, \"costPrice\": 2}], \"head\": []}');

-- ----------------------------
-- Table structure for ys_group
-- ----------------------------
DROP TABLE IF EXISTS `ys_group`;
CREATE TABLE `ys_group`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `rule` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '规则数组',
  `update_time` int NULL DEFAULT NULL,
  `create_time` int NULL DEFAULT NULL,
  `ucid` int NOT NULL DEFAULT 3,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of ys_group
-- ----------------------------
INSERT INTO `ys_group` VALUES (1, '超级管理员', '', NULL, NULL, 3);

-- ----------------------------
-- Table structure for ys_group_rule
-- ----------------------------
DROP TABLE IF EXISTS `ys_group_rule`;
CREATE TABLE `ys_group_rule`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ucid` int NOT NULL DEFAULT 3,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of ys_group_rule
-- ----------------------------

-- ----------------------------
-- Table structure for ys_image
-- ----------------------------
DROP TABLE IF EXISTS `ys_image`;
CREATE TABLE `ys_image`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '图片路径',
  `from` tinyint NOT NULL DEFAULT 1 COMMENT '1 来自本地，2 来自公网',
  `use_name` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `category_id` int NOT NULL COMMENT '图片分类',
  `is_visible` int NOT NULL DEFAULT 1 COMMENT '是否能显示1能0不能',
  `deleted_at` int NULL DEFAULT NULL,
  `created_at` int NULL DEFAULT NULL,
  `updated_at` int NULL DEFAULT NULL,
  `ucid` int NOT NULL DEFAULT 3 COMMENT 'ucid',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '图片总表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of ys_image
-- ----------------------------

-- ----------------------------
-- Table structure for ys_image_category
-- ----------------------------
DROP TABLE IF EXISTS `ys_image_category`;
CREATE TABLE `ys_image_category`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `category_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `pid` int NOT NULL,
  `level` int NOT NULL,
  `is_visible` int NOT NULL,
  `ucid` int NOT NULL DEFAULT 3 COMMENT 'ucid',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of ys_image_category
-- ----------------------------

-- ----------------------------
-- Table structure for ys_material
-- ----------------------------
DROP TABLE IF EXISTS `ys_material`;
CREATE TABLE `ys_material`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'PK',
  `create_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者ID',
  `type` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '类型1、图片；2、视频',
  `group_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '分组ID',
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '素材名',
  `url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '素材链接',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL,
  `is_del` int NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '素材库' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ys_material
-- ----------------------------
INSERT INTO `ys_material` VALUES ('11ca610d0dec2d8b5250866b85468c09', 'admin', '1', NULL, 'banner1.jpg', 'https://xjm-picture.oss-cn-beijing.aliyuncs.com/shop/-1/20210707144914879420.jpg', '2021-07-07 14:49:16', NULL, 0);
INSERT INTO `ys_material` VALUES ('5076f8d4600d068f361837b406906401', 'admin', '1', NULL, '1a616980219343bf60c4f0b62b4c2ae1.jpeg', 'https://xjm-picture.oss-cn-beijing.aliyuncs.com/shop/-1/20210816143718562220.jpeg', '2021-08-16 14:37:20', NULL, 0);
INSERT INTO `ys_material` VALUES ('7eeabf842fb791638673fa3153f10839', 'admin', '1', NULL, '5c9ccca90d2d3.png', 'https://xjm-picture.oss-cn-beijing.aliyuncs.com/shop/-1/20210707162150079923.png', '2021-07-07 16:21:51', NULL, 0);
INSERT INTO `ys_material` VALUES ('855db72d362abc1154a6b6dbdd76e8bf', 'admin', '1', NULL, '5c9ccca8bc1e0.png', 'https://xjm-picture.oss-cn-beijing.aliyuncs.com/shop/-1/20210707162150079109.png', '2021-07-07 16:21:51', NULL, 0);
INSERT INTO `ys_material` VALUES ('c6e410ebd2e7210f98d95e559b85bfa3', 'admin', '1', NULL, '5c9ccca8aa5b9.png', 'https://xjm-picture.oss-cn-beijing.aliyuncs.com/shop/-1/20210707162149781205.png', '2021-07-07 16:21:50', NULL, 0);
INSERT INTO `ys_material` VALUES ('f04fd2cd03048f766f404c3ec580e67b', 'admin', '1', NULL, '5c9ccca8aa5b9.png', 'https://xjm-picture.oss-cn-beijing.aliyuncs.com/shop/-1/20210707145010884038.png', '2021-07-07 14:50:11', NULL, 1);

-- ----------------------------
-- Table structure for ys_material_group
-- ----------------------------
DROP TABLE IF EXISTS `ys_material_group`;
CREATE TABLE `ys_material_group`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'PK',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者ID',
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '分组名',
  `update_time` datetime NULL DEFAULT NULL,
  `is_del` int NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '素材分组' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ys_material_group
-- ----------------------------
INSERT INTO `ys_material_group` VALUES ('2b854bcc104371c4a90f13283765e8c5', '2021-05-27 17:24:43', NULL, '新闻', '2021-06-18 16:19:49', 1);
INSERT INTO `ys_material_group` VALUES ('5b61232049e724f587989d376b260252', '2021-05-27 15:10:09', NULL, 'EP商品', NULL, 1);
INSERT INTO `ys_material_group` VALUES ('8cc5996f6c4b54fcf5c0f2845a5d9afa', '2020-06-26 11:07:26', NULL, '商品', '2020-06-26 18:20:14', 0);
INSERT INTO `ys_material_group` VALUES ('98a76c9e94d8691deab95bd2908cbab2', '2021-05-25 10:46:03', NULL, '装修', NULL, 1);
INSERT INTO `ys_material_group` VALUES ('d9b1b9d69f5b72cda63e26dc9f087623', '2020-06-26 18:20:19', NULL, 'icon', NULL, 1);

-- ----------------------------
-- Table structure for ys_money_log
-- ----------------------------
DROP TABLE IF EXISTS `ys_money_log`;
CREATE TABLE `ys_money_log`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `uid` int NOT NULL,
  `type_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `content` varchar(800) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `operator` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `wx_refund` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '微信退款id',
  `created_at` int NULL DEFAULT NULL,
  `ucid` int NOT NULL DEFAULT 3 COMMENT 'ucid',
  `updated_at` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订单日志表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ys_money_log
-- ----------------------------

-- ----------------------------
-- Table structure for ys_nav
-- ----------------------------
DROP TABLE IF EXISTS `ys_nav`;
CREATE TABLE `ys_nav`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `nav_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `img_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图片ID',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '转跳路径',
  `url_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '转跳名字',
  `category_id` int NOT NULL DEFAULT 0 COMMENT '转跳分类ID',
  `is_visible` int NOT NULL COMMENT '	是否显示 1 显示 0 不显示	',
  `sort` int NOT NULL,
  `other` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '其他',
  `ucid` int NOT NULL DEFAULT 3 COMMENT 'ucid',
  `created_at` int NULL DEFAULT NULL,
  `deleted_at` int NULL DEFAULT NULL,
  `updated_at` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '导航栏' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ys_nav
-- ----------------------------

-- ----------------------------
-- Table structure for ys_order
-- ----------------------------
DROP TABLE IF EXISTS `ys_order`;
CREATE TABLE `ys_order`  (
  `order_id` int NOT NULL AUTO_INCREMENT,
  `order_num` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '订单编号',
  `user_id` int NULL DEFAULT NULL,
  `state` int NOT NULL DEFAULT 0 COMMENT '0未完成 1已完成 2已评价 -1退款中 -2已退款-3关闭订单',
  `payment_state` int NOT NULL DEFAULT 0 COMMENT '支付状态 0 1',
  `coupon_id` int NULL DEFAULT 0 COMMENT '优惠券ID',
  `payment_type` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '支付来源',
  `goods_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '第一个物品的名字',
  `goods_money` decimal(10, 2) NOT NULL COMMENT '商品总价',
  `coupon_money` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '优惠券价格',
  `order_money` decimal(10, 2) NOT NULL COMMENT '订单总价',
  `user_ip` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `goods_picture` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `message` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `pay_time` int NULL DEFAULT NULL COMMENT '支付时间',
  `pay_cate` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '3' COMMENT '支付方式1微信支付2余额支付3暂不支付',
  `ucid` int NOT NULL DEFAULT 3,
  `table_num` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '几号桌',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  `is_del` int NULL DEFAULT 0,
  PRIMARY KEY (`order_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订单表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ys_order
-- ----------------------------
INSERT INTO `ys_order` VALUES (3, '1457615157155991552', 34, 0, 0, 0, 'wx', '卫龙辣条', 49.00, 0.00, 49.00, '127.0.0.1', 'https://xjm-picture.oss-cn-beijing.aliyuncs.com/shop/-1/20210816143718562220.jpeg', '不打包', NULL, '1', 3, '1', '2021-11-08 15:45:09', NULL, 0);

-- ----------------------------
-- Table structure for ys_order_goods
-- ----------------------------
DROP TABLE IF EXISTS `ys_order_goods`;
CREATE TABLE `ys_order_goods`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `order_id` int NOT NULL,
  `goods_id` int NOT NULL,
  `goods_name` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sku_name` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `price` decimal(10, 2) NOT NULL,
  `number` int NOT NULL,
  `total_price` decimal(10, 2) NOT NULL,
  `state` int NOT NULL DEFAULT 0 COMMENT '订单状态0未完成 1已完成 2已评价 -1退款中 -2已退款-3关闭订单	',
  `pic` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int NOT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  `is_del` int NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订单商品详情表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of ys_order_goods
-- ----------------------------
INSERT INTO `ys_order_goods` VALUES (1, 3, 3, '卫龙辣条', NULL, 2.00, 8, 16.00, 0, 'https://xjm-picture.oss-cn-beijing.aliyuncs.com/shop/-1/20210816143718562220.jpeg', 34, '2021-11-08 15:45:09', NULL, 0);
INSERT INTO `ys_order_goods` VALUES (2, 3, 2, '水果奶茶', '多冰,红豆', 33.00, 1, 33.00, 0, 'https://xjm-picture.oss-cn-beijing.aliyuncs.com/shop/-1/20210707144914879420.jpg', 34, '2021-11-08 15:45:09', NULL, 0);

-- ----------------------------
-- Table structure for ys_order_log
-- ----------------------------
DROP TABLE IF EXISTS `ys_order_log`;
CREATE TABLE `ys_order_log`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `order_id` int NOT NULL,
  `type_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `content` varchar(800) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `operator` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `wx_refund` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '微信退款id',
  `created_at` int NULL DEFAULT NULL,
  `ucid` int NOT NULL,
  `updated_at` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订单日志表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ys_order_log
-- ----------------------------

-- ----------------------------
-- Table structure for ys_play_award
-- ----------------------------
DROP TABLE IF EXISTS `ys_play_award`;
CREATE TABLE `ys_play_award`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `type_id` int NOT NULL COMMENT '类型ID',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `lv` int NOT NULL DEFAULT 0 COMMENT '概率',
  `award` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '奖励内容',
  `award_type` int NOT NULL DEFAULT 0 COMMENT '奖品类型(0为积分1为商品)',
  `stock` int NOT NULL DEFAULT 0,
  `img_id` int NOT NULL DEFAULT 0,
  `ucid` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '玩法奖励表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of ys_play_award
-- ----------------------------

-- ----------------------------
-- Table structure for ys_points_record
-- ----------------------------
DROP TABLE IF EXISTS `ys_points_record`;
CREATE TABLE `ys_points_record`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `uid` int UNSIGNED NOT NULL,
  `credittype` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `num` decimal(10, 2) NOT NULL DEFAULT 0.00,
  `operator` int UNSIGNED NULL DEFAULT 0,
  `module` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `clerk_id` int UNSIGNED NOT NULL DEFAULT 0,
  `store_id` int UNSIGNED NOT NULL DEFAULT 0,
  `clerk_type` tinyint UNSIGNED NOT NULL DEFAULT 1,
  `created_at` int NULL DEFAULT NULL,
  `remark` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `real_uniacid` int NOT NULL DEFAULT 0,
  `updated_at` int NULL DEFAULT NULL,
  `ucid` int NOT NULL DEFAULT 3,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `uid`(`uid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '积分记录表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of ys_points_record
-- ----------------------------

-- ----------------------------
-- Table structure for ys_rate
-- ----------------------------
DROP TABLE IF EXISTS `ys_rate`;
CREATE TABLE `ys_rate`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `order_id` int NOT NULL,
  `goods_id` int NULL DEFAULT NULL,
  `rate` int NOT NULL DEFAULT 5,
  `content` varchar(800) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `user_id` int NOT NULL,
  `imgs` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图片集',
  `headpic` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `nickname` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `reply_content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '回复内容',
  `reply_time` int NOT NULL DEFAULT 0 COMMENT '回复时间',
  `aid` int NOT NULL DEFAULT 0 COMMENT '管理员id',
  `created_at` int NULL DEFAULT NULL,
  `video` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '视频地址',
  `updated_at` int NULL DEFAULT NULL,
  `ucid` int NOT NULL DEFAULT 3 COMMENT 'ucid',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '评价表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of ys_rate
-- ----------------------------

-- ----------------------------
-- Table structure for ys_reduction
-- ----------------------------
DROP TABLE IF EXISTS `ys_reduction`;
CREATE TABLE `ys_reduction`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `statu` int NOT NULL COMMENT '是否开启 1开启，2关闭',
  `full` float(11, 2) NOT NULL COMMENT '满多少',
  `reduce` float(11, 2) NULL DEFAULT NULL COMMENT '减多少',
  `start_time` int NULL DEFAULT NULL COMMENT '开始时间',
  `end_time` int NULL DEFAULT NULL COMMENT '结束时间',
  `created_at` int NULL DEFAULT NULL COMMENT '创建 时间',
  `updated_at` int NULL DEFAULT NULL COMMENT '修改时间',
  `deleted_at` int NULL DEFAULT NULL COMMENT '删除时间',
  `ucid` int NOT NULL DEFAULT 3,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of ys_reduction
-- ----------------------------

-- ----------------------------
-- Table structure for ys_reduction_goods
-- ----------------------------
DROP TABLE IF EXISTS `ys_reduction_goods`;
CREATE TABLE `ys_reduction_goods`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `reduction_id` int NOT NULL COMMENT '满减活动id',
  `goods_id` int NOT NULL COMMENT '商品id',
  `ucid` int NOT NULL DEFAULT 3 COMMENT 'ucid',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of ys_reduction_goods
-- ----------------------------

-- ----------------------------
-- Table structure for ys_search
-- ----------------------------
DROP TABLE IF EXISTS `ys_search`;
CREATE TABLE `ys_search`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '热词',
  `num` int NOT NULL DEFAULT 0,
  `created_at` int NOT NULL DEFAULT 0,
  `updated_at` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '热搜表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ys_search
-- ----------------------------

-- ----------------------------
-- Table structure for ys_sys_backup
-- ----------------------------
DROP TABLE IF EXISTS `ys_sys_backup`;
CREATE TABLE `ys_sys_backup`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `size` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '大小',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '路径',
  `create_time` int NOT NULL,
  `ucid` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of ys_sys_backup
-- ----------------------------

-- ----------------------------
-- Table structure for ys_sys_config
-- ----------------------------
DROP TABLE IF EXISTS `ys_sys_config`;
CREATE TABLE `ys_sys_config`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `key_str` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `value_str` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '1基础2微信3支付宝4短信5物流6商家信息',
  `updated_time` date NULL DEFAULT NULL,
  `created_time` date NULL DEFAULT NULL,
  `is_del` int NOT NULL DEFAULT 0 COMMENT 'ucid',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 520 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统配置表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of ys_sys_config
-- ----------------------------
INSERT INTO `ys_sys_config` VALUES (2, 'gzhName', '', '公众号名字', 'WxConfig', NULL, '2021-06-24', 0);
INSERT INTO `ys_sys_config` VALUES (3, 'gzhAppId', '', '公众号appid', 'WxConfig', NULL, '2021-06-24', 0);
INSERT INTO `ys_sys_config` VALUES (4, 'gzhAppSecret', '', '公众号秘钥', 'WxConfig', NULL, '2021-06-24', 0);
INSERT INTO `ys_sys_config` VALUES (5, 'miniAppId', 'wx6abdf8110eae455f', '小程序appid', 'WxConfig', NULL, '2021-06-24', 0);
INSERT INTO `ys_sys_config` VALUES (6, 'miniAppSecret', '527e73e701b0fa9da7a194fece8bd507', '小程序秘钥', 'WxConfig', NULL, '2021-06-24', 0);
INSERT INTO `ys_sys_config` VALUES (21, 'getPoints', '1', '购物获得积分', '1', NULL, '2021-06-24', 0);
INSERT INTO `ys_sys_config` VALUES (22, 'usePoints', '0', '积分购物', '1', NULL, '2021-06-24', 0);
INSERT INTO `ys_sys_config` VALUES (34, 'qrCode', 'www.xxxx.com', '二维码链接', '1', NULL, '2021-06-24', 0);
INSERT INTO `ys_sys_config` VALUES (45, 'sendType', '3', '配送方式', 'shopConfig', NULL, '2021-06-24', 0);
INSERT INTO `ys_sys_config` VALUES (64, 'shopName', '45466546', '商家的店名', 'shopConfig', NULL, '2021-06-24', 0);
INSERT INTO `ys_sys_config` VALUES (65, 'address', '鹿鸣湖壹号南门984949', '商家店铺地址', 'shopConfig', NULL, '2021-06-24', 0);
INSERT INTO `ys_sys_config` VALUES (68, 'tel', '0859-1234567', '商家电话', 'shopConfig', NULL, '2021-06-24', 0);
INSERT INTO `ys_sys_config` VALUES (69, 'startSendTime', '08:30:00', '开始配送时间', 'shopConfig', NULL, '2021-06-24', 0);
INSERT INTO `ys_sys_config` VALUES (70, 'sendMoney', '0', '配送费', 'shopConfig', NULL, '2021-06-24', 0);
INSERT INTO `ys_sys_config` VALUES (75, 'endSendTime', '18:30:00', '结束配送时间', 'shopConfig', NULL, '2021-06-24', 0);

-- ----------------------------
-- Table structure for ys_tui
-- ----------------------------
DROP TABLE IF EXISTS `ys_tui`;
CREATE TABLE `ys_tui`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `order_id` int NOT NULL COMMENT '订单id',
  `tui_num` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '退款单号',
  `nickname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '昵称',
  `order_num` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '订单号',
  `money` decimal(10, 2) NOT NULL COMMENT '价钱',
  `message` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '信息',
  `because` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '原因',
  `ip` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` int NOT NULL DEFAULT 0 COMMENT '0退款中1已退款2驳回中',
  `aid` int NOT NULL DEFAULT 0,
  `wx_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` int NOT NULL,
  `ucid` int NOT NULL DEFAULT 3,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '退货管理表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of ys_tui
-- ----------------------------

-- ----------------------------
-- Table structure for ys_user
-- ----------------------------
DROP TABLE IF EXISTS `ys_user`;
CREATE TABLE `ys_user`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `nickname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '昵称',
  `unionid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `openid_gzh` varchar(70) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '公众号openid',
  `openid_app` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `openid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '小程序openid',
  `money` double(11, 2) NOT NULL DEFAULT 0.00 COMMENT '余额',
  `sign_time` bigint NULL DEFAULT NULL COMMENT '上次签到时间',
  `sign_day` bigint NULL DEFAULT NULL COMMENT '连续签到天数',
  `level_id` bigint NULL DEFAULT 1 COMMENT '用户等级',
  `headpic` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '头像',
  `mobile` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '电话',
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '用户真实姓名',
  `invite_code` int NULL DEFAULT NULL COMMENT '邀请码',
  `invite_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '邀请链接',
  `points` int NOT NULL DEFAULT 0,
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  `is_del` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 35 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of ys_user
-- ----------------------------
INSERT INTO `ys_user` VALUES (34, '小金木', NULL, NULL, NULL, 'ozf8D5QS4HAXx_FKNfpsSifYntzo', 0.00, NULL, NULL, 1, 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLpXnA0m6AATibJlz4Th8EFwvic1JUtashd3pf4UbAwQfYRSYRHficmicQ3Vt7fkMoJB67h5EepQ1gU6w/132', NULL, NULL, NULL, NULL, 0, '2021-06-25 11:42:27', '2021-08-16 09:39:09', 0);

-- ----------------------------
-- Table structure for ys_user_address
-- ----------------------------
DROP TABLE IF EXISTS `ys_user_address`;
CREATE TABLE `ys_user_address`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '收获人姓名',
  `mobile` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '手机号',
  `province` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `city` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `detail` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '详细地址',
  `user_id` int NOT NULL,
  `is_default` tinyint(1) NOT NULL,
  `created_at` int NULL DEFAULT NULL,
  `updated_at` int NULL DEFAULT NULL,
  `ucid` int NOT NULL DEFAULT 3,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户地址表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of ys_user_address
-- ----------------------------

-- ----------------------------
-- Table structure for ys_user_coupon
-- ----------------------------
DROP TABLE IF EXISTS `ys_user_coupon`;
CREATE TABLE `ys_user_coupon`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL COMMENT '用户ID',
  `coupon_id` int NOT NULL COMMENT '优惠券ID',
  `full` decimal(10, 2) NULL DEFAULT NULL COMMENT '满多少',
  `reduce` decimal(10, 2) NULL DEFAULT NULL COMMENT '减多少',
  `end_time` int NOT NULL COMMENT '有效时间',
  `status` int NULL DEFAULT 0 COMMENT '使用状态(0未使用1已使用2已完成3已过期',
  `create_time` datetime NOT NULL COMMENT '领取时间',
  `update_time` datetime NULL DEFAULT NULL,
  `is_del` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户优惠券表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of ys_user_coupon
-- ----------------------------

-- ----------------------------
-- Table structure for ys_user_level
-- ----------------------------
DROP TABLE IF EXISTS `ys_user_level`;
CREATE TABLE `ys_user_level`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name_l` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `discount` float(6, 2) NULL DEFAULT NULL COMMENT '折扣',
  `sort` int NOT NULL COMMENT '排序',
  `created_at` int NULL DEFAULT NULL COMMENT '创建时间',
  `updated_at` int NULL DEFAULT NULL COMMENT '修改时间',
  `deleted_at` int NULL DEFAULT NULL COMMENT '软删除',
  `ucid` int NOT NULL DEFAULT 3 COMMENT 'ucid',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of ys_user_level
-- ----------------------------

-- ----------------------------
-- Table structure for ys_video
-- ----------------------------
DROP TABLE IF EXISTS `ys_video`;
CREATE TABLE `ys_video`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '图片路径',
  `from` tinyint NOT NULL DEFAULT 1 COMMENT '1 来自本地，2 来自公网',
  `use_name` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `is_visible` int NOT NULL DEFAULT 1 COMMENT '是否能显示1能0不能',
  `description` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '描述字段',
  `delete_time` int NULL DEFAULT NULL,
  `update_time` int NULL DEFAULT NULL,
  `ucid` int NOT NULL DEFAULT 3,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '视频表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of ys_video
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
