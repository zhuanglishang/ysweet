# 鸽了鸽了，不想搞了，仅供大家娱乐

## 基于 RuoYi-Vue-Plus 和如花点餐一个很旧的版本的uniapp二开得到的点餐系统
- 如花点餐链接https://gitee.com/qianyu_wyc/ruhua_mail
- RuoYi-Vue-Plus链接https://gitee.com/JavaLionLi/RuoYi-Vue-Plus
- 本项目是管理系统的服务器端代码
- 本项目配套管理系统前端vue项目 https://gitee.com/dovi/ysweet-admin-ui
- 本项目配套微信小程序uniapp项目 https://gitee.com/dovi/ysweet-uniapp

## 模块介绍
在若依的基础上分出了两个模块
- admin后台管理系统服务器启动模块(RuoYiApplication.java启动)
- shop-点餐系统的业务代码都在里面(不能启动)
- app-一个可以启动的模块，里面主要是处理微信小程序客户端的请求，删减了admin的功能代码得到的(RuoYiApplication.java启动)

## 数据库为mysql8.0的ide需要有lombok插件,log4j控制台打印我配置了彩色打印，jvm参数添加-Dlog4j.skipJansi=false开启使用

## 演示图例

<table border="1" cellpadding="1" cellspacing="1" style="width:500px">
	<tbody>
		<tr>
                        <td><img src="https://xjm-picture.oss-cn-beijing.aliyuncs.com/view/FHLBT5%25GP_U%25MLO%24_B%258MSB.png" width="750" /></td>
			<td><img src="https://xjm-picture.oss-cn-beijing.aliyuncs.com/view/PY%5DWY%24PVJ4C%6025)C%254ZB0HM.png" width="750" /></td>
			<td><img src="https://xjm-picture.oss-cn-beijing.aliyuncs.com/view/(%24%7B6EQZY4Q7UO4M8)%25AJW%60L.png" width="750" /></td>
			<td><img src="https://xjm-picture.oss-cn-beijing.aliyuncs.com/view/01JTF%7DHZ_(90BRAFF~%25Z%7B%5B5.png" width="750" /></td>
		</tr>
                <tr>
			<td><img src="https://xjm-picture.oss-cn-beijing.aliyuncs.com/view/1Y%24Q8%7DOYJIW%5D%5DUB2KK6(JCP.png" width="750" /></td>
			<td><img src="https://xjm-picture.oss-cn-beijing.aliyuncs.com/view/BCMXV((2%25OOT%2456Y%7D4%24SGGG.png" width="750" /></td>
                        <td><img src="https://xjm-picture.oss-cn-beijing.aliyuncs.com/view/4.png" width="750" /></td>
                        <td><img src="https://xjm-picture.oss-cn-beijing.aliyuncs.com/view/2.png" width="750" /></td>
		</tr>
                <tr>
			<td><img src="https://xjm-picture.oss-cn-beijing.aliyuncs.com/view/3.png" width="750" /></td>
			<td><img src="https://xjm-picture.oss-cn-beijing.aliyuncs.com/view/1.png" width="750" /></td>
                        <td><img src="https://xjm-picture.oss-cn-beijing.aliyuncs.com/view/5.png" width="750" /></td>
                        <td><img src="https://xjm-picture.oss-cn-beijing.aliyuncs.com/view/6.png" width="750" /></td>
		</tr>
                <tr>
			<td><img src="https://xjm-picture.oss-cn-beijing.aliyuncs.com/view/7.png" width="750" /></td>
			<td><img src="https://xjm-picture.oss-cn-beijing.aliyuncs.com/view/8.png" width="750" /></td>
		</tr>
	</tbody>
</table>

