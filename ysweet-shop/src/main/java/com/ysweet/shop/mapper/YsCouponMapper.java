package com.ysweet.shop.mapper;

import com.ysweet.shop.domain.YsCoupon;
import com.ysweet.common.core.page.BaseMapperPlus;

/**
 * 优惠券Mapper接口
 *
 * @author 小金木
 * @date 2021-06-17
 */
public interface YsCouponMapper extends BaseMapperPlus<YsCoupon> {

}
