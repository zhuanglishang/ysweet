package com.ysweet.shop.mapper;

import com.ysweet.common.core.page.BaseMapperPlus;
import com.ysweet.shop.domain.YsGoods;

/**
 * 商品Mapper接口
 *
 * @author 小金木
 * @date 2021-06-17
 */
public interface YsGoodsMapper extends BaseMapperPlus<YsGoods> {

}
