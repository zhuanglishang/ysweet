package com.ysweet.shop.mapper;

import com.ysweet.shop.domain.YsImage;
import com.ysweet.common.core.page.BaseMapperPlus;

/**
 * 图片总Mapper接口
 *
 * @author 小金木
 * @date 2021-06-17
 */
public interface YsImageMapper extends BaseMapperPlus<YsImage> {

}
