package com.ysweet.shop.mapper;

import com.ysweet.common.core.page.BaseMapperPlus;
import com.ysweet.shop.domain.YsMaterial;

/**
 * 素材库Mapper接口
 *
 * @author 小金木
 * @date 2021-06-18
 */
public interface YsMaterialMapper extends BaseMapperPlus<YsMaterial> {

}
