package com.ysweet.system.mapper;

import com.ysweet.system.domain.YsMaterialGroup;
import com.ysweet.common.core.page.BaseMapperPlus;

/**
 * 素材分组Mapper接口
 *
 * @author 小金木
 * @date 2021-06-18
 */
public interface YsMaterialGroupMapper extends BaseMapperPlus<YsMaterialGroup> {

}
