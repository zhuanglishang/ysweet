package com.ysweet.shop.mapper;

import com.ysweet.shop.domain.YsRate;
import com.ysweet.common.core.page.BaseMapperPlus;

/**
 * 评价Mapper接口
 *
 * @author 小金木
 * @date 2021-06-17
 */
public interface YsRateMapper extends BaseMapperPlus<YsRate> {

}
