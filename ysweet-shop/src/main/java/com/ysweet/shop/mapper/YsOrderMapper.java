package com.ysweet.shop.mapper;

import com.ysweet.shop.domain.YsOrder;
import com.ysweet.common.core.page.BaseMapperPlus;

/**
 * 订单Mapper接口
 *
 * @author 小金木
 * @date 2021-06-17
 */
public interface YsOrderMapper extends BaseMapperPlus<YsOrder> {

}
