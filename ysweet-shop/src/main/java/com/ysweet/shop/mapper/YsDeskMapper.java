package com.ysweet.shop.mapper;

import com.ysweet.shop.domain.YsDesk;
import com.ysweet.common.core.page.BaseMapperPlus;

/**
 * 餐桌管理Mapper接口
 *
 * @author 小金木
 * @date 2021年8月20日10:39:09
 */
public interface YsDeskMapper extends BaseMapperPlus<YsDesk> {

}
