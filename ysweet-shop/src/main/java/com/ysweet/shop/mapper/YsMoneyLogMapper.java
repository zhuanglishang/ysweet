package com.ysweet.shop.mapper;

import com.ysweet.shop.domain.YsMoneyLog;
import com.ysweet.common.core.page.BaseMapperPlus;

/**
 * 订单日志Mapper接口
 *
 * @author 小金木
 * @date 2021-06-17
 */
public interface YsMoneyLogMapper extends BaseMapperPlus<YsMoneyLog> {

}
