package com.ysweet.shop.mapper;

import com.ysweet.shop.domain.YsCategory;
import com.ysweet.common.core.page.BaseMapperPlus;

/**
 * 商品分类Mapper接口
 *
 * @author 小金木
 * @date 2021-06-17
 */
public interface YsCategoryMapper extends BaseMapperPlus<YsCategory> {

}
