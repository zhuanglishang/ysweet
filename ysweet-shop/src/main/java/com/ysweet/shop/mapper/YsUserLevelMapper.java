package com.ysweet.shop.mapper;

import com.ysweet.shop.domain.YsUserLevel;
import com.ysweet.common.core.page.BaseMapperPlus;

/**
 * 【请填写功能名称】Mapper接口
 *
 * @author 小金木
 * @date 2021-06-17
 */
public interface YsUserLevelMapper extends BaseMapperPlus<YsUserLevel> {

}
