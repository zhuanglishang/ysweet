package com.ysweet.shop.mapper;

import com.ysweet.shop.domain.YsFavorites;
import com.ysweet.common.core.page.BaseMapperPlus;

/**
 * 收藏Mapper接口
 *
 * @author 小金木
 * @date 2021-06-17
 */
public interface YsFavoritesMapper extends BaseMapperPlus<YsFavorites> {

}
