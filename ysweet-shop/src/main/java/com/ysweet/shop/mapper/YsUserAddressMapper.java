package com.ysweet.shop.mapper;

import com.ysweet.shop.domain.YsUserAddress;
import com.ysweet.common.core.page.BaseMapperPlus;

/**
 * 用户地址Mapper接口
 *
 * @author 小金木
 * @date 2021-06-17
 */
public interface YsUserAddressMapper extends BaseMapperPlus<YsUserAddress> {

}
