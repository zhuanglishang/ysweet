package com.ysweet.shop.mapper;

import com.ysweet.shop.domain.YsTui;
import com.ysweet.common.core.page.BaseMapperPlus;

/**
 * 退货管理Mapper接口
 *
 * @author 小金木
 * @date 2021-06-17
 */
public interface YsTuiMapper extends BaseMapperPlus<YsTui> {

}
