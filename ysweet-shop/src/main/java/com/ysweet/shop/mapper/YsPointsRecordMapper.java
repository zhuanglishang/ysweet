package com.ysweet.shop.mapper;

import com.ysweet.shop.domain.YsPointsRecord;
import com.ysweet.common.core.page.BaseMapperPlus;

/**
 * 积分记录Mapper接口
 *
 * @author 小金木
 * @date 2021-06-17
 */
public interface YsPointsRecordMapper extends BaseMapperPlus<YsPointsRecord> {

}
