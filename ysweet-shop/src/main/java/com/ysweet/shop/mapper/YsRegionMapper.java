package com.ysweet.shop.mapper;

import com.ysweet.shop.domain.YsRegion;
import com.ysweet.common.core.page.BaseMapperPlus;

/**
 * 地区总Mapper接口
 *
 * @author 小金木
 * @date 2021-06-17
 */
public interface YsRegionMapper extends BaseMapperPlus<YsRegion> {

}
