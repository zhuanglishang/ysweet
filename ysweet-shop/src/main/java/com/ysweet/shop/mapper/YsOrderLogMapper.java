package com.ysweet.shop.mapper;

import com.ysweet.shop.domain.YsOrderLog;
import com.ysweet.common.core.page.BaseMapperPlus;

/**
 * 订单日志Mapper接口
 *
 * @author 小金木
 * @date 2021-06-17
 */
public interface YsOrderLogMapper extends BaseMapperPlus<YsOrderLog> {

}
