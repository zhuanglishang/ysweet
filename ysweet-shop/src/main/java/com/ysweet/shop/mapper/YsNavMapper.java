package com.ysweet.shop.mapper;

import com.ysweet.shop.domain.YsNav;
import com.ysweet.common.core.page.BaseMapperPlus;

/**
 * 导航栏Mapper接口
 *
 * @author 小金木
 * @date 2021-06-17
 */
public interface YsNavMapper extends BaseMapperPlus<YsNav> {

}
