package com.ysweet.shop.mapper;

import com.ysweet.shop.domain.YsFxGoods;
import com.ysweet.common.core.page.BaseMapperPlus;

/**
 * 分销商品Mapper接口
 *
 * @author 小金木
 * @date 2021-06-17
 */
public interface YsFxGoodsMapper extends BaseMapperPlus<YsFxGoods> {

}
