package com.ysweet.shop.mapper;

import com.ysweet.shop.domain.YsGoodsSku;
import com.ysweet.common.core.page.BaseMapperPlus;

/**
 * 商品skui规格价格库存信息Mapper接口
 *
 * @author 小金木
 * @date 2021-06-17
 */
public interface YsGoodsSkuMapper extends BaseMapperPlus<YsGoodsSku> {

}
