package com.ysweet.shop.mapper;

import com.ysweet.shop.domain.YsArticle;
import com.ysweet.common.core.page.BaseMapperPlus;

/**
 * CMS文章Mapper接口
 *
 * @author 小金木
 * @date 2021-06-17
 */
public interface YsArticleMapper extends BaseMapperPlus<YsArticle> {

}
