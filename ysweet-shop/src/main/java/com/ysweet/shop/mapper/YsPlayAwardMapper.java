package com.ysweet.shop.mapper;

import com.ysweet.shop.domain.YsPlayAward;
import com.ysweet.common.core.page.BaseMapperPlus;

/**
 * 玩法奖励Mapper接口
 *
 * @author 小金木
 * @date 2021-06-17
 */
public interface YsPlayAwardMapper extends BaseMapperPlus<YsPlayAward> {

}
