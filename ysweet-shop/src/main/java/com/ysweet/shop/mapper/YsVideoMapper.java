package com.ysweet.shop.mapper;

import com.ysweet.shop.domain.YsVideo;
import com.ysweet.common.core.page.BaseMapperPlus;

/**
 * 视频Mapper接口
 *
 * @author 小金木
 * @date 2021-06-17
 */
public interface YsVideoMapper extends BaseMapperPlus<YsVideo> {

}
