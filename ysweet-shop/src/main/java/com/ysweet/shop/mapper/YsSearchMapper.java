package com.ysweet.shop.mapper;

import com.ysweet.shop.domain.YsSearch;
import com.ysweet.common.core.page.BaseMapperPlus;

/**
 * 热搜Mapper接口
 *
 * @author 小金木
 * @date 2021-06-17
 */
public interface YsSearchMapper extends BaseMapperPlus<YsSearch> {

}
