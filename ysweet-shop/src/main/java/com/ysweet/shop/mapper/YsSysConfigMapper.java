package com.ysweet.shop.mapper;

import com.ysweet.shop.domain.YsSysConfig;
import com.ysweet.common.core.page.BaseMapperPlus;

/**
 * 系统配置Mapper接口
 *
 * @author 小金木
 * @date 2021-06-17
 */
public interface YsSysConfigMapper extends BaseMapperPlus<YsSysConfig> {

}
