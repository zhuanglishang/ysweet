package com.ysweet.shop.mapper;

import com.ysweet.shop.domain.YsOrderGoods;
import com.ysweet.common.core.page.BaseMapperPlus;

/**
 * 订单商品详情Mapper接口
 *
 * @author 小金木
 * @date 2021-06-17
 */
public interface YsOrderGoodsMapper extends BaseMapperPlus<YsOrderGoods> {

}
