package com.ysweet.shop.mapper;

import com.ysweet.shop.domain.YsBanner;
import com.ysweet.common.core.page.BaseMapperPlus;

/**
 * banner管理Mapper接口
 *
 * @author 小金木
 * @date 2021-06-17
 */
public interface YsBannerMapper extends BaseMapperPlus<YsBanner> {

}
