package com.ysweet.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;

/**
 * 图片总对象 ys_image
 *
 * @author 小金木
 * @date 2021-06-17
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("ys_image")
public class YsImage implements Serializable {

    private static final long serialVersionUID=1L;



    @TableId(value = "id")
    private Long id;

    /** 图片路径 */
    private String url;

    /** 1 来自本地，2 来自公网 */
    private Long from;


    private String useName;

    /** 图片分类 */
    private Long categoryId;

    /** 是否能显示1能0不能 */
    private Long isVisible;


    private Long deletedAt;


    private Long createdAt;


    private Long updatedAt;

    /** ucid */
    private Long ucid;

}
