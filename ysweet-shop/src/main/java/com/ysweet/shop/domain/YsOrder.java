package com.ysweet.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.ysweet.common.core.domain.BaseDomain;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * 订单对象 ys_order
 *
 * @author 小金木
 * @date 2021-06-17
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("ys_order")
public class YsOrder extends BaseDomain {

	private static final long serialVersionUID = 1L;

	/**
	 *主键
	 */
	@TableId(value = "order_id")
	private Long orderId;

	/**
	 * 订单编号
	 */
	private String orderNum;

	/**
	 * 下单用户id
	 */
	private Long userId;

	/**
	 * 0未完成 1已完成 2已评价 -1退款中 -2已退款-3关闭订单
	 */
	private Integer state;

	/**
	 * 支付状态 0 1
	 */
	private Integer paymentState;

	/**
	 * 优惠券ID
	 */
	private Long couponId;

	/**
	 * 支付来源
	 */
	private String paymentType;

	/**
	 * 商品总价
	 */
	private BigDecimal goodsMoney;

	/**
	 * 优惠券价格
	 */
	private BigDecimal couponMoney;

	/**
	 * 订单总价
	 */
	private BigDecimal orderMoney;

	/**
	 * 下单用户ip
	 */
	private String userIp;

	/**
	 * 第一个物品的图片
	 */
	private String goodsPicture;

	/**
	 * 第一个物品的名字
	 */
	private String goodsName;

	/**
	 * 备注
	 */
	private String message;


	/**
	 * 支付时间
	 */
	private Long payTime;


	/**
	 * 支付方式1微信支付2余额支付3暂不支付
	 */
	private String payCate;

	/**
	 * 几号桌
	 */
	private String tableNum;
	/***
	 * 订单商品的数据
	 */
	@TableField(exist = false)
	private String goods;
}
