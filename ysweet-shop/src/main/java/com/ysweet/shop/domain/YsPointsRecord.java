package com.ysweet.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 积分记录对象 ys_points_record
 *
 * @author 小金木
 * @date 2021-06-17
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("ys_points_record")
public class YsPointsRecord implements Serializable {

    private static final long serialVersionUID=1L;


    /**  */
    @TableId(value = "id")
    private Long id;

    /**  */
    private String uid;

    /**  */
    private String credittype;

    /**  */
    private BigDecimal num;

    /**  */
    private String operator;

    /**  */
    private String module;

    /**  */
    private String clerkId;

    /**  */
    private String storeId;

    /**  */
    private String clerkType;

    /**  */
    private Long createdAt;

    /**  */
    private String remark;

    /**  */
    private Long realUniacid;

    /**  */
    private Long updatedAt;

    /**  */
    private Long ucid;

}
