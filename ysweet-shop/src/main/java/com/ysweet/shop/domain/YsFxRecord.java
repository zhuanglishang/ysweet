package com.ysweet.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 分销记录对象 ys_fx_record
 *
 * @author 小金木
 * @date 2021-06-17
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("ys_fx_record")
public class YsFxRecord implements Serializable {

    private static final long serialVersionUID=1L;



    @TableId(value = "id")
    private Long id;


    private Long userId;


    private Long orderId;


    private Long goodsId;

    /** 分销提成 */
    private BigDecimal money;


    private Long createdAt;


    private Long updatedAt;

}
