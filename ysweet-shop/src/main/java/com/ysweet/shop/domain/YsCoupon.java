package com.ysweet.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.ysweet.common.core.domain.BaseDomain;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;
import java.math.BigDecimal;

/**
 * 优惠券对象 ys_coupon
 *
 * @author 小金木
 * @date 2021-06-17
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("ys_coupon")
public class YsCoupon extends BaseDomain {

	private static final long serialVersionUID = 1L;

	@TableId(value = "id")
	private Long id;

	/**
	 * 类型1:店铺优惠券
	 */
	private Integer couponType;

	/**
	 * 优惠券名称
	 */
	private String couponName;

	/**
	 * 状态1:使用1次，2使用无数次
	 */
	private Integer couponStatus;

	/**
	 * vip特权券是否能领取,0不可领取,1能领取
	 */
	private Long isShow;

	/**
	 * 库存null为无限张
	 */
	private Long stock;

	/**
	 * 满多少，0为无门槛
	 */
	private BigDecimal couponFull;

	/**
	 * 减多少
	 */
	private BigDecimal reduce;

	/**
	 * 开始时间
	 */
	private Date startTime;

	/**
	 * 结束时间
	 */
	private Date endTime;

	/**
	 * 使用时间
	 */
	private Long couponDay;

	/**
	 * 是否拥有这张优惠券 1已拥有 0未拥有
	 */
	@TableField(exist = false)
	private Integer having=0;

}
