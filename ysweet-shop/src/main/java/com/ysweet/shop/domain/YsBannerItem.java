package com.ysweet.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;

/**
 * banner子项对象 ys_banner_item
 *
 * @author 小金木
 * @date 2021-06-17
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("ys_banner_item")
public class YsBannerItem implements Serializable {

    private static final long serialVersionUID=1L;



    @TableId(value = "id")
    private Long id;

    /** 外键，关联image表 */
    private Long imgId;

    /** 执行关键字，根据不同的type含义不同 */
    private String keyWord;

    /** 跳转类型 */
    private String type;


    private Long jumpId;


    private Long deletedAt;

    /** 外键，关联banner表 */
    private Long bannerId;

    /** 是否显示  1 显示 0 不显示 */
    private Long isVisible;


    private Long sort;


    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateTime;

    /** ucid */
    private Long ucid;


    @TableField(fill = FieldFill.INSERT)
    private Long createTime;

}
