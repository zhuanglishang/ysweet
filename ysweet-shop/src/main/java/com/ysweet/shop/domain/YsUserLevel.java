package com.ysweet.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 【请填写功能名称】对象 ys_user_level
 *
 * @author 小金木
 * @date 2021-06-17
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("ys_user_level")
public class YsUserLevel implements Serializable {

    private static final long serialVersionUID=1L;


    /** ID */
    @TableId(value = "id")
    private Long id;

    /** 名称 */
    private String nameL;

    /** 折扣 */
    private BigDecimal discount;

    /** 排序 */
    private Long sort;

    /** 创建时间 */
    private Long createdAt;

    /** 修改时间 */
    private Long updatedAt;

    /** 软删除 */
    private Long deletedAt;

    /** ucid */
    private Long ucid;

}
