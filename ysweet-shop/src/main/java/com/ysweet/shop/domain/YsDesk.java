package com.ysweet.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.ysweet.common.core.domain.BaseDomain;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 【请填写功能名称】对象 ys_zhgl
 *
 * @author 小金木
 * @date 2021-06-17
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("ys_desk")
public class YsDesk extends BaseDomain {

    private static final long serialVersionUID=1L;


    /**  */
    @TableId(value = "id")
    private Long id;

    /** 桌号 */
    private Integer deskNumber;

    /** 备注 */
    private String description;

    /** H5二维码 */
    private String h5Img;

    /** 小程序二维码 */
    private String xcxImg;

}
