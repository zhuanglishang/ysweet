package com.ysweet.shop.domain.dto;

import lombok.*;

/**
 * @ClassName WechatUserDTO
 * @Author
 * @Date 2020/6/4
 **/
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class WechatUserDto {

    private String openid;

    private String unionId;

    private String routineOpenid;

    private String nickname;

    private String headimgurl;

    private Integer sex;

    private String city;

    private String language;

    private String province;

    private String country;

}
