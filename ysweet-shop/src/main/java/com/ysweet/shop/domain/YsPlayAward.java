package com.ysweet.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;

/**
 * 玩法奖励对象 ys_play_award
 *
 * @author 小金木
 * @date 2021-06-17
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("ys_play_award")
public class YsPlayAward implements Serializable {

    private static final long serialVersionUID=1L;


    /**  */
    @TableId(value = "id")
    private Long id;

    /** 类型ID */
    private Long typeId;

    /**  */
    private String name;

    /** 概率 */
    private Long lv;

    /** 奖励内容 */
    private String award;

    /** 奖品类型(0为积分1为商品) */
    private Long awardType;

    /**  */
    private Long stock;

    /**  */
    private Long imgId;

    /**  */
    private Long ucid;

}
