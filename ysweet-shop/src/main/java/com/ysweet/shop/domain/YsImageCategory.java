package com.ysweet.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;

/**
 * 【请填写功能名称】对象 ys_image_category
 *
 * @author 小金木
 * @date 2021-06-17
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("ys_image_category")
public class YsImageCategory implements Serializable {

    private static final long serialVersionUID=1L;


    /**  */
    @TableId(value = "id")
    private Long id;

    /**  */
    private String categoryName;

    /**  */
    private Long pid;

    /**  */
    private Long level;

    /**  */
    private Long isVisible;

    /** ucid */
    private Long ucid;

}
