package com.ysweet.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.ysweet.common.core.domain.BaseDomain;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 素材分组对象 ys_material_group
 *
 * @author 小金木
 * @date 2021-06-18
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("ys_material_group")
public class YsMaterialGroup extends BaseDomain {

    @TableId(value = "id",type=IdType.INPUT)
    private String id;

    /** 创建者ID */
    private String createId;

    /** 分组名 */
    private String name;



}
