package com.ysweet.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;

/**
 * 分销商品对象 ys_fx_goods
 *
 * @author 小金木
 * @date 2021-06-17
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("ys_fx_goods")
public class YsFxGoods implements Serializable {

    private static final long serialVersionUID=1L;



    @TableId(value = "id")
    private Long id;


    private Long goodsId;

    /** 价格 */
    private Long price;


    private Long createdAt;


    private Long updatedAt;

}
