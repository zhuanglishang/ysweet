package com.ysweet.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import com.ysweet.common.core.domain.BaseDomain;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;


/**
 * 商品对象 ys_goods
 *
 * @author 小金木
 * @date 2021-07-08
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName(value="ys_goods",autoResultMap = true)
public class YsGoods extends BaseDomain {

	/**
	 * 商品id
	 */
	@TableId(value = "good_id")
	private Long goodId;

	/**
	 * 商品名称
	 */
	private String goodName;

	/**
	 * 商品分类id
	 */
	private Integer categoryId;

	/**
	 * 商品主图(商品列表展示图片)
	 */
	private String showImage;

	/**
	 * 商品关键词
	 */
	private String keywords;
	/**
	 * 单规格还是多规格
	 */
	private Integer specType;

	/**
	 * 商品简介，促销语
	 */
	private String description;

	/**
	 * 展示图片(详情页面滚动图片)
	 */
	@TableField(typeHandler = FastjsonTypeHandler.class)
	private List<String> detailImages;

	/**
	 * 是否热销商品
	 */
	private Integer isHot;

	/**
	 * 是否推荐
	 */
	private Integer isRecommend;

	/**
	 * 排序
	 */
	private Long sort;

	/**
	 * 是否新品
	 */
	private Integer isNew;

	/**
	 * 是否下架 1下架，0正常
	 */
	private Integer soldOut;

	@TableField(exist = false)
	private String ysGoodsSku;
}
