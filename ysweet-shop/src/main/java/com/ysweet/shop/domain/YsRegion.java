package com.ysweet.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;

/**
 * 地区总对象 ys_region
 *
 * @author 小金木
 * @date 2021-06-17
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("ys_region")
public class YsRegion implements Serializable {

    private static final long serialVersionUID=1L;


    /** ID */
    @TableId(value = "id")
    private Long id;

    /** 父id */
    private Long pid;

    /** 名称 */
    private String name;

    /** 全称 */
    private String mergerName;

    /** 层级 1 2 3  */
    private String level;

    /** ucid */
    private Long ucid;

}
