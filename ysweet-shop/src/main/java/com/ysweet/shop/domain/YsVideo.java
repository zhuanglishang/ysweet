package com.ysweet.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;

/**
 * 视频对象 ys_video
 *
 * @author 小金木
 * @date 2021-06-17
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("ys_video")
public class YsVideo implements Serializable {

    private static final long serialVersionUID=1L;


    /**  */
    @TableId(value = "id")
    private Long id;

    /** 图片路径 */
    private String url;

    /** 1 来自本地，2 来自公网 */
    private Long from;

    /**  */
    private String useName;

    /** 是否能显示1能0不能 */
    private Long isVisible;

    /** 描述字段 */
    private String description;

    /**  */
    private Long deleteTime;

    /**  */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateTime;

    /**  */
    private Long ucid;

}
