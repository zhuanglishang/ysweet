package com.ysweet.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;

/**
 * 评价对象 ys_rate
 *
 * @author 小金木
 * @date 2021-06-17
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("ys_rate")
public class YsRate implements Serializable {

    private static final long serialVersionUID=1L;



    @TableId(value = "id")
    private Long id;


    private Long orderId;


    private Long goodsId;


    private Long rate;


    private String content;


    private Long userId;

    /** 图片集 */
    private String imgs;

    /** 头像 */
    private String headpic;


    private String nickname;

    /** 回复内容 */
    private String replyContent;

    /** 回复时间 */
    private Long replyTime;

    /** 管理员id */
    private Long aid;


    private Long createdAt;

    /** 视频地址 */
    private String video;


    private Long updatedAt;

    /** ucid */
    private Long ucid;

}
