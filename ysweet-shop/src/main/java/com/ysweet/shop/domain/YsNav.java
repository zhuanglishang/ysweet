package com.ysweet.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;

/**
 * 导航栏对象 ys_nav
 *
 * @author 小金木
 * @date 2021-06-17
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("ys_nav")
public class YsNav implements Serializable {

    private static final long serialVersionUID=1L;


    /**  */
    @TableId(value = "id")
    private Long id;

    /** 名称 */
    private String navName;

    /** 图片ID */
    private String imgId;

    /** 转跳路径 */
    private String url;

    /** 转跳名字 */
    private String urlName;

    /** 转跳分类ID */
    private Long categoryId;

    /** 	是否显示 1 显示 0 不显示	 */
    private Long isVisible;

    /**  */
    private Long sort;

    /** 其他 */
    private String other;

    /** ucid */
    private Long ucid;

    /**  */
    private Long createdAt;

    /**  */
    private Long deletedAt;

    /**  */
    private Long updatedAt;

}
