package com.ysweet.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;

/**
 * 【请填写功能名称】对象 ys_sys_backup
 *
 * @author 小金木
 * @date 2021-06-17
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("ys_sys_backup")
public class YsSysBackup implements Serializable {

    private static final long serialVersionUID=1L;



    @TableId(value = "id")
    private Long id;

    /** 名称 */
    private String name;

    /** 大小 */
    private String size;

    /** 路径 */
    private String url;


    @TableField(fill = FieldFill.INSERT)
    private Long createTime;


    private Long ucid;

}
