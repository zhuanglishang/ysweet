package com.ysweet.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.ysweet.common.core.domain.BaseDomain;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 【请填写功能名称】对象 ys_user
 *
 * @author 小金木
 * @date 2021-06-17
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@TableName("ys_user")
public class YsUser extends BaseDomain {

	@TableId(value = "id")
	private Long id;

	/**
	 * 昵称
	 */
	private String nickname;


	private String unionid;

	/**
	 * 公众号openid
	 */
	private String openidGzh;


	private String openidApp;

	/**
	 * 小程序openid
	 */
	private String openid;

	/**
	 * 余额
	 */
	private Double money;

	/**
	 * 上次签到时间
	 */
	private Long signTime;

	/**
	 * 连续签到天数
	 */
	private Long signDay;

	/**
	 * 用户等级
	 */
	private Long levelId;

	/**
	 * 头像
	 */
	private String headpic;

	/**
	 * 电话
	 */
	private String mobile;

	/**
	 * 用户真实姓名
	 */
	private String userName;

	/**
	 * 邀请码
	 */
	private Long inviteCode;

	/**
	 * 邀请链接
	 */
	private String inviteUrl;

	private Long points;

	@TableField(exist = false)
	private Integer couponSize;
}
