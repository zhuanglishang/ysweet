package com.ysweet.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;

/**
 * 商品skui规格价格库存信息对象 ys_goods_sku
 *
 * @author 小金木
 * @date 2021-06-17
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("ys_goods_sku")
public class YsGoodsSku implements Serializable {

    private static final long serialVersionUID=1L;


    /** 表序号 */
    @TableId(value = "sku_id")
    private Long skuId;

    /** 商品编号 */
    private Long goodsId;


    private String json;

}
