package com.ysweet.shop.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ysweet.common.core.domain.BaseDomain;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * 用户优惠券对象 ys_user_coupon
 *
 * @author 小金木
 * @date 2021-06-17
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("ys_user_coupon")
public class YsUserCoupon extends BaseDomain {

    @TableId(value = "id")
    private Long id;

    /** 用户ID */
    private Long userId;

    /** 优惠券ID */
    private Long couponId;

    /** 满多少 */
    private BigDecimal full;

    /** 减多少 */
    private BigDecimal reduce;

    /** 有效时间 */
	private Long endTime;

    /** 使用状态(0未使用1已使用2已完成3已过期 */
    private Long status;

}
