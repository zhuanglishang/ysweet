package com.ysweet.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.ysweet.common.core.domain.BaseDomain;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * banner管理对象 ys_banner
 *
 * @author 小金木
 * @date 2021-06-21
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("ys_banner")
public class YsBanner extends BaseDomain {


	@TableId(value = "id")
	private Long id;

	/** Banner名称，通常作为标识 */
	private String name;

	/** Banner描述 */
	private String description;

	/** 广告的图片,不能为空 */
	private String imageUrl;

	/** 跳转的类型 */
	private Integer redirectType;

	/** 关键字,不同跳转有不同作用,可能是商品的id,也可能是文章的链接等等 */
	private String keyStr;

	/** 广告分类 */
	private Integer groupId;

	/** 是否显示广告,默认是1显示 */
	private Integer status;

}
