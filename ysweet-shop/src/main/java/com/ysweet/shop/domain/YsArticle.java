package com.ysweet.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.ysweet.common.core.domain.BaseDomain;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * CMS文章对象 ys_article
 *
 * @author 小金木
 * @date 2021-06-17
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("ys_article")
public class YsArticle extends BaseDomain {

    private static final long serialVersionUID=1L;


    /** 文章编号 */
    @TableId(value = "id")
    private Long id;

    /** 0活动公告，1独立文章，2公告 */
    private Integer type;

    /** 文章标题 */
    private String title;

    /** 文章摘要 */
    private String summary;

    /** 文章正文 */
    private String content;

    /** 文章标题图片 */
    private String image;

    /** 是否显示 1 显示 0 不显示 */
    private Integer isHidden;

    /** 发布者用户名  */
    private String author;

}
