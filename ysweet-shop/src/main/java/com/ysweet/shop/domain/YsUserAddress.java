package com.ysweet.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;

/**
 * 用户地址对象 ys_user_address
 *
 * @author 小金木
 * @date 2021-06-17
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("ys_user_address")
public class YsUserAddress implements Serializable {

    private static final long serialVersionUID=1L;



    @TableId(value = "id")
    private Long id;

    /** 收获人姓名 */
    private String name;

    /** 手机号 */
    private String mobile;


    private String province;


    private String city;

    /** 详细地址 */
    private String detail;


    private Long userId;


    private Integer isDefault;


    private Long createdAt;


    private Long updatedAt;


    private Long ucid;

}
