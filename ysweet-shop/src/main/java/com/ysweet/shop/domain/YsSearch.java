package com.ysweet.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;

/**
 * 热搜对象 ys_search
 *
 * @author 小金木
 * @date 2021-06-17
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("ys_search")
public class YsSearch implements Serializable {

    private static final long serialVersionUID=1L;



    @TableId(value = "id")
    private Long id;

    /** 热词 */
    private String name;


    private Long num;


    private Long createdAt;


    private Long updatedAt;

}
