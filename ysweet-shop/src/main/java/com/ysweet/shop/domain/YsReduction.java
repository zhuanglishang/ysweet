package com.ysweet.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 【请填写功能名称】对象 ys_reduction
 *
 * @author 小金木
 * @date 2021-06-17
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("ys_reduction")
public class YsReduction implements Serializable {

    private static final long serialVersionUID=1L;



    @TableId(value = "id")
    private Long id;

    /** 名称 */
    private String name;

    /** 是否开启 1开启，2关闭 */
    private Long statu;

    /** 满多少 */
    private BigDecimal full;

    /** 减多少 */
    private BigDecimal reduce;

    /** 开始时间 */
    private Long startTime;

    /** 结束时间 */
    private Long endTime;

    /** 创建 时间 */
    private Long createdAt;

    /** 修改时间 */
    private Long updatedAt;

    /** 删除时间 */
    private Long deletedAt;


    private Long ucid;

}
