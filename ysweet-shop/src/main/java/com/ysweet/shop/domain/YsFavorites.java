package com.ysweet.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 收藏对象 ys_favorites
 *
 * @author 小金木
 * @date 2021-06-17
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("ys_favorites")
public class YsFavorites implements Serializable {

    private static final long serialVersionUID=1L;


    /** 记录ID */
    @TableId(value = "id")
    private Long id;

    /** 用户ID */
    private Long uid;

    /** 商品ID */
    private String favId;


    private Long imgId;

    /** 商品收藏时价格 */
    private BigDecimal price;

    /** 收藏时间 */
    @TableField(fill = FieldFill.INSERT)
    private Long createTime;

    /** ucid */
    private Long ucid;

}
