package com.ysweet.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统配置对象 ys_sys_config
 *
 * @author 小金木
 * @date 2021-06-17
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("ys_sys_config")
public class YsSysConfig implements Serializable {

	private static final long serialVersionUID = 1L;


	@TableId(value = "id")
	private Long id;

	private String keyStr;

	private String valueStr;

	private String description;

	private String type;

	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date updatedTime;

	@TableField(fill = FieldFill.INSERT)
	private Date createdTime;

	private Integer isDel;

}
