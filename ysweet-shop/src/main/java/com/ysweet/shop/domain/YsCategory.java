package com.ysweet.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.ysweet.common.core.domain.BaseDomain;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 商品分类对象 ys_category
 *
 * @author 小金木
 * @date 2021年7月6日17:15:40
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("ys_category")
public class YsCategory extends BaseDomain {

	@TableId(value = "category_id")
	private Long categoryId;

	/**
	 * 名称
	 */
	private String categoryName;

	/**
	 * 商品分类简称
	 */
	private String shortName;

	/**
	 * 是否显示  1 显示 0 不显示
	 */
	private Integer isVisible;

	private Integer sort;

	/**
	 * 商品分类图片
	 */
	private String categoryPic;


}
