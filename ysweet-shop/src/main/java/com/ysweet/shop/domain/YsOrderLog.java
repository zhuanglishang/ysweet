package com.ysweet.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;

/**
 * 订单日志对象 ys_order_log
 *
 * @author 小金木
 * @date 2021-06-17
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("ys_order_log")
public class YsOrderLog implements Serializable {

    private static final long serialVersionUID=1L;


    /**  */
    @TableId(value = "id")
    private Long id;

    /**  */
    private Long orderId;

    /**  */
    private String typeName;

    /**  */
    private String content;

    /**  */
    private String operator;

    /**  */
    private String ip;

    /** 微信退款id */
    private String wxRefund;

    /**  */
    private Long createdAt;

    /**  */
    private Long ucid;

    /**  */
    private Long updatedAt;

}
