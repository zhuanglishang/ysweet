package com.ysweet.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 退货管理对象 ys_tui
 *
 * @author 小金木
 * @date 2021-06-17
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("ys_tui")
public class YsTui implements Serializable {

    private static final long serialVersionUID=1L;



    @TableId(value = "id")
    private Long id;

    /** 订单id */
    private Long orderId;

    /** 退款单号 */
    private String tuiNum;

    /** 昵称 */
    private String nickname;

    /** 订单号 */
    private String orderNum;

    /** 价钱 */
    private BigDecimal money;

    /** 信息 */
    private String message;

    /** 原因 */
    private String because;


    private String ip;

    /** 0退款中1已退款2驳回中 */
    private Long status;


    private Long aid;


    private String wxId;


    private String remark;


    private Long createdAt;


    private Long ucid;

}
