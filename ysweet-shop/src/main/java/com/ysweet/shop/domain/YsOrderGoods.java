package com.ysweet.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.ysweet.common.core.domain.BaseDomain;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.math.BigDecimal;

/**
 * 订单商品详情对象 ys_order_goods
 *
 * @author 小金木
 * @date 2021-06-17
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("ys_order_goods")
public class YsOrderGoods extends BaseDomain {

    private static final long serialVersionUID=1L;


    /** 主键 */
    @TableId(value = "id")
    private Long id;

    /** 订单id */
    private Long orderId;

    /** 物品id */
    private Long goodsId;

    /** 物品名字 */
    private String goodsName;

    /** sku组合 */
    private String skuName;

    /** 价格 */
    private BigDecimal price;

    /** 个数 */
    private Integer number;

    /** 总价 */
    private BigDecimal totalPrice;

    /** 订单状态0未完成 1已完成 2已评价 -1退款中 -2已退款-3关闭订单	 */
    private Integer state;

    /** 图片 */
    private String pic;

    /** 用户id */
    private Long userId;

}
