package com.ysweet.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;

/**
 * 【请填写功能名称】对象 ys_group_rule
 *
 * @author 小金木
 * @date 2021-06-17
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("ys_group_rule")
public class YsGroupRule implements Serializable {

    private static final long serialVersionUID=1L;



    @TableId(value = "id")
    private Long id;


    private String name;


    private String title;


    private Long ucid;

}
