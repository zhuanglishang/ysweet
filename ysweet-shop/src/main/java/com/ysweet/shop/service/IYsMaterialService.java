package com.ysweet.shop.service;


import com.ysweet.common.core.page.IServicePlus;
import com.ysweet.common.core.page.TableDataInfo;
import com.ysweet.shop.domain.YsMaterial;

/**
 * 素材库Service接口
 *
 * @author 小金木
 * @date 2021年6月21日10:27:01
 */
public interface IYsMaterialService extends IServicePlus<YsMaterial> {
	TableDataInfo<YsMaterial> selectPageDictDataList(YsMaterial ysMaterial);
	Boolean deleteMaterial(String id);
}
