package com.ysweet.shop.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ysweet.common.core.page.TableDataInfo;
import com.ysweet.common.utils.PageUtils;
import com.ysweet.shop.domain.YsCoupon;
import com.ysweet.shop.mapper.YsCouponMapper;
import com.ysweet.shop.service.IYsCouponService;
import org.springframework.stereotype.Service;

/**
 *  优惠券管理
 * @author 小金木
 * @date 2021年6月25日11:17:10
 */
@Service
public class YsCouponService extends ServiceImpl<YsCouponMapper, YsCoupon> implements IYsCouponService {
	/***
	 * 根据查询条件返回优惠券列表
	 * @param ysCoupon
	 * @return
	 */
	@Override
	public TableDataInfo<YsCoupon> selectByPage(YsCoupon ysCoupon) {
		LambdaQueryWrapper<YsCoupon> lqw = new LambdaQueryWrapper<YsCoupon>();
			/*.eq(ObjectUtil.isNotEmpty(ysBanner.getGroupId())&&ysBanner.getGroupId()>0, YsBanner::getGroupId, ysBanner.getGroupId())
			.eq(ObjectUtil.isNotEmpty(ysBanner.getStatus()),YsBanner::getStatus, ysBanner.getStatus())
			.orderByDesc(YsBanner::getCreateTime);*/
		return PageUtils.buildDataInfo(page(PageUtils.buildPage(), lqw));
	}
}
