package com.ysweet.shop.service;

import com.ysweet.common.core.page.IServicePlus;
import com.ysweet.common.core.page.TableDataInfo;
import com.ysweet.shop.domain.YsCategory;

/**
 *  商品分类
 *
 * @author 小金木
 * @date 2021年7月6日17:24:35
 */
public interface IYsCategoryService extends IServicePlus<YsCategory> {
	/***
	 * 根据查询条件返回广告列表
	 * @param ysCategory
	 * @return
	 */
	TableDataInfo<YsCategory> selectByPage(YsCategory ysCategory);
}
