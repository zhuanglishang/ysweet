package com.ysweet.shop.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ysweet.common.core.page.TableDataInfo;
import com.ysweet.common.utils.PageUtils;
import com.ysweet.common.utils.file.FileUtils;
import com.ysweet.shop.domain.YsMaterial;
import com.ysweet.shop.mapper.YsMaterialMapper;
import com.ysweet.shop.service.IYsMaterialService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 素材库Service业务层处理
 *
 * @author 小金木
 * @date 2021-06-18
 */
@Service
public class YsMaterialServiceImpl extends ServiceImpl<YsMaterialMapper, YsMaterial> implements IYsMaterialService {

	@Override
	public TableDataInfo<YsMaterial> selectPageDictDataList(YsMaterial ysMaterial) {
		LambdaQueryWrapper<YsMaterial> lqw = new LambdaQueryWrapper<YsMaterial>()
			.eq(StrUtil.isNotBlank(ysMaterial.getGroupId()) && !ysMaterial.getGroupId().equals("-1"), YsMaterial::getGroupId, ysMaterial.getGroupId())
			.orderByDesc(YsMaterial::getCreateTime);
		return PageUtils.buildDataInfo(page(PageUtils.buildPage(), lqw));
	}

	@Override
	@Transactional
	public Boolean deleteMaterial(String id) {
		YsMaterial ysMaterial = getById(id);
		if (removeById(id)) {
			//阿里云oss删除图片资源
			return FileUtils.deleteOssFile(ysMaterial.getUrl());
		}
		return false;
	}
}
