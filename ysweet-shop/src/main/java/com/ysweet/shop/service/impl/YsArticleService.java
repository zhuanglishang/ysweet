package com.ysweet.shop.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ysweet.common.core.page.TableDataInfo;
import com.ysweet.common.utils.PageUtils;
import com.ysweet.shop.domain.YsArticle;
import com.ysweet.shop.mapper.YsArticleMapper;
import com.ysweet.shop.service.IYsArticleService;
import org.springframework.stereotype.Service;

/**
 *  文章管理
 *
 * @author 小金木
 * 2021年8月18日13:48:16
 */
@Service
public class YsArticleService extends ServiceImpl<YsArticleMapper, YsArticle> implements IYsArticleService {
	@Override
	public TableDataInfo<YsArticle> selectOfPage(YsArticle ysArticle) {
		if(ObjectUtil.isEmpty(ysArticle))
			ysArticle=new YsArticle();
		LambdaQueryWrapper<YsArticle> lambdaQueryWrapper=new LambdaQueryWrapper();
		lambdaQueryWrapper.eq(YsArticle::getIsHidden,0);
		lambdaQueryWrapper.eq(ObjectUtil.isNotEmpty(ysArticle.getId()),YsArticle::getId,ysArticle.getId());
		lambdaQueryWrapper.like(ObjectUtil.isNotEmpty(ysArticle.getTitle()),YsArticle::getTitle,ysArticle.getTitle());
		lambdaQueryWrapper.eq(ObjectUtil.isNotEmpty(ysArticle.getType())&&ysArticle.getType()>=0,YsArticle::getType,ysArticle.getType());
		return PageUtils.buildDataInfo(page(PageUtils.buildPage(), lambdaQueryWrapper));
	}
}
