package com.ysweet.shop.service.impl;

import cn.hutool.Hutool;
import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSON;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.gson.JsonObject;
import com.ysweet.common.core.domain.AjaxResult;
import com.ysweet.common.core.page.TableDataInfo;
import com.ysweet.common.utils.PageUtils;
import com.ysweet.common.utils.SecurityUtils;
import com.ysweet.common.utils.ServletUtils;
import com.ysweet.common.utils.ip.IpUtils;
import com.ysweet.shop.domain.YsOrder;
import com.ysweet.shop.domain.YsOrderGoods;
import com.ysweet.shop.mapper.YsOrderMapper;
import com.ysweet.shop.service.IYsOrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

/**
 *  订单管理
 *
 * @author 小金木
 * 2021年8月23日14:23:09
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class YsOrderService extends ServiceImpl<YsOrderMapper, YsOrder> implements IYsOrderService {

	private final  YsOrderGoodsServiceImpl ysOrderGoodsServiceImpl;

	@Override
	public TableDataInfo<YsOrder> selectOfPage(YsOrder ysOrder) {
		if(ObjectUtil.isEmpty(ysOrder)) {
			ysOrder=new YsOrder();
		}
		LambdaQueryWrapper<YsOrder> lambdaQueryWrapper=new LambdaQueryWrapper();
		lambdaQueryWrapper.eq(ObjectUtil.isNotEmpty(ysOrder.getUserId()),YsOrder::getUserId,ysOrder.getUserId());
		lambdaQueryWrapper.eq(ObjectUtil.isNotEmpty(ysOrder.getState()),YsOrder::getState,ysOrder.getState());
		lambdaQueryWrapper.eq(ObjectUtil.isNotEmpty(ysOrder.getPaymentState()),YsOrder::getPaymentState,ysOrder.getPaymentState());
		lambdaQueryWrapper.eq(ObjectUtil.isNotEmpty(ysOrder.getTableNum()),YsOrder::getTableNum,ysOrder.getTableNum());
		return PageUtils.buildDataInfo(page(PageUtils.buildPage(), lambdaQueryWrapper));
	}

	@Override
	public YsOrder selectById(Long orderId) {
		YsOrder ysOrder=this.getById(orderId);
		ysOrderGoodsServiceImpl.list();
		LambdaQueryWrapper<YsOrderGoods> lambdaQueryWrapper=new LambdaQueryWrapper();
		lambdaQueryWrapper.eq(ObjectUtil.isNotEmpty(orderId),YsOrderGoods::getOrderId,orderId);
		List<YsOrderGoods> list= ysOrderGoodsServiceImpl.list(lambdaQueryWrapper);
		ysOrder.setGoods(JSONUtil.toJsonStr(list));
		return ysOrder;
	}

	/***
	 * 提交订单
	 * @param ysOrder
	 * @return
	 */
	@Override
	@Transactional
	public AjaxResult commitOrder(YsOrder ysOrder) {
		Snowflake snowflake = IdUtil.getSnowflake(1, 1);
		Long id = snowflake.nextId();
		ysOrder.setOrderNum(id.toString());
		JSONArray jsonArray= JSONUtil.parseArray(ysOrder.getGoods());
		String image=jsonArray.getJSONObject(0).getStr("showImage");
		ysOrder.setGoodsPicture(image);
		String name=jsonArray.getJSONObject(0).getStr("goodName");
		ysOrder.setGoodsName(name);
		ysOrder.setState(0);
		ysOrder.setUserIp(IpUtils.getIpAddr(ServletUtils.getRequest()));
		if(save(ysOrder)){
			JSONArray jsonArrayObj=JSONUtil.parseArray(ysOrder.getGoods());
			List<YsOrderGoods> list = ListUtil.list(false);
			for(int i=0;i<jsonArrayObj.size();i++){
				YsOrderGoods ysOrderGoods=new YsOrderGoods();
				ysOrderGoods.setOrderId(ysOrder.getOrderId());
				JSONObject jsonObject= jsonArrayObj.getJSONObject(i);
				Long goodId=jsonObject.getLong("goodId");
				ysOrderGoods.setGoodsId(goodId);
				String goodName=jsonObject.getStr("goodName");
				ysOrderGoods.setGoodsName(goodName);
				String showImage=jsonObject.getStr("showImage");
				ysOrderGoods.setPic(showImage);
				String materials_text=jsonObject.getStr("materials_text");
				ysOrderGoods.setSkuName(materials_text);
				BigDecimal price=jsonObject.getBigDecimal("price");
				ysOrderGoods.setPrice(price);
				int number=jsonObject.getInt("number");
				ysOrderGoods.setNumber(number);
				ysOrderGoods.setTotalPrice(price.multiply(new BigDecimal(number)));
				ysOrderGoods.setState(0);
				ysOrderGoods.setUserId(ysOrder.getUserId());
				list.add(ysOrderGoods);
			}
			if(!ysOrderGoodsServiceImpl.saveBatch(list)){
				return AjaxResult.error();
			}
			return AjaxResult.success(ysOrder);
		}
		return AjaxResult.error();
	}
}
