package com.ysweet.shop.service;

import com.ysweet.common.core.domain.AjaxResult;
import com.ysweet.system.domain.YsMaterialGroup;
import com.ysweet.common.core.page.IServicePlus;


/**
 * 素材分组Service接口
 *
 * @author 小金木
 * @date 2021年6月21日10:26:56
 */
public interface IYsMaterialGroupService extends IServicePlus<YsMaterialGroup> {
	AjaxResult addGroup(String name);
}
