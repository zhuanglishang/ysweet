package com.ysweet.shop.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ysweet.common.core.page.TableDataInfo;
import com.ysweet.common.utils.PageUtils;
import com.ysweet.shop.domain.YsBanner;
import com.ysweet.shop.mapper.YsBannerMapper;
import com.ysweet.shop.service.IYsBannerService;
import org.springframework.stereotype.Service;

/**
 *  广告基础信息
 *
 * @author 小金木
 * @date 2021年6月21日10:27:07
 */
@Service
public class YsBannerService extends ServiceImpl<YsBannerMapper, YsBanner> implements IYsBannerService {
	/***
	 * 根据查询条件返回广告列表
	 * @param ysBanner
	 * @return
	 */
	@Override
	public TableDataInfo<YsBanner> selectByPage(YsBanner ysBanner) {
		LambdaQueryWrapper<YsBanner> lqw = new LambdaQueryWrapper<YsBanner>()
			.eq(ObjectUtil.isNotEmpty(ysBanner.getGroupId())&&ysBanner.getGroupId()>0, YsBanner::getGroupId, ysBanner.getGroupId())
			.eq(ObjectUtil.isNotEmpty(ysBanner.getStatus()),YsBanner::getStatus, ysBanner.getStatus())
			.orderByDesc(YsBanner::getCreateTime);
		return PageUtils.buildDataInfo(page(PageUtils.buildPage(), lqw));
	}
}
