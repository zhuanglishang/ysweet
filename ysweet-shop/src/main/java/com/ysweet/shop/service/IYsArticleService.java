package com.ysweet.shop.service;

import com.ysweet.common.core.page.IServicePlus;
import com.ysweet.common.core.page.TableDataInfo;
import com.ysweet.shop.domain.YsArticle;

/**
 *  文章管理
 *
 * @author 小金木
 * @date 2021年6月21日10:27:07
 */
public interface IYsArticleService extends IServicePlus<YsArticle> {
	TableDataInfo<YsArticle> selectOfPage(YsArticle ysArticle);
}
