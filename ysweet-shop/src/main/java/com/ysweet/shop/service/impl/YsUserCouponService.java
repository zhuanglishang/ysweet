package com.ysweet.shop.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ysweet.common.core.page.TableDataInfo;
import com.ysweet.common.utils.PageUtils;
import com.ysweet.shop.domain.YsUserCoupon;
import com.ysweet.shop.mapper.YsUserCouponMapper;
import com.ysweet.shop.service.IYsUserCouponService;
import org.springframework.stereotype.Service;

/**
 *  用户优惠券管理
 * @author 小金木
 * @date 2021年6月25日11:17:10
 */
@Service
public class YsUserCouponService extends ServiceImpl<YsUserCouponMapper, YsUserCoupon> implements IYsUserCouponService {
	/***
	 * 根据查询条件返回优惠券列表
	 * @param ysUserCoupon
	 * @return
	 */
	@Override
	public TableDataInfo<YsUserCoupon> selectByPage(YsUserCoupon ysUserCoupon) {
		/*
		* LambdaQueryWrapper<YsBanner> lqw = new LambdaQueryWrapper<YsBanner>()
			.eq(ObjectUtil.isNotEmpty(ysBanner.getGroupId())&&ysBanner.getGroupId()>0, YsBanner::getGroupId, ysBanner.getGroupId())
			.eq(ObjectUtil.isNotEmpty(ysBanner.getStatus()),YsBanner::getStatus, ysBanner.getStatus())
			.orderByDesc(YsBanner::getCreateTime);
		return PageUtils.buildDataInfo(page(PageUtils.buildPage(), lqw));*/
		return PageUtils.buildDataInfo(page(PageUtils.buildPage(), null));
	}
}
