package com.ysweet.shop.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ysweet.common.core.page.TableDataInfo;
import com.ysweet.common.utils.PageUtils;
import com.ysweet.shop.domain.YsCategory;
import com.ysweet.shop.mapper.YsCategoryMapper;
import com.ysweet.shop.service.IYsCategoryService;
import org.springframework.stereotype.Service;

/**
 * 商品分类管理
 *
 * @author 小金木
 * @date 2021年6月21日10:27:07
 */
@Service
public class YsCategoryService extends ServiceImpl<YsCategoryMapper, YsCategory> implements IYsCategoryService {
	/***
	 * 根据查询条件返回广告列表
	 * @param ysCategory
	 * @return
	 */
	@Override
	public TableDataInfo<YsCategory> selectByPage(YsCategory ysCategory) {
		if (ObjectUtil.isEmpty(ysCategory)) {
			ysCategory = new YsCategory();
		}
		LambdaQueryWrapper<YsCategory> lqw = new LambdaQueryWrapper<YsCategory>()
			.eq(ObjectUtil.isNotEmpty(ysCategory) && ObjectUtil.isNotEmpty(ysCategory.getIsVisible()), YsCategory::getIsVisible, ysCategory.getIsVisible())
			.orderByAsc(YsCategory::getSort);
		return PageUtils.buildDataInfo(page(PageUtils.buildPage(), lqw));
	}
}
