package com.ysweet.shop.service;

import com.ysweet.common.core.page.IServicePlus;
import com.ysweet.common.core.page.TableDataInfo;
import com.ysweet.shop.domain.YsOrderGoods;

import java.util.List;

/**
 * 订单物品管理
 *
 * @author 小金木
 * @date 2021年8月23日14:23:00
 */
public interface IYsOrderGoodsService extends IServicePlus<YsOrderGoods> {
	/***
	 *  根据条件查询定单物品
	 * @param ysOrderGoods 实体类
	 * @return 分页的数据
	 */
	TableDataInfo<YsOrderGoods> selectOfPage(YsOrderGoods ysOrderGoods);

	/***
	 * 更新或者保存订单物品
	 * @param list 需要更新的物品
	 * @return 结果
	 */
	boolean saveOrUpdateOrderGoods(List<YsOrderGoods> list);
}
