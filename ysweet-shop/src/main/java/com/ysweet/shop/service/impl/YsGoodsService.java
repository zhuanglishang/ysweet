package com.ysweet.shop.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ysweet.common.core.page.TableDataInfo;
import com.ysweet.common.utils.PageUtils;
import com.ysweet.shop.domain.YsGoods;
import com.ysweet.shop.domain.YsGoodsSku;
import com.ysweet.shop.mapper.YsGoodsMapper;
import com.ysweet.shop.service.IYsGoodsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 商品信息
 *
 * @author 小金木
 * @date 2021年7月8日14:20:02
 */
@Service
@RequiredArgsConstructor
public class YsGoodsService extends ServiceImpl<YsGoodsMapper, YsGoods> implements IYsGoodsService {

	private final YsGoodsSkuService ysGoodsSkuService;

	/***
	 * 根据查询条件返回商品信息
	 * @param ysGoods
	 * @return
	 */
	@Override
	public TableDataInfo<YsGoods> selectByPage(YsGoods ysGoods) {
		LambdaQueryWrapper<YsGoods> lqw = new LambdaQueryWrapper<YsGoods>();
		lqw.eq(ObjectUtil.isNotEmpty(ysGoods.getGoodId()), YsGoods::getGoodId, ysGoods.getGoodId());
		lqw.eq(ObjectUtil.isNotEmpty(ysGoods.getCategoryId()), YsGoods::getCategoryId, ysGoods.getCategoryId());
		lqw.eq(YsGoods::getSoldOut,0);
		TableDataInfo<YsGoods> result = PageUtils.buildDataInfo(page(PageUtils.buildPage(), lqw));
		List<YsGoods> list = result.getRows().stream().map(this::apply).collect(Collectors.toList());
		result.setRows(list);
		return result;
	}

	/***
	 * 保存商品
	 * @param ysBanner
	 * @return
	 */
	@Override
	public Boolean saveOrUpdateGoods(YsGoods ysGoods) {
		this.saveOrUpdate(ysGoods);
		String skuStr = ysGoods.getYsGoodsSku();
		Long goodsId = ysGoods.getGoodId();
		YsGoodsSku ysGoodsSku = new YsGoodsSku();
		ysGoodsSku.setGoodsId(goodsId);
		ysGoodsSku.setJson(skuStr);
		ysGoodsSkuService.saveOrUpdate(ysGoodsSku, new QueryWrapper<YsGoodsSku>().lambda().eq(YsGoodsSku::getGoodsId, goodsId));
		return true;
	}
	/***
	 * 根据分类查询商品
	 * @param categoryId
	 * @return
	 */
	@Override
	public TableDataInfo<YsGoods> selectByCategoryId(Integer categoryId) {
		YsGoods ysGoods = new YsGoods();
		ysGoods.setCategoryId(categoryId);
		return this.selectByPage(ysGoods);
	}
	/***
	 * 根据类型查询商品
	 * @param type
	 * @return
	 */
	@Override
	public List<YsGoods> selectByType(String type) {
		YsGoods ysGoods = new YsGoods();
		boolean isNull = false;
		switch (type) {
			case "recommend":
				ysGoods.setIsRecommend(1);
				break;
			case "hot":
				ysGoods.setIsHot(1);
				break;
			case "new":
				ysGoods.setIsNew(1);
				break;
			default:
				isNull = true;
				break;
		}
		if (isNull) {
			return new ArrayList();
		}
		LambdaQueryWrapper<YsGoods> wrapper = new LambdaQueryWrapper<>();
		wrapper.eq(ObjectUtil.isNotEmpty(ysGoods.getIsNew()), YsGoods::getIsNew, ysGoods.getIsNew());
		wrapper.eq(ObjectUtil.isNotEmpty(ysGoods.getIsHot()), YsGoods::getIsHot, ysGoods.getIsHot());
		wrapper.eq(ObjectUtil.isNotEmpty(ysGoods.getIsRecommend()), YsGoods::getIsRecommend, ysGoods.getIsRecommend());
		wrapper.eq(YsGoods::getSoldOut,0);
		List<YsGoods> result = this.list(wrapper);
		List<YsGoods> list = result.stream().map(this::apply).collect(Collectors.toList());

		return list;
	}

	/***
	 * 组装商品的sku
	 * @param obj
	 * @return
	 */
	private YsGoods apply(YsGoods obj) {
		QueryWrapper<YsGoodsSku> innerWrapper = new QueryWrapper<>();
		innerWrapper.eq("goods_id", obj.getGoodId());
		YsGoodsSku ysGoodsSku = ysGoodsSkuService.getOne(innerWrapper);
		if (ObjectUtil.isNotEmpty(ysGoodsSku)) {
			obj.setYsGoodsSku(ysGoodsSku.getJson());
		}
		return obj;
	}
}
