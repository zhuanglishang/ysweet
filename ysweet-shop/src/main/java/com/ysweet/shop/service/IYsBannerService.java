package com.ysweet.shop.service;

import com.ysweet.common.core.page.IServicePlus;
import com.ysweet.common.core.page.TableDataInfo;
import com.ysweet.shop.domain.YsBanner;

/**
 *  广告基础信息
 *
 * @author 小金木
 * @date 2021年6月21日10:27:07
 */
public interface IYsBannerService extends IServicePlus<YsBanner> {
	/***
	 * 根据查询条件返回广告列表
	 * @param ysBanner YsBanners实体
	 * @return 发的撒
	 */
	TableDataInfo<YsBanner> selectByPage(YsBanner ysBanner);
}
