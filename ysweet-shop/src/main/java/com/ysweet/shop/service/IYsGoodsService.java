package com.ysweet.shop.service;

import com.ysweet.common.core.page.IServicePlus;
import com.ysweet.common.core.page.TableDataInfo;
import com.ysweet.shop.domain.YsGoods;

import java.util.List;

/**
 * 商品信息
 *
 * @author 小金木
 * @date 2021年7月8日14:20:11
 */
public interface IYsGoodsService extends IServicePlus<YsGoods> {
	/***
	 * 根据查询条件返回商品信息
	 * @param ysBanner
	 * @return
	 */
	TableDataInfo<YsGoods> selectByPage(YsGoods ysBanner);

	/***
	 * 保存商品
	 * @param ysBanner
	 * @return
	 */
	Boolean saveOrUpdateGoods(YsGoods ysBanner);

	/***
	 * 根据分类查询商品
	 * @param categoryId
	 * @return
	 */
	TableDataInfo<YsGoods> selectByCategoryId(Integer categoryId);

	/***
	 * 根据类型查询商品
	 * @param type
	 * @return
	 */
	List<YsGoods> selectByType(String type);
}
