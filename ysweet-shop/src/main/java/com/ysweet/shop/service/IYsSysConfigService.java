package com.ysweet.shop.service;

import com.ysweet.common.core.page.IServicePlus;
import com.ysweet.shop.domain.YsSysConfig;

import java.util.List;
import java.util.Map;

/**
 * 广告基础信息
 *
 * @author 小金木
 * @date 2021年6月21日10:27:07
 */
public interface IYsSysConfigService extends IServicePlus<YsSysConfig> {
	/***
	 * 根据配置类型获取配置的集合
	 * @param type
	 * @return
	 */
	List<YsSysConfig> selectByType(String type);
	/***
	 * 更新key对应的value
	 * @param param
	 * @return
	 */
	boolean updateConfig(Map<String, String> param);
}
