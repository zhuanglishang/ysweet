package com.ysweet.shop.service;

import com.ysweet.common.core.page.IServicePlus;
import com.ysweet.shop.domain.YsUser;

/**
 * UserService接口
 *
 * @author 小金木
 * @date 2021-06-17
 */
public interface IYsUserService extends IServicePlus<YsUser> {
}
