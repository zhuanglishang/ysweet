package com.ysweet.shop.service;

import com.ysweet.common.core.domain.AjaxResult;
import com.ysweet.common.core.page.IServicePlus;
import com.ysweet.common.core.page.TableDataInfo;
import com.ysweet.shop.domain.YsOrder;

/**
 *  订单管理
 *
 * @author 小金木
 * @date 2021年8月23日14:23:00
 */
public interface IYsOrderService extends IServicePlus<YsOrder> {
	TableDataInfo<YsOrder> selectOfPage(YsOrder ysOrder);

	AjaxResult commitOrder(YsOrder ysOrder);
	YsOrder selectById(Long selectById);

}
