package com.ysweet.shop.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ysweet.common.core.page.TableDataInfo;
import com.ysweet.common.utils.PageUtils;
import com.ysweet.shop.domain.YsDesk;
import com.ysweet.shop.mapper.YsDeskMapper;
import com.ysweet.shop.service.IYsDeskService;
import org.springframework.stereotype.Service;

/**
 * 餐桌Service业务层处理
 *
 * @author 小金木
 * @date 2021年8月20日10:38:37
 */
@Service
public class YsDeskServiceImpl extends ServiceImpl<YsDeskMapper, YsDesk> implements IYsDeskService {

	@Override
	public TableDataInfo<YsDesk> listOfPage(YsDesk ysDesk) {
		if(ObjectUtil.isEmpty(ysDesk))
			ysDesk=new YsDesk();
		LambdaQueryWrapper<YsDesk> lambdaQueryWrapper=new LambdaQueryWrapper();
		lambdaQueryWrapper.eq(ObjectUtil.isNotEmpty(ysDesk.getId()),YsDesk::getId,ysDesk.getId());
		return PageUtils.buildDataInfo(page(PageUtils.buildPage(), lambdaQueryWrapper));
	}
}
