package com.ysweet.shop.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ysweet.shop.domain.YsSysConfig;
import com.ysweet.shop.mapper.YsSysConfigMapper;
import com.ysweet.shop.service.IYsSysConfigService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 广告基础信息
 *
 * @author 小金木
 * @date 2021年6月21日10:27:07
 */
@Service
public class YsSysConfigService extends ServiceImpl<YsSysConfigMapper, YsSysConfig> implements IYsSysConfigService {
	/***
	 * 根据配置类型获取配置的集合
	 * @param type
	 * @return
	 */
	@Override
	public List<YsSysConfig> selectByType(String type) {
		LambdaQueryWrapper<YsSysConfig> lqw = new LambdaQueryWrapper<YsSysConfig>()
			.eq(YsSysConfig::getType, type);
		return list(lqw);
	}

	/***
	 * 更新key对应的value
	 * @param param
	 * @return
	 */
	@Override
	public boolean updateConfig(Map<String, String> param) {
		List<YsSysConfig> list=new ArrayList<>(param.size());
		for(Map.Entry<String,String> entry:param.entrySet()){
			String key=entry.getKey();
			String value=entry.getValue();
			YsSysConfig ysSysConfig= this.getOne(new LambdaQueryWrapper<YsSysConfig>()
				.eq(YsSysConfig::getKeyStr, key));
			ysSysConfig.setValueStr(value);
			list.add(ysSysConfig);
		}
		return this.saveOrUpdateBatch(list);
	}
}
