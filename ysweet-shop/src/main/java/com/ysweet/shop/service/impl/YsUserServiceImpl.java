package com.ysweet.shop.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ysweet.shop.domain.YsUser;
import com.ysweet.shop.mapper.YsUserMapper;
import com.ysweet.shop.service.IYsUserService;
import org.springframework.stereotype.Service;

/**
 * 【请填写功能名称】Service业务层处理
 *
 * @author 小金木
 * @date 2021-06-17
 */
@Service
public class YsUserServiceImpl extends ServiceImpl<YsUserMapper, YsUser> implements IYsUserService {

}
