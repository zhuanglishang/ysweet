package com.ysweet.shop.service;

import com.ysweet.common.core.page.IServicePlus;
import com.ysweet.common.core.page.TableDataInfo;
import com.ysweet.shop.domain.YsCoupon;

/**
 *  优惠券管理
 * @author 小金木
 * @date 2021年6月25日11:17:03
 */
public interface IYsCouponService extends IServicePlus<YsCoupon> {
	/***
	 * 根据查询条件返回优惠券列表
	 * @param ysBanner
	 * @return
	 */
	TableDataInfo<YsCoupon> selectByPage(YsCoupon ysBanner);
}
