package com.ysweet.shop.service;

import com.ysweet.common.core.page.IServicePlus;
import com.ysweet.common.core.page.TableDataInfo;
import com.ysweet.shop.domain.YsUserCoupon;

/**
 *  用户优惠券管理
 * @author 小金木
 * @date 2021年6月25日11:17:03
 */
public interface IYsUserCouponService extends IServicePlus<YsUserCoupon> {
	/***
	 * 根据查询条件返回用户优惠券
	 * @param ysUserCoupon
	 * @return
	 */
	TableDataInfo<YsUserCoupon> selectByPage(YsUserCoupon ysUserCoupon);
}
