package com.ysweet.shop.service;

import com.ysweet.common.core.page.IServicePlus;
import com.ysweet.common.core.page.TableDataInfo;
import com.ysweet.shop.domain.YsDesk;

/**
 * UserService接口
 *
 * @author 小金木
 * @date 2021年8月20日10:39:01
 */
public interface IYsDeskService extends IServicePlus<YsDesk> {
	TableDataInfo<YsDesk> listOfPage(YsDesk ysDesk);
}
