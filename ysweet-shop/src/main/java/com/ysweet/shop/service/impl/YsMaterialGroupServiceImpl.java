package com.ysweet.shop.service.impl;

import cn.hutool.crypto.digest.MD5;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ysweet.common.core.domain.AjaxResult;
import com.ysweet.common.utils.SecurityUtils;
import com.ysweet.shop.service.IYsMaterialGroupService;
import com.ysweet.system.domain.YsMaterialGroup;
import com.ysweet.system.mapper.YsMaterialGroupMapper;
import org.springframework.stereotype.Service;

/**
 * 素材分组Service业务层处理
 *
 * @author 小金木
 * @date 2021-06-18
 */
@Service
public class YsMaterialGroupServiceImpl extends ServiceImpl<YsMaterialGroupMapper, YsMaterialGroup> implements IYsMaterialGroupService {
	@Override
	public AjaxResult<Boolean> addGroup(String name) {
		YsMaterialGroup ysMaterialGroup=new YsMaterialGroup();
		ysMaterialGroup.setId(MD5.create().digestHex(name));
		ysMaterialGroup.setName(name);
		ysMaterialGroup.setCreateId(SecurityUtils.getUsername());
		return AjaxResult.success(save(ysMaterialGroup));
	}
}
