package com.ysweet.shop.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ysweet.common.core.page.TableDataInfo;
import com.ysweet.common.utils.PageUtils;
import com.ysweet.shop.domain.YsGoodsSku;
import com.ysweet.shop.mapper.YsGoodsSkuMapper;
import com.ysweet.shop.service.IYsGoodsSkuService;
import org.springframework.stereotype.Service;

/**
 * 商品规格信息
 *
 * @author 小金木
 * @date 2021年7月8日14:20:02
 */
@Service
public class YsGoodsSkuService extends ServiceImpl<YsGoodsSkuMapper, YsGoodsSku> implements IYsGoodsSkuService {
	/***
	 * 根据查询条件返回商品信息
	 * @param ysGoodsSku
	 * @return
	 */
	@Override
	public TableDataInfo<YsGoodsSku> selectByPage(YsGoodsSku ysGoodsSku) {
		LambdaQueryWrapper<YsGoodsSku> lqw = new LambdaQueryWrapper<>();
		return PageUtils.buildDataInfo(page(PageUtils.buildPage(), lqw));
	}
}
