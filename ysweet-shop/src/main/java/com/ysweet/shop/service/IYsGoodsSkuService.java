package com.ysweet.shop.service;

import com.ysweet.common.core.page.IServicePlus;
import com.ysweet.common.core.page.TableDataInfo;
import com.ysweet.shop.domain.YsGoodsSku;

/**
 * 商品规格信息
 *
 * @author 小金木
 * @date 2021年7月8日14:20:11
 */
public interface IYsGoodsSkuService extends IServicePlus<YsGoodsSku> {
	/***
	 * 根据查询条件返回商品规格信息
	 * @param ysGoodsSku
	 * @return
	 */
	TableDataInfo<YsGoodsSku> selectByPage(YsGoodsSku ysGoodsSku);
}
