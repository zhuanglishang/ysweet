package com.ysweet.shop.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ysweet.common.core.domain.AjaxResult;
import com.ysweet.common.core.page.TableDataInfo;
import com.ysweet.common.utils.PageUtils;
import com.ysweet.shop.domain.YsOrderGoods;
import com.ysweet.shop.mapper.YsOrderGoodsMapper;
import com.ysweet.shop.service.IYsOrderGoodsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 订单管理
 *
 * @author 小金木
 * 2021年8月23日14:23:09
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class YsOrderGoodsServiceImpl extends ServiceImpl<YsOrderGoodsMapper, YsOrderGoods> implements IYsOrderGoodsService {
	@Override
	public TableDataInfo<YsOrderGoods> selectOfPage(YsOrderGoods ysOrderGoods) {
		if (ObjectUtil.isEmpty(ysOrderGoods)) {
			ysOrderGoods = new YsOrderGoods();
		}
		LambdaQueryWrapper<YsOrderGoods> lambdaQueryWrapper = new LambdaQueryWrapper<>();
		lambdaQueryWrapper.eq(ObjectUtil.isNotEmpty(ysOrderGoods.getOrderId()), YsOrderGoods::getOrderId, ysOrderGoods.getUserId());
		lambdaQueryWrapper.eq(ObjectUtil.isNotEmpty(ysOrderGoods.getState()), YsOrderGoods::getState, ysOrderGoods.getState());
		lambdaQueryWrapper.eq(ObjectUtil.isNotEmpty(ysOrderGoods.getUserId()), YsOrderGoods::getUserId, ysOrderGoods.getUserId());
		return PageUtils.buildDataInfo(page(PageUtils.buildPage(), lambdaQueryWrapper));
	}

	@Override
	@Transactional
	public boolean saveOrUpdateOrderGoods(List<YsOrderGoods> ysOrderGoods) {
		if (ysOrderGoods.size() > 0) {
			for (YsOrderGoods good : ysOrderGoods) {
				saveOrUpdate(good);
			}
		}
		return true;
	}
}
