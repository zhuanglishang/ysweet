package com.ysweet.common.exception.user;

import com.ysweet.common.exception.BaseException;

/**
 * 用户信息异常类
 *
 * @author 小金木
 */
public class UserException extends BaseException
{
    private static final long serialVersionUID = 1L;

    public UserException(String code, Object[] args)
    {
        super("user", code, args, null);
    }
}
