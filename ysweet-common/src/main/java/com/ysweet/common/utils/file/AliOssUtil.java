package com.ysweet.common.utils.file;


import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.ysweet.common.config.AlibbOssConfig;
import com.ysweet.common.utils.spring.SpringUtils;

/***
 * 获得指定城市的endpoint
 * @author 小金木
 * @date 2021年5月26日08:32:08
 */
public class AliOssUtil {
    /*华东1（杭州）	https://oss-cn-hangzhou.aliyuncs.com
    华东2（上海）	https://oss-cn-shanghai.aliyuncs.com
    华北1（青岛）		https://oss-cn-qingdao.aliyuncs.com
    华北2（北京）		https://oss-cn-beijing.aliyuncs.com
    华北 3（张家口）	https://oss-cn-zhangjiakou.aliyuncs.com
    华北5（呼和浩特）	https://oss-cn-huhehaote.aliyuncs.com
    华北6（乌兰察布）	https://oss-cn-wulanchabu.aliyuncs.com
    华南1（深圳）	https://oss-cn-shenzhen.aliyuncs.com
    华南2（河源）	https://oss-cn-heyuan.aliyuncs.com
    华南3（广州）	https://oss-cn-guangzhou.aliyuncs.com
    西南1（成都）	https://oss-cn-chengdu.aliyuncs.com*/

    /***
     * 获得oss的物理服务器所在地址
     * @param region
     * @return
     */
    public static String getEndpoint(String region) {
        switch (region) {
            case "华东2":
                return "https://https://oss-cn-shanghai.aliyuncs.com";
            case "华北1（青岛）":
                return "https://https://oss-cn-qingdao.aliyuncs.com";
            case "华北2":
                return "https://oss-cn-beijing.aliyuncs.com";
            case "华东1":
            default:
                return "https://https://oss-cn-hangzhou.aliyuncs.com";
        }
    }

    /***
     * 获取一个Oss
     * @return
     */
    public static OSS buildOSS(){
        AlibbOssConfig alibbOssConfig= SpringUtils.getBean(AlibbOssConfig.class);
        String endPoint=getEndpoint(alibbOssConfig.getRegion());
        OSS ossClient = new OSSClientBuilder().build(endPoint, alibbOssConfig.getAccessKey(), alibbOssConfig.getAccessKeySecret());
        return  ossClient;
    }

}
