package com.ysweet.common.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/***
 * 阿里的oss配置
 * @author 小金木
 * @date 2021年5月26日12:31:27
 */
@Configuration(proxyBeanMethods = false)
@ConfigurationProperties(prefix = "alibb.oss")
@Getter
@Setter
public class AlibbOssConfig {
    private String accessKey;
    private String accessKeySecret;
    private String region;
}
