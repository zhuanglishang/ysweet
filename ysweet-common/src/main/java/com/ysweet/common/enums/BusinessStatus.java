package com.ysweet.common.enums;

/**
 * 操作状态
 *
 * @author 小金木
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
