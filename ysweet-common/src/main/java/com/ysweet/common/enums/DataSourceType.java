package com.ysweet.common.enums;

/**
 * 数据源
 *
 * @author 小金木
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
