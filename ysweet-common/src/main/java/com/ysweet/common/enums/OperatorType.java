package com.ysweet.common.enums;

/**
 * 操作人类别
 *
 * @author 小金木
 */
public enum OperatorType
{
    /**
     * 其它
     */
    OTHER,

    /**
     * 后台用户
     */
    MANAGE,

    /**
     * 手机端用户
     */
    MOBILE
}
