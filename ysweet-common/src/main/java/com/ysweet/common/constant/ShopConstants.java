package com.ysweet.common.constant;

/**
 * 店铺相关的常量
 * @author hupeng
 * @since 2020-02-27
 */
public class ShopConstants {

    public static final String MINI_SESSION_KET = "session_key:";

    /**
     * 微信小程序service
     */
	public static final String WEIXIN_MA_SERVICE = "weixin_ma_service";

	public static final String APP_LOGIN_USER = "app-online-token:";
}
