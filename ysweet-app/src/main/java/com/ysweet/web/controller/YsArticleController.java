package com.ysweet.web.controller;

import com.ysweet.common.core.domain.AjaxResult;
import com.ysweet.shop.domain.YsArticle;
import com.ysweet.shop.service.IYsArticleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 文章管理
 *
 * @author 小金木
 * @date 2021年6月21日10:27:07
 */
@Api(value = "文章管理控制器", tags = {"文章信息管理"})
@RequiredArgsConstructor
@RestController
@RequestMapping("/shop/article")
public class YsArticleController {

	private final IYsArticleService ysArticleService;

	@ApiOperation("查询文章列表")
	@GetMapping("/type_article")
	//@AuthCheck
	public AjaxResult getArticle(YsArticle ysArticle) {
		return AjaxResult.success(ysArticleService.selectOfPage(ysArticle).getRows());
	}

}
