package com.ysweet.web.controller;

import com.ysweet.web.core.interceptor.AuthCheck;
import com.ysweet.common.core.controller.BaseController;
import com.ysweet.common.core.domain.AjaxResult;
import com.ysweet.shop.service.IYsCouponService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/***
 * 优惠券增删改查
 * @author 小金木
 * @date 2021年6月25日11:24:21
 */
@Api(value = "优惠券管理相关逻辑", tags = {"优惠券管理相关逻辑"})
@RestController
@RequestMapping("/shop/coupon")
@RequiredArgsConstructor
@Slf4j
public class YsCouponController extends BaseController {

	private final IYsCouponService ysCouponService;

	/**
	 * 获得用户自己的优惠券
	 */
	@GetMapping("/getAllCoupon")
	@ApiOperation(value = "获得所有优惠券", notes = "获得所有优惠券")
	@AuthCheck
	public AjaxResult getAllCoupon() {
		return AjaxResult.success(ysCouponService.list());
	}

	/**
	 * 获得可用的满减优惠券
	 */
	@GetMapping("/order_coupon")
	@ApiOperation(value = "获得可用的满减优惠券", notes = "获得可用的满减优惠券")
	@AuthCheck
	public AjaxResult getOrderCoupon(String totalMoney) {
		return AjaxResult.success(ysCouponService.list());
	}


}
