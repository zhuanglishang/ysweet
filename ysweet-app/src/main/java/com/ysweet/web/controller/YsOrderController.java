package com.ysweet.web.controller;

import com.ysweet.common.core.domain.AjaxResult;
import com.ysweet.shop.domain.YsOrder;
import com.ysweet.shop.service.IYsOrderService;
import com.ysweet.web.core.interceptor.AuthCheck;
import com.ysweet.web.core.service.JwtToken;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * 订单管理
 *
 * @author 小金木
 * @date 2021年8月23日14:24:11
 */
@Api(value = "订单管理", tags = {"订单管理"})
@RequiredArgsConstructor
@RestController
@RequestMapping("/shop/order")
public class YsOrderController {

	private final IYsOrderService ysOrderService;

	@ApiOperation("查询订单列表")
	@GetMapping("/list")
	@AuthCheck
	public AjaxResult getOrders(YsOrder ysOrder) {
		ysOrder.setUserId(JwtToken.getUidByToken());
		return AjaxResult.success(ysOrderService.selectOfPage(ysOrder).getRows());
	}
	@ApiOperation("提交订单信息")
	@PostMapping("/commit")
	@AuthCheck
	public AjaxResult saveOrder(@RequestBody YsOrder ysOrder) {
		ysOrder.setUserId(JwtToken.getUidByToken());
		return AjaxResult.success(ysOrderService.commitOrder(ysOrder));
	}

	@ApiOperation("查询某个订单的详情")
	@GetMapping("/get_order_one")
	@AuthCheck
	public AjaxResult getOrders(Long orderId) {
		return AjaxResult.success(ysOrderService.selectById(orderId));
	}

	@ApiOperation("删除订单")
	@GetMapping("/delete")
	@AuthCheck
	public AjaxResult delete(Long orderId) {
		return AjaxResult.success(ysOrderService.removeById(orderId));
	}
}
