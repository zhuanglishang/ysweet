package com.ysweet.web.controller;

import com.ysweet.common.core.controller.BaseController;
import com.ysweet.common.core.page.TableDataInfo;
import com.ysweet.shop.domain.YsBanner;
import com.ysweet.shop.service.IYsBannerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 广告信息管理
 *
 * @author 小金木
 * @date 2021年6月21日10:27:07
 */
@Api(value = "广告控制器", tags = {"广告信息管理"})
@RequiredArgsConstructor
@RestController
@RequestMapping("/shop/banner")
public class YsBannerController extends BaseController {

	private final IYsBannerService ysBannerService;

	@ApiOperation("查询广告列表")
	@GetMapping("/list")
	public TableDataInfo<YsBanner> getBanner(YsBanner ysBanner) {
		return ysBannerService.selectByPage(ysBanner);
	}

}
