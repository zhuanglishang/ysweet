package com.ysweet.web.controller;

import com.ysweet.common.core.domain.AjaxResult;
import com.ysweet.shop.domain.YsDesk;
import com.ysweet.shop.service.IYsDeskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 文章管理
 *
 * @author 小金木
 * @date 2021年6月21日10:27:07
 */
@Api(value = "餐桌管理控制器", tags = {"餐桌信息管理"})
@RequiredArgsConstructor
@RestController
@RequestMapping("/shop/desk")
public class YsDeskController {

	private final IYsDeskService ysDeskService;

	@ApiOperation("查询所有餐桌")
	@GetMapping("/get_all_zz")
	//@AuthCheck
	public AjaxResult getDesk(YsDesk ysDesk) {
		return AjaxResult.success(ysDeskService.listOfPage(ysDesk).getRows());
	}

}
