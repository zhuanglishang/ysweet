package com.ysweet.web.controller;

import com.ysweet.common.core.controller.BaseController;
import com.ysweet.common.core.domain.AjaxResult;
import com.ysweet.shop.service.IYsGoodsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 广告信息管理
 *
 * @author 小金木
 * @date 2021年6月21日10:27:07
 */
@Api(value = "商品管理", tags = {"商品管理"})
@RequiredArgsConstructor
@RestController
@RequestMapping("/shop/goods")
public class YsGoodsController extends BaseController {

	private final IYsGoodsService ysGoodsService;

	@ApiOperation("根据商品类别查询商品列表")
	@GetMapping("/list")
	public AjaxResult getGoodsByCategory(Integer categoryId) {
		return AjaxResult.success(ysGoodsService.selectByCategoryId(categoryId));
	}

	@ApiOperation("根据关键字查询商品列表")
	@GetMapping("/special")
	public AjaxResult getGoodsByType(String type) {
		return AjaxResult.success(ysGoodsService.selectByType(type));
	}

}
