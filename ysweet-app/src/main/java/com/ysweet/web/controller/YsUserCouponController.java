package com.ysweet.web.controller;

import com.ysweet.common.core.controller.BaseController;
import com.ysweet.common.core.domain.AjaxResult;
import com.ysweet.shop.service.IYsUserCouponService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/***
 * 用户优惠券管理
 * @author 小金木
 * @date 2021年6月25日11:24:21
 */
@Api(value = "用户优惠券相关逻辑", tags = {"用户优惠券相关逻辑"})
@RestController
@RequestMapping("/userCoupon")
@RequiredArgsConstructor
public class YsUserCouponController extends BaseController {

	private final IYsUserCouponService ysUserCouponService;

	/**
	 * 获得用户自己的优惠券
	 */
	@PostMapping("/getUserCoupon")
	@ApiOperation(value = "获得用户自己的优惠券", notes = "获得用户自己的优惠券")
	public AjaxResult getUserCoupon() {
		return AjaxResult.success();
	}

}
