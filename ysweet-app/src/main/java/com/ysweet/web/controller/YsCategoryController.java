package com.ysweet.web.controller;

import com.ysweet.common.core.controller.BaseController;
import com.ysweet.common.core.page.TableDataInfo;
import com.ysweet.shop.domain.YsCategory;
import com.ysweet.shop.service.IYsCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/***
 * 商品分类的增删改查
 * @author 小金木
 * @date 2021年7月6日17:12:02
 */
@Api(value = "商品分类相关逻辑", tags = {"商品分类相关逻辑"})
@RestController
@RequestMapping("/shop/category")
@RequiredArgsConstructor
public class YsCategoryController extends BaseController {

	private final IYsCategoryService ysCategoryService;

	@PostMapping("/getAllCategory")
	@ApiOperation(value = "获得所有商品分类", notes = "获得所有商品分类")
	public TableDataInfo<YsCategory> getAllCoupon(@RequestBody(required=false) YsCategory ysCategory) {
		return ysCategoryService.selectByPage(ysCategory);
	}

}
