package com.ysweet.web.core.interceptor;


import com.auth0.jwt.interfaces.Claim;
import com.ysweet.common.api.ApiCode;
import com.ysweet.common.constant.ShopConstants;
import com.ysweet.common.core.redis.RedisCache;
import com.ysweet.web.core.service.JwtToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.util.ObjectUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.Optional;


/**
 * 权限拦截器
 *
 * @author 小金木
 * @date 2021年6月11日10:37:00
 */

public class PermissionInterceptor implements HandlerInterceptor {

    @Autowired
    private RedisCache redisCache;

    @Value("${token.token-expired-in}")
    private Long expiredTimeIn;

    public PermissionInterceptor() {
        super();
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Optional<AuthCheck> authCheck = this.getAuthCheck(handler);
        if (!authCheck.isPresent()) {
            return true;
        }

        String bearerToken = request.getHeader("Authorization");
        if (ObjectUtils.isEmpty(bearerToken)) {
            throw new AccountExpiredException(ApiCode.UNAUTHORIZED.getMessage());
        }

        if (!bearerToken.startsWith("Bearer")) {
            throw new AccountExpiredException(ApiCode.UNAUTHORIZED.getMessage());
        }
        String[] tokens = bearerToken.split(" ");
        if (!(tokens.length == 2)) {
            throw new AccountExpiredException(ApiCode.UNAUTHORIZED.getMessage());
        }
        String token = tokens[1];

        Optional<Map<String, Claim>> optionalMap = JwtToken.getClaims(token);
        Map<String, Claim> map = optionalMap
                .orElseThrow(() -> new AccountExpiredException(ApiCode.UNAUTHORIZED.getMessage()));

        Integer uid = map.get("uid").asInt();

        //检测用户是否被踢出
        if (redisCache.getCacheObject(ShopConstants.APP_LOGIN_USER + uid) == null) {
            throw new AccountExpiredException(ApiCode.UNAUTHORIZED.getMessage());
        }
        return true;
    }

    private Optional<AuthCheck> getAuthCheck(Object handler) {
        if (handler instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            AuthCheck authCheck = handlerMethod.getMethod().getAnnotation(AuthCheck.class);
            if (authCheck == null) {
                return Optional.empty();
            }
            return Optional.of(authCheck);
        }
        return Optional.empty();
    }

}

