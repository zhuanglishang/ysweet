package com.ysweet.web.core.config;

import com.ysweet.web.core.interceptor.PermissionInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import com.ysweet.common.config.RuoYiConfig;
import com.ysweet.common.constant.Constants;
import com.ysweet.framework.interceptor.RepeatSubmitInterceptor;

/**
 * 通用配置
 *
 * @author 小金木
 */
@Configuration
public class InterceptorConfig implements WebMvcConfigurer
{
	@Autowired
	private RepeatSubmitInterceptor repeatSubmitInterceptor;

	@Bean
	public HandlerInterceptor getPermissionInterceptor() {
		return new PermissionInterceptor();
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry)
	{
		/** 本地文件上传路径 */
		registry.addResourceHandler(Constants.RESOURCE_PREFIX + "/**").addResourceLocations("file:" + RuoYiConfig.getProfile() + "/");
		/** 配置knife4j 显示文档 */
		registry.addResourceHandler("doc.html").addResourceLocations("classpath:/META-INF/resources/");
		/** swagger配置 */
		registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
		registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
		registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/").setCachePeriod(0);
	}

	/**
	 * 自定义拦截规则
	 */
	@Override
	public void addInterceptors(InterceptorRegistry registry)
	{
		registry.addInterceptor(repeatSubmitInterceptor).addPathPatterns("/**");
		registry.addInterceptor(getPermissionInterceptor()).addPathPatterns("/**");
	}

	/**
	 * 跨域配置
	 */
	@Bean
	public CorsFilter corsFilter()
	{
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		CorsConfiguration config = new CorsConfiguration();
		config.setAllowCredentials(true);
		// 设置访问源地址
		config.addAllowedOrigin("*");
		// 设置访问源请求头
		config.addAllowedHeader("*");
		// 设置访问源请求方法
		config.addAllowedMethod("*");
		// 对接口配置跨域设置
		source.registerCorsConfiguration("/**", config);
		return new CorsFilter(source);
	}
}
