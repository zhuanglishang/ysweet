package com.ysweet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.security.servlet.ManagementWebSecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

/**
 * 启动程序
 *
 * @author 小金木
 */

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, SecurityAutoConfiguration.class, ManagementWebSecurityAutoConfiguration.class})
public class RuoYiApplication {
	public static void main(String[] args) {
		System.setProperty("spring.devtools.restart.enabled", "false");
		SpringApplication.run(RuoYiApplication.class, args);
		System.out.println("        ┌─┐       ┌─┐                  \n" +
			"     ┌──┘ ┴───────┘ ┴──┐               \n" +
			"     │                 │               \n" +
			"     │       ───       │               \n" +
			"     │  ─┬┘       └┬─  │               \n" +
			"     │                 │               \n" +
			"     │       ─┴─       │               \n" +
			"     │                 │               \n" +
			"     └───┐         ┌───┘               \n" +
			"         │         │                   \n" +
			"         │         │   神兽保佑         \n" +
			"         │         │   代码永无BUG!     \n" +
			"         │         └──────────────┐    \n" +
			"         │                        │    \n" +
			"         │                        ├─┐  \n" +
			"         │                        ┌─┘  \n" +
			"         │                        │    \n" +
			"         └─┐  ┐  ┌───────┬──┐  ┌──┘    \n" +
			"           │ ─┤ ─┤       │ ─┤ ─┤       \n" +
			"           └──┴──┘       └──┴──┘       \n" +
			"                                       \n" +
			"(♥◠‿◠)ﾉﾞ  RuoYi-Vue-Plus启动成功  ლ(´ڡ`ლ)ﾞ");
	}
}
